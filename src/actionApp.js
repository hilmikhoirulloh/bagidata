import axios from "axios";
import * as CONFIG from "../config";
import {store ,  persistor} from "./app/redux/store";
import {SetDefaultIcon} from "./app/home/moduls/recommend/actions";
import { HandleForgetPassword } from "./app/authentication/moduls/forget-password/actions";
import {header} from "./helper";

export function appInitialized(){
    return {
        type: "NAV_REDUCER",
        page : "ourboarding"
    }
} 
export function changeAppInitialized(page){
    return {
        type: "NAV_REDUCER",
        page: page
    }
}
export function getWallet(){
    return {
        type : "GET_WALLET",
        payload  : axios.get(`${CONFIG.URL_API}/api/v1/userwallet`,header())
    }    
}
export function runApp(root){
    return async dispatch => {
        if(root == "after-login"){
           await dispatch(getWallet()) 
        }
    }
}