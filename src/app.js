
import React , {Component} from 'react'
import {registerScreens } from './app/navigators/RootNavigator';
import {UIManager} from "react-native";
import {Navigation} from "react-native-navigation";
import {store , persistor} from "./app/redux/store";
import * as appActions from "./actionApp";



import {
    setCustomText, setCustomPicker 
} from "react-native-global-props";
const customText = {
    style: {
        fontFamily: "Barlow-Regular"
    }
}
const customPicker = {
    style: {
        color : "red"
    }
}
setCustomPicker(customPicker);
setCustomText(customText);
registerScreens();

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);


const tabs = [
    {
        label: 'Beranda',
        screen: 'bagidata.Layout.Home',
        icon: require('../img/icon/home_active.png'),
        selectedIcon: require('../img/icon/home_active.png'),
        titleImage: require('../img/logo.png'),
        title: "",
    },                    
    {
        label: 'Koneksi',
        screen: 'bagidata.Layout.Connection',
        icon: require('../img/icon/connection_active.png'),
        selectedIcon: require('../img/icon/connection_active.png'),      
        title: "",
    },  
    {
        label: 'Riwayat',
        screen: 'bagidata.Layout.History',
        icon: require('../img/icon/all_history.png'),
        selectedIcon: require('../img/icon/question_active.png'),      
        title: "",
    },
    {
        label: 'Bantuan',
        screen: 'bagidata.Layout.Help',
        icon: require('../img/icon/question_active.png'),
        selectedIcon: require('../img/icon/question_active.png'),      
        title: "",
    },
    {
        label: 'Profil',
        screen: 'bagidata.Layout.Profile',
        icon: require('../img/icon/people_active.png'),
        selectedIcon: require('../img/icon/people_active.png'),      
        title: "",
    }
];

class  App extends Component {
    constructor(props) {
        super(props);

        store.subscribe(this.onStoreUpdate.bind(this));
        store.dispatch(appActions.appInitialized("after-login"));
    }

    onStoreUpdate = async () => {
        let {root } = store.getState();
        if (this.currentRoot != root.root) {
            this.currentRoot = root.root;
            await this.startApp(root.root);
            await store.dispatch(appActions.runApp(root.root));   
        }
    }
    

    startApp(root) {
        switch (root) {
            case 'register':
                    Navigation.startSingleScreenApp({
                  screen: {
                    screen: 'bagidata.Register', 
                    navigatorStyle: {}, 
                    navigatorButtons: {} ,
                    animationType: 'none',
                    },              
                    appStyle : {
                        navBarHidden: true,
                        navBarTransparent:true,
                    }
                });
                return;
            case 'ourboarding':
                Navigation.startSingleScreenApp({
                  screen: {
                    screen: 'bagidata.OurBoarding', 
                    navigatorStyle: {}, 
                    navigatorButtons: {} ,
                    animationType: 'none',
                    },              
                    appStyle : {
                        navBarHidden: true,
                        navBarTransparent:true,
                    }
                });
            return; 
            case 'after-login':
                Navigation.startTabBasedApp({
                    tabs,
                    animationType: 'none',
                    appStyle:{
                            tabBarBackgroundColor: '#ffffff',
                            navBarButtonColor: '#444444',
                            tabBarButtonColor: '#bbb',
                            navBarTextColor: '#444444',
                            navigationBarColor: '#003a66',
                            navBarBackgroundColor: '#ffffff',
                            statusBarColor: '#002b4c',
                            navBarBackgroundColor: '#f7f7f7', 
                            navBarTransparent: true,
                            tabFontFamily: 'Barlow-Regular',
                            selectedTabFontSize: 5,
                            forceTitlesDisplay: false
                        }
                    });
                return;
            default: 
          }
    }
}

new App()