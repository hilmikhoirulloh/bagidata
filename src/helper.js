import {store} from "./app/redux/store";

export function header(){
    const state = store.getState();
    let token = state.LoginAuthReducer.results.token;
    return {
        headers: {
            'Content-Type': 'application/json',
            'authorization' : `${token}`
          }
    }
}