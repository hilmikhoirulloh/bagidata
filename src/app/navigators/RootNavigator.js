import React  , {Component} from "react"
import {Navigation, ScreenVisibilityListener} from 'react-native-navigation';
import {store , persistor} from "../redux/store";
import { Provider } from "react-redux";

import OurBoarding from "../ourboarding/screen";

import AuthRegister from '../authentication/moduls/register/screens';
import AuthLogin from '../authentication/moduls/login/screens';
import AuthForgetPassword from '../authentication/moduls/forget-password/screens';
import AuthVerifyCode from '../authentication/moduls/verify-code/screens';
import AuthSetPassword from '../authentication/moduls/change-password/screens/';
import AuthAlert from "../authentication/components/alert";


// import Layout from '../layout';
import HomePromoAlert from "../home/components/alertPromotionBuy";
import Home from '../home/moduls/main/screen';
import HomeBuy from "../home/moduls/buy/screen";
import HomeBuyChargePulsa from "../home/moduls/charge-pulsa/screen";
import HomeBuyPackageData from "../home/moduls/buy-package-data/screen";
import HomeBuyGoPay from "../home/moduls/buy-gopay/screen";
import HomePayment from "../home/moduls/payment/screen";
import HomeEachPayment from "../home/moduls/each-payment-page/screen";
import HomeTransfer from "../home/moduls/transfer/screen";
import HomeTransferHistory from "../home/moduls/history-transfer/screen";
import HomeFavorite from "../home/moduls/favorite/screen";
import HomeRecommend from "../home/moduls/recommend/screen";
import HomePopupCategory from "../home/moduls/category-popup/screen";
import HomeEachPromotion from "../home/moduls/each-promotion/screen";
import HomeEachCategory from "../home/moduls/each-category/screen";
import HomeSearchPromotion from "../home/moduls/search/screen";
import HomeDetailStatus from "../home/moduls/detail-status/screen";
import HomeAchievement from "../home/moduls/achievement/screen";
import HomeTransaction from "../home/moduls/transaction/screen";

// import HomeGamificationReward from "../home/moduls/gamificationReward/screen";
// import HomeGamificationReward from "../home/moduls/gamificationReward/screen";
// import HomeGamificationAchievement from "../home/moduls/gamificationAchievement/screen";

import Connection from "../connection/moduls/main/screen";
import ConnectionPopupAccessPromotion from "../connection/moduls/main/components/popupAccessPromotion";
import ConnectionPopupDataControl from "../connection/moduls/main/components/popupDataControl";
import ConnectionPopupSocialMedia from "../connection/moduls/main/components/popupSocialMedia";
import ConnectionPopupReceipt from "../connection/moduls/main/components/popupReceipt";
import ConnectionUploadReceipt from "../connection/moduls/upload-receipt/screen";
import ConnectionPageWarning from "../connection/moduls/page-warning/screen";
import ConnectionCamera from "../connection/moduls/camera/screen";
import ConnectionDataControl from "../connection/moduls/data-control/screen";
import ConnectionConnectSosmed from "../connection/moduls/connect-sosmed/screen";
import ConnectionAuthCamera from "../connection/moduls/auth-camera/screen";

import HistoryApp from "../history/moduls/main/screen";
import HistoryAppTransfer from "../history/moduls/transfer/screen";
import HistoryAppBill from "../history/moduls/buy/screen";
import HistoryAppBuy from "../history/moduls/bill/screen";
import HistoryAppUploadReceipt from "../history/moduls/upload-receipt/screen";

import Help from "../help/moduls/main/screen";
import HelpEachPage from "../help/moduls/eachHelp/screen";
// import HelpAward from "../help/moduls/award/screen";
// import HelpFacebook from "../help/moduls/facebook/screen";
// import HelpInstagram from "../help/moduls/instagram/screen";
// import HelpNote from "../help/moduls/note/screen";
import HelpProvider from "../help/moduls/provider/screen";
// import HelpTwitter from "../help/moduls/twitter/screen";
// import HelpLock from "../help/moduls/lock/screen";

import Profile from "../profile/moduls/main/screen";
import ProfileSetting from "../profile/moduls/setting/screen";
import ProfileAccount from "../profile/moduls/account/screen";
import ProfileOthers from "../profile/moduls/others/screen";
 
import Loading from "../components/loading";

export function registerScreens() {

  Navigation.registerComponent('bagidata.OurBoarding', () => OurBoarding , store , Provider);

  Navigation.registerComponent('bagidata.Login', () => AuthLogin , store , Provider);
  Navigation.registerComponent('bagidata.Register', () => AuthRegister , store , Provider );
  Navigation.registerComponent('bagidata.ForgetPassword', () => AuthForgetPassword , store , Provider);
  Navigation.registerComponent('bagidata.VerifyCode', () => AuthVerifyCode , store , Provider);
  Navigation.registerComponent('bagidata.SetPassword' , ()=> AuthSetPassword , store, Provider);
  Navigation.registerComponent('bagidata.Auth.Alert' , ()=> AuthAlert , store, Provider);
  

  Navigation.registerComponent('bagidata.Layout.Home', () => Home , store , Provider);

  Navigation.registerComponent('bagidata.Layout.Home.EachPromotion' , ()=> HomeEachPromotion , store , Provider);
  Navigation.registerComponent('bagidata.Layout.Home.PopupCategory' , ()=> HomePopupCategory , store , Provider);
  Navigation.registerComponent('bagidata.Layout.Home.Buy', () => HomeBuy , store , Provider);
  Navigation.registerComponent("bagidata.Layout.Home.ChargePulsa" , () => HomeBuyChargePulsa  , store , Provider);
  Navigation.registerComponent("bagidata.Layout.Home.PackageData" , () => HomeBuyPackageData  , store , Provider);
  Navigation.registerComponent("bagidata.Layout.Home.Gopay" , () => HomeBuyGoPay  , store , Provider);
  Navigation.registerComponent("bagidata.Layout.Home.EachPagePayment" , () => HomeEachPayment , store , Provider);  
  Navigation.registerComponent('bagidata.Layout.Home.Pay', () => HomePayment , store , Provider);
  Navigation.registerComponent('bagidata.Layout.Home.Recommend' , ()=> HomeRecommend , store , Provider);
  Navigation.registerComponent('bagidata.Layout.Home.EachCategory' , ()=> HomeEachCategory , store, Provider);
  Navigation.registerComponent('bagidata.Layout.Home.SearchPromotion', ()=> HomeSearchPromotion , store , Provider );
  Navigation.registerComponent('bagidata.Layout.Home.Transfer', () => HomeTransfer , store , Provider);
  Navigation.registerComponent('bagidata.Layout.Home.Favorite', () => HomeFavorite , store , Provider);
  Navigation.registerComponent('bagidata.Layout.Home.DetailStatus', () => HomeDetailStatus, store, Provider)
  Navigation.registerComponent('bagidata.Layout.Home.Achievement', () => HomeAchievement, store, Provider)
  Navigation.registerComponent('bagidata.Layout.Home.Transaction', () => HomeTransaction, store, Provider)
  
  Navigation.registerComponent("bagidata.Layout.Connection" , ()=> Connection , store , Provider);
  Navigation.registerComponent("bagidata.Layout.Connection.PopupReceipt" , ()=> ConnectionPopupReceipt , store , Provider);
  Navigation.registerComponent("bagidata.Layout.Connection.PopupSocialMedia" , ()=> ConnectionPopupSocialMedia , store , Provider);
  Navigation.registerComponent("bagidata.Layout.Connection.PopupAccessPromotion" , ()=> ConnectionPopupAccessPromotion , store , Provider);
  Navigation.registerComponent("bagidata.Layout.Connection.PopupDataControl" , ()=> ConnectionPopupDataControl , store , Provider);
  Navigation.registerComponent("bagidata.Layout.Connection.UploadReceipt" , ()=> ConnectionUploadReceipt , store , Provider);
  Navigation.registerComponent("bagidata.Layout.Connection.PageWarning" , ()=> ConnectionPageWarning , store , Provider);
  Navigation.registerComponent("bagidata.Layout.Connection.Camera" , ()=> ConnectionCamera , store , Provider);
  Navigation.registerComponent("bagidata.Layout.Connection.DataControl" , ()=>ConnectionDataControl , store , Provider);
  Navigation.registerComponent("bagidata.Layout.Connection.AuthCamera" , ()=>ConnectionAuthCamera , store , Provider);
  Navigation.registerComponent("bagidata.Layout.Connection.ConnectSosmed" , ()=>ConnectionConnectSosmed , store , Provider);
  
  Navigation.registerComponent("bagidata.Layout.History" , () => HistoryApp , store , Provider );
  Navigation.registerComponent("bagidata.Layout.History.Transfer" , () => HistoryAppTransfer , store , Provider );
  Navigation.registerComponent("bagidata.Layout.History.Bill" , () => HistoryAppBill , store , Provider );
  Navigation.registerComponent("bagidata.Layout.History.Buy" , () => HistoryAppBuy , store , Provider );
  Navigation.registerComponent("bagidata.Layout.History.UploadReceipt" , () => HistoryAppUploadReceipt , store , Provider );

  Navigation.registerComponent("bagidata.Layout.Help" , () => Help , store , Provider );
  Navigation.registerComponent("bagidata.Layout.Help.Award" , () => HelpAward , store , Provider );
  Navigation.registerComponent("bagidata.Layout.Help.Facebook" , () => HelpFacebook , store , Provider );
  Navigation.registerComponent("bagidata.Layout.Help.Instagram" , () => HelpInstagram , store , Provider );
  Navigation.registerComponent("bagidata.Layout.Help.Note" , () => HelpNote , store , Provider );
  Navigation.registerComponent("bagidata.Layout.Help.Provider" , () => HelpProvider , store , Provider );
  Navigation.registerComponent("bagidata.Layout.Help.Twitter" , () => HelpTwitter , store , Provider );
  Navigation.registerComponent("bagidata.Layout.Help.Lock" , () => HelpLock , store , Provider );
  Navigation.registerComponent("bagidata.Layout.Help.EachPage" , () => HelpEachPage , store , Provider );

  Navigation.registerComponent("bagidata.Layout.Profile" , ()=> Profile , store , Provider);
  Navigation.registerComponent("bagidata.Layout.Profile.Setting" , ()=> ProfileSetting , store , Provider);
  Navigation.registerComponent("bagidata.Layout.Profile.Account" , ()=> ProfileAccount , store , Provider);
  Navigation.registerComponent("bagidata.Layout.Profile.Others" , ()=> ProfileOthers , store , Provider);


  Navigation.registerComponent("bagidata.Component.Loading" , ()=> Loading , store , Provider);
  Navigation.registerComponent("bagidata.Component.Home.PromoAlert" , ()=> HomePromoAlert , store , Provider);

}


