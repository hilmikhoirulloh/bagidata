import React, {Component} from 'react';

import Header from "../components/header";

import {Container, Content, ListItem, List, Left, Body, Right} from "native-base";
import {Text, Image} from "react-native";

import  {connect} from "react-redux";
import {Styles} from "../../../styles";

class Account extends Component{
    handleBack = ()=>{
        this.props.navigator.pop({
            animationType:"none"
        })
    } 
    handleChangePage = (page) =>{
        this.props.navigator.push({
            screen : `bagidata.Layout.Profile.${page}`,
            animationType : "none"
        })
    }
    render(){
        return(
            <Container>
                <Header onBack={this.handleBack}/>
                <Content>
                    <List>
                        <ListItem  itemHeader first style={Styles.ListItem}>
                            <Text>Notifikasi</Text>
                        </ListItem>
                        <ListItem onPress={()=>{this.handleChangePage("Others")}} itemHeader first style={Styles.ListItem}>
                            <Text>Lainnya</Text>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        )
    }
}

export default connect()(Account);