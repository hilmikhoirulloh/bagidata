import React, {Component} from 'react';

import Header from "../components/header";

import {Container, Content, ListItem, List, Left, Body, Right} from "native-base";
import {Text, Image} from "react-native";

import  {connect} from "react-redux";
import {Styles} from "../../../styles";

class Setting extends Component{
    handleBack = ()=>{
        this.props.navigator.dismissModal({
            animationType:"none"
        })
    } 
    handleChangePage = (page) =>{
        this.props.navigator.push({
            screen : `bagidata.Layout.Profile.${page}`,
            animationType : "none"
        })
    }
    render(){
        return(
            <Container>
                <Header onBack={this.handleBack}/>
                <Content>
                    <List>
                        <ListItem  itemHeader first style={Styles.ListItem}>
                            <Left >
                                <Image source={require("../../../../../../img/icon/shered.png")} style={{height:52/3 , width:52/3}} resizeMode="stretch"/>
                            </Left>
                            <Body style={{marginLeft:-190}}>
                                <Text style={{color:"#aaa"}}>Undang Teman</Text>
                            </Body>
                            <Right>
                               <Text style={[Styles.fontBold , {color:"#aaa"}]}>Segera</Text>
                            </Right>
                        </ListItem>
                        <ListItem  itemHeader first style={Styles.ListItem}>
                            <Left >
                                <Image source={require("../../../../../../img/icon/lock.png")} style={{height:68/3 , width:46/3}} resizeMode="stretch"/>
                            </Left>
                            <Body style={{marginLeft:-190}}>
                                <Text>Kata Sandi</Text>
                            </Body>
                            <Right/>
                        </ListItem>
                        <ListItem  itemHeader first style={Styles.ListItem} onPress={()=>{this.handleChangePage("Account")}}>
                            <Left />
                            <Body style={{marginLeft:-190}}>
                                <Text>Akun</Text>
                            </Body>
                            <Right/>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        )
    }
}

export default connect()(Setting);