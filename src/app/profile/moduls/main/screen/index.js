import React , {Component } from "react";
import {View , Text ,TouchableOpacity , ProgressBarAndroid , Image, RefreshControl } from "react-native";
import {Container  , Content , Thumbnail , H2 ,  H3 , Grid , Row , Col} from "native-base";
import {Styles, COLOR_SECONDARY} from "../../../styles";
import {GetProfile} from "../action";


import Header from "../components/header";

import Collapse from '../components/collapse';

import {connect } from "react-redux";
import {DATA_PROFILE} from "../variables";
import { GetProfileReducer } from "../reducers";

class Main extends Component{
    constructor(props){
        super(props);
        this.props.navigator.setStyle({
            navBarHidden : true
        })
    }
    componentWillMount(){
        this.props.navigator.setStyle({
            navBarHidden : true
        })
        // this.props.dispatch(GetProfile(1))
    }
    
    handleChangePage = (page)=>{
        this.props.navigator.showModal({
            screen  :`bagidata.Layout.Profile.${page}`
        })
    }
    render(){
        return(
            <Container style={Styles.bgWhite} >
                <Header onChangePage={this.handleChangePage} />
                <Content
                    refreshControl={
                        <RefreshControl
                            style={{backgroundColor: '#E0FFFF'}}
                            refreshing={false}
                            tintColor="#ff0000"
                            title="Loading..."
                            titleColor="#00ff00"
                            colors={[COLOR_SECONDARY]}
                            progressBackgroundColor="white"
                        />}
                >
                    <View style={[Styles.bgGrey ]}>
                        <Grid>
                            <Row alignItems="center" justifyContent="center" style={{paddingVertical:10}}>
                                <Thumbnail source={{uri : "http://www.portoalegre.travel/upload/b/157/1570178_wallpaper-%E0%B8%AB%E0%B8%B4%E0%B8%99%E0%B8%AD%E0%B9%88%E0%B8%AD%E0%B8%99.jpg"}} circular large/>
                            </Row>
                            <Row alignItems="center" justifyContent="center" style={{paddingVertical:10}}>
                                <H3>
                                    Hilmi Khoirulloh
                                </H3>
                            </Row>
                        </Grid>
                    </View>
                    <View style={{padding:20 , borderBottomWidth:5 , borderBottomColor:"#f8f8f8"}}>
                        <Grid>
                            <Row>
                                <Col style={{flex:3.5}} justifyContent="center" >
                                    <Row>
                                        <Image source={require("../../../../../../img/icon/money_medium.png")} style={{width:57/2 , height:51/2}}/>
                                    </Row>
                                    <Row>
                                        <View style={{ borderRadius:10 , overflow:"hidden" , flex:1 }}>
                                            <ProgressBarAndroid
                                                styleAttr="Horizontal"
                                                indeterminate={false}
                                                progress={0.5}
                                                style={{transform: [{ scaleX: 1.0 }, { scaleY: 2.5 }]  }}    
                                            />
                                        </View>
                                    </Row>
                                    
                                </Col>
                                <Col style={{flex:1}} >
                                    <H2 style={{textAlign:"right" , marginTop:20}}>
                                        100%
                                    </H2>
                                </Col>
                            </Row>
                        </Grid>
                    </View>
                    <Collapse render={DATA_PROFILE} />       
                </Content>
            </Container>
        )
    }
}

const mapStateToProps = (state)=>({
    profile : state.GetProfileReducer.results.data  
})
export default connect(mapStateToProps)(Main);
