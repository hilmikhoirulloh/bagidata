import React , {Component} from "react";
import { Text  , TouchableOpacity} from "react-native";
import { Icon ,  Separator   } from "native-base";
import ListItem from "./listItem";

import {Styles} from "../../../styles";
import {Collapse,CollapseHeader, CollapseBody} from 'accordion-collapse-react-native';

export default class EachCollapse extends Component{
    state = {
        active : false
    }
    handleToggle = async (data) => {
        await this.setState({
            active : data
        });
        await this.refs.collapse.setState({
            show : data
        })
        
    }
    render(){
        const {data} = this.props;   
        return(
            <Collapse  onToggle={this.handleToggle.bind(this)} ref="collapse" style={{borderBottomWidth:5 ,paddingHorizontal:20 ,  borderBottomColor:"#f8f8f8"}}>
                <CollapseHeader    style={{paddingVertical:10 , alignItems:"center" , justifyContent:"center" , flexDirection:"row"}}>
                    <Separator style={[Styles.bgWhite , {paddingLeft:0}]}>
                        <Text style={[Styles.fontBlack , Styles.fontBold]}>{data.title}</Text>
                    </Separator>
                    <Icon name={this.state.active ? "ios-arrow-up" : "ios-arrow-down" }/>
                </CollapseHeader>    
                <CollapseBody style={{marginHorizontal:-20}}>
                    {data.value.map((data_children,j) =>(
                        <ListItem dataArray={data_children} key={j} onPress={this.onPress}/>
                    ))}
                    
                </CollapseBody>
            </Collapse>

        )
    }
}