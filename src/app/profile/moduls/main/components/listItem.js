import React ,  {Component } from "react" ; 
import {Text, Image , TouchableOpacity }  from "react-native";
import {ListItem , Left ,Body , Right , Icon } from "native-base";
import {AutoComplete , DatePicker, EmailInput, NumberInput, Selected, TextInput} from  "../components/form/index"

import {COLOR_PRIMARY} from "../../../styles"

export default class ListItemComponent extends Component{
    
    state = {
        hasPress : false
    }
    typeInput(type , name , placeholder){
        switch (type) {
            case "email": 
                return (
                    <EmailInput
                        name = {name}
                        placeholder = {placeholder}
                    />
                )
            case "number":
                return(
                    <NumberInput
                        name = {name}
                        placeholder = {placeholder}
                    />
                )
            case "text":
                return(
                    <TextInput
                        name = {name}
                        placeholder = {placeholder}
                    />
                )
            case "select":
                return(
                    <Selected
                        name = {name}
                        placeholder = {placeholder}
                    />
                )
            case "autocomplit":
                return(
                    <AutoComplete
                        name = {name}
                        placeholder = {placeholder}
                    />
                )
            case "date":
                return(
                    <DatePicker 
                        name = {name}
                        placeholder = {placeholder}
                    />
                )
        }
    }
    handlePress = ()=>{
        this.setState({
            hasPress :!this.state.hasPress
        })
    }
    render(){
        const { dataArray } = this.props;
        // if(this.state)
        let buttonCondition = false;
        if(this.state.hasPress){
            buttonCondition = <Icon active type="FontAwesome" name="save" style={{color:COLOR_PRIMARY}}  />

        }else{
            if(!dataArray.value){
                buttonCondition = <Icon active name="ios-add-circle-outline" style={{color:"#f44336"}}  />    
            }else{
                buttonCondition = <Icon active name="ios-checkmark-circle-outline" style={{color:"#64dd17"}}  />
            }
        }
        return(
            <ListItem icon onPress={this.handlePress.bind(this)} style={{borderBottomColor:"#eee" , borderBottomWidth:1 ,height:60 ,  marginLeft:-10 , paddingLeft:30}}>
                <Left style={{borderBottomWidth:0 , width:40}}>
                    <Image source={dataArray.icon} style={{width: dataArray.width_icon/3 , height:dataArray.height_icon/3}} resizeMode="stretch"  />
                </Left>
                <Body style={{borderBottomWidth:0}}>
                    {
                        this.state.hasPress ?
                            this.typeInput(dataArray.type , dataArray.name , dataArray.stetment , dataArray.value )
                        :
                            <Text>{dataArray.title}</Text>
                    }
                </Body>
                <Right style={{borderBottomWidth:0 , height:"100%" }}>
                    <TouchableOpacity >
                        {buttonCondition}
                    </TouchableOpacity>
                </Right>
            </ListItem>
        )
    }
}