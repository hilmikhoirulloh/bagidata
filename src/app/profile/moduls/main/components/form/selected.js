import React , {Component } from "react";
import {View} from "react-native";

import {Form} from "native-base";
import { reduxForm , Field} from "redux-form";

import { Selected } from "../input";

class SelectedComponent extends Component{
    render(){
        return(
            // <View style={{marginLeft : -5}}>

                <Field
                    // name={this.props.name}
                    keyboardType="number"
                    component={Selected}
                />
            // </View>
        )
    }
} 

export default reduxForm({
    form : "selected_profil"
})(SelectedComponent)