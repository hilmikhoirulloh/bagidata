import React , {Component } from "react";
import {View} from "react-native";
import {Form} from "native-base";
import { reduxForm , Field} from "redux-form";

import { InputText } from "../input";

class AutoCompleteComponent extends Component{
    render(){
        return(
            // <View style={{marginLeft : -5}}>
                <Field
                    name={this.props.name}
                    placeholder={this.props.placeholder}
                    component={InputText}
                />
            // </View>
        )
    }
} 

export default reduxForm({
    form : "autocompleted_profil",
    asyncChangeValidate : ["autocomplete"]
})(AutoCompleteComponent)