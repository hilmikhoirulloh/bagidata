import React , {Component } from "react";
import {View} from "react-native";

import {Form} from "native-base";
import { reduxForm , Field} from "redux-form";

import { InputText } from "../input";

class TextComponent extends Component{
    render(){
        return(
            <Field
                name={this.props.name}
                placeholder = {this.props.placeholder}
                component={InputText}
                style={{marginLeft : -5}}
            />
        )
    }
} 

export default reduxForm({
    form : "text_profil",
})(TextComponent)