import React , {Component } from "react";
import {View} from "react-native";
// import {Form} from "react-native";
import {Form} from "native-base";
import { reduxForm , Field} from "redux-form";

import { InputDatePicker } from "../input";

class DatePickerComponent extends Component{
    render(){
        return(
            // <View style={{marginLeft : -5}}>
                <Field
                    name={this.props.name}
                    placeholder = {this.props.placeholder}
                    component={InputDatePicker}
                />
            // </View>
        )
    }
} 

export default reduxForm({
    form : "datepicker_profil"
})(DatePickerComponent)