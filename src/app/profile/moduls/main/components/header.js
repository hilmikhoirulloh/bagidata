import React ,{Component} from "react";
import {TouchableOpacity} from "react-native";
import {Header, Body, Title, Right, Icon} from "native-base";
import {Styles} from "../../../styles";

export default class HeaderComponent extends Component{
    render(){
        const {onChangePage} = this.props;
        return(
            <Header style={[Styles.bgWhite , {marginBottom: 3}]}>
                <Body style={{marginLeft:10}}>
                    <Title style={Styles.fontBlack}>
                        Profil
                    </Title> 
                </Body> 
                <Right style={{paddingRight:10}}>
                    <TouchableOpacity onPress={()=>{onChangePage("Setting")}}>
                        <Icon name="md-more" />
                    </TouchableOpacity>
                </Right>
            </Header>
        )
    }
}