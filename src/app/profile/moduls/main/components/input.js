import React ,{Component} from "react"
import {Item, Icon, Input,  Picker,  DatePicker} from "native-base"
import {Styles  , COLOR_PRIMARY} from "../../../styles";
import {View , Text} from "react-native";

export class InputText  extends Component{
    render(){
        const {input , placeholder , meta , keyboardType}  = this.props;
        return(
            <Input {...input} 
                keyboardType={keyboardType}  
                placeholder={placeholder} 
                style={{fontSize:14 , color:"#444", marginLeft:-5 }} 
                placeholderTextColor="#aaa"
                autoFocus={true}
            />
        )
    }
}

export class Selected extends Component{
    constructor(props) {
        super(props);
        this.state = {
          selected2: undefined
        };
    }
    onValueChange2(value) {
        this.setState({
            selected2: value
        });
    }
    render(){
        const {input , keyboardType , data , meta}  = this.props;
        return(
            <Item picker>
                <Picker
                    // {...input}
                    textStyle={{fontSize: 12}}
                    mode="dropdown"
                    iosIcon={<Icon name="ios-arrow-down-outline" />}
                    style={{ color:"#444" ,  flex: 1 , fontSize:10}}
                    
                    itemTextStyle={{ fontSize: 18, color: 'white' }}
                    selectedValue={this.state.selected2}
                    onValueChange={this.onValueChange2.bind(this)}
                >
                    <Picker.Item label="Wallet" value="key0" style={{color:"red"}}/>
                    <Picker.Item label="ATM Card" value="key1" />
                    <Picker.Item label="Debit Card" value="key2" />
                    <Picker.Item label="Credit Card" value="key3" />
                    <Picker.Item label="Net Banking" value="key4" />
                </Picker>
            </Item>
        )
    }
}

export class InputDatePicker extends Component{
    constructor(props) {
        super(props);
        this.state = { chosenDate: new Date() };
        this.setDate = this.setDate.bind(this);
    }
    setDate(newDate) {
    this.setState({ chosenDate: newDate });
    }
    render(){
        const {input ,name , meta , placeholder}  = this.props;
        return(
            <View style={{marginLeft:-10}}>
                <DatePicker
                    {...input}
                    defaultDate={new Date(2018, 4, 4)}
                    minimumDate={new Date(2018, 1, 1)}
                    maximumDate={new Date(2018, 12, 31)}
                    locale={"en"}
                    timeZoneOffsetInMinutes={undefined}
                    modalTransparent={false}
                    animationType={"fade"}
                    androidMode={"default"}
                    placeHolderText={placeholder}
                    textStyle={{fontSize:14  , color:"#444"  , fontWeight:"100" }}
                    placeHolderTextStyle={{fontSize:14 , color:"#aaa"  , fontWeight:"100" }}
                    onDateChange={this.setDate}
                    />
            </View>
        )
    }
}
