import axios from "axios";
import {URL_API} from "../../../../../config";

export function GetProfile(id){
    return {
        type : "GET_PROFILE",
        payload : axios.get(`${URL_API}/api/v1/profile/${id}`)
    }
}