export const DATA_PROFILE = [
    {
        title : "Informasi Umum",
        value :
        [
            {
                icon : require("../../../../../img/icon/profile_email.png"),
                width_icon : 52 ,
                height_icon : 50 ,
                title : "E-mail",
                name : "email",
                value : "hilmi" ,
                type : "email",
                stetment : "Masukan email"  
            },
            {
                icon : require("../../../../../img/icon/phone.png"),
                width_icon :40 ,
                height_icon :68,
                title : "Nomer Telepon",
                name : "nomer_telepon",
                value : false ,
                type : "number",
                stetment : "Masukan telpon"  

            },
            {
                icon : require("../../../../../img/icon/bithday.png"),
                width_icon :52 ,
                height_icon :47,
                name : "tempat_lahir",
                title : "Tempat Lahir",
                value : false ,
                type : "autocomplit",
                stetment : "Masukan tempat lahir" 
            },
            {
                icon : require("../../../../../img/icon/bithday.png"),
                width_icon :52 ,
                height_icon :47,
                title : "Tanggal Lahir",
                name : "tanggal_lahir",
                value : false ,
                type : "date"   ,
                stetment : "Masukan tanggal lahir"
            },
            {
                icon : require("../../../../../img/icon/identity.png"),
                width_icon : 68 ,
                height_icon :52 ,
                title : "Identitas",
                name : "identitas",
                value : false ,
                type : "select",
                stetment : "Masukan identitas"

            } 
        ]
    }
]