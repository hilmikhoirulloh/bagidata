const getProfileState = {
    isLoading : false,
    results : [],
    error : false
}


export const GetProfileReducer = (state = getProfileState , action) => {
    switch(action.type){
        case "GET_PROFILE_PENDING" :
            return {...state, isLoading : true, error : false}
        case "GET_PROFILE_FULFILLED" :
            return {...state, results : action.payload.data, error : false, isLoading: false}
        case "GET_PROFILE_REJECTED" : 
            return {...state, isLoading: false, error: true, results:[]}
        default:
            return state    
    }
} 