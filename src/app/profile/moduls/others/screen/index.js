import React, {Component} from 'react';

import Header from "../components/header";

import {Container, Content, ListItem, List, Left, Body, Right} from "native-base";
import {Text, Image} from "react-native";

import  {connect} from "react-redux";
import {Styles} from "../../../styles";

class Others extends Component{
    handleBack = ()=>{
        this.props.navigator.pop({
            animationType:"none"
        })
    } 
    handleChangePage = (page) =>{
        this.props.navigator.push({
            screen : `bagidata.Layout.Profil.${page}`,
            animationType : "none"
        })
    }
    render(){
        return(
            <Container>
                <Header onBack={this.handleBack}/>
                <Content>
                    <List>
                        <ListItem  itemHeader first style={Styles.ListItem}>
                            <Text>Hapus Data</Text>
                        </ListItem>
                        <ListItem  itemHeader first style={Styles.ListItem}>
                            <Text>Hapus Akun</Text>
                        </ListItem>
                        <ListItem itemHeader first style={Styles.ListItem}>
                            <Text>Keluar</Text>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        )
    }
}

export default connect()(Others);