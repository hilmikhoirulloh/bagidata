const walletState = {
    result : null,
    isLoading : false,
    error : false
}

export function WalletReducer(state = walletState ,action){
    switch(action.type){
        case "GET_WALLET_PENDING" :
            return {...state,  isLoading :true, error : false}
        case "GET_WALLET_FULFILLED" :
            return {...state,  result : action.payload.data[0] , isLoading:false , error :false}
        case "GET_WALLET_REJECTED" :
            return {...state,  isLoading:false , error :true, result:[]}        
        default :
            return state
    }
}