const _navState = {
    root : undefined
}

const Nav = (state = _navState  , action)=>{
    switch(action.type){
        case "NAV_REDUCER":
            return {...state,root : action.page}
       
        default:
            return state
    }
};
export default Nav; 