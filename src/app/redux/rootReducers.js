// import { combineReducers } from 'redux';
import { persistCombineReducers } from "redux-persist";


import storage from "redux-persist/lib/storage";
import reducers from "./reducers";

const config = {
  key: 'bagidata',
  storage,
  whitelist :[
    "root",
    "LoginAuthReducer",
    "HandleIconHomeReducer"
  ]
}
const appReducer = persistCombineReducers(
  config , 
  reducers
);

export default appReducer
