import {LoginAuthReducer } from '../authentication/moduls/login/reducers';
import {RegisterAuthReducer}from '../authentication/moduls/register/reducers';
import {ForgetPasswordAuthReducer}from '../authentication/moduls/forget-password/reducers';
import {VerifyCodeReducer , ResendOtpReducer}from '../authentication/moduls/verify-code/reducers';
import { SetPasswordReducer} from "../authentication/moduls/change-password/reducers";
import {HandleIconHomeReducer} from "../home/moduls/recommend/reducers";
import {SearchDataControlReducer}from '../connection/moduls/data-control/reducers';


import { reducer as form } from 'redux-form';

import Nav from "./_nav";
import {WalletReducer} from "./global";

import {GetProfileReducer} from "../profile/moduls/main/reducers";

import {GetIdFilterPromotion} from "../home/moduls/main/reducers";

export default {
    form, 
    root : Nav,
    ForgetPasswordAuthReducer , 
    LoginAuthReducer , 
    RegisterAuthReducer , 
    VerifyCodeReducer,
    SetPasswordReducer,
    HandleIconHomeReducer, 
    SearchDataControlReducer,
    WalletReducer,
    ResendOtpReducer,
    GetIdFilterPromotion,
    GetProfileReducer
};