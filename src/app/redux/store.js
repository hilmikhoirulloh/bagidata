import {applyMiddleware , createStore} from "redux"
import rootReducer  from "./rootReducers";
import promise from "redux-promise-middleware";
import logger from "redux-logger";
import thunk from "redux-thunk";

import { persistReducer , persistStore , persistCombineReducers} from "redux-persist"; 


    const store = createStore(
        rootReducer , 
        applyMiddleware(promise(), thunk)
    );

    const persistor = persistStore(store , ()=>{
        store.getState()
    });
    
export { 
    persistor , 
    store
};

