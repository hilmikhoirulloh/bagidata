import React , {Component} from "react";
import {Icon} from "native-base";
import {View , Text , TouchableOpacity } from "react-native";
import {Styles  , HEIGHT_WINDOW , WIDTH_WINDOW } from "../styles" ;


const HEIGHT = HEIGHT_WINDOW * 50 / 100 ;
const WIDTH = WIDTH_WINDOW * 90 / 100 ;
export default class Alert extends Component {
    constructor(props){
        super(props);
    }
    handleClose = ()=>{
        this.props.navigator.dismissLightBox();
    }
    render(){
        const {title, body, button, onClose} = this.props.data()
        return(
            <View style={[Styles.bgWhite , {width:300 , height:200, borderRadius:15 }]}>
                <Icon name="close" fontSize="16" style={{position:"absolute" , top:8 , right:8 , fontSize:17 }} onPress={() => {this.handleClose(); onClose ? onClose() : null; }}/>
                
                <View style={{top:30, left:20, right:20 ,position:"absolute"}}>
                    <Text style={{fontSize:17 , color:"#444" , fontFamily : "Barlow-SemiBold"}}>
                        {title}
                    </Text>
                    <Text style={{fontSize:16 }}>
                        {body}
                    </Text>
                </View>

                <View style={{position:"absolute", bottom:3, left:0, right:10, flexDirection:"row", alignItems:"flex-end", justifyContent:"flex-end"}}>
                    {button.map((data, i)=>(

                        <TouchableOpacity key={i} style={{padding:10 }} onPress={()=>{data.onPress()}}>                    
                            <Text style={[{fontFamily:"Barlow-Light", fontSize:16} ,data.style]}>
                                {data.name}
                            </Text>
                        </TouchableOpacity>
                    ))}
                    
                </View>
            </View>
        )
    }
} 