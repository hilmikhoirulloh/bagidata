import {StyleSheet , Dimensions} from "react-native"

export const SCALE_RESIZE_IMAGE = 1.1 ;
export const WIDTH_WINDOW = Dimensions.get("window").width ;
export const HEIGHT_WINDOW = Dimensions.get("window").height ;
export const COLOR_PRIMARY = "#ec008c";

export const Styles = StyleSheet.create({
    Center : {
        justifyContent : "center",
        alignItems:"center",
    },
    bgPrimary : {
        backgroundColor :COLOR_PRIMARY  
    },
    fontButton : {
        color:"white" , 
        fontSize:15,
        fontFamily : "Barlow-SemiBold"
    },
    justifyEnd: {
        justifyContent:"flex-end"
    },
    justifyStart: {
        justifyContent:"flex-start"
    },
    wrapperBackground: {
        position : "absolute" , 
        top:-60 , 
        left : -10 , 
        right : 0 , 
        height: HEIGHT_WINDOW 
    },
    background : {
        flex: 1 , 
        width:null , 
        height:200 
    },
    flat : {
        elevation:0
    },
    borderNone : {
        borderColor  : "transparent"
    },
    bgGrey : { 
        backgroundColor : "#f8f8f8"
    },
    inputText : {
        paddingHorizontal:5,    
    },
    textCenter : {
        textAlign: "center"
    },
    bottom : {
        position :"absolute", 
        bottom:0 , 
        right:0 , 
        left:0
    },
    textLeft : {
        textAlign: "left"
    },
    textRight : {
        textAlign: "right"
    },
    Image : {
        height : 55 * SCALE_RESIZE_IMAGE,
        width: 200 * SCALE_RESIZE_IMAGE
    },
    linearGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 5
    },
    button : {  
    }, 
    inputText : {
        paddingHorizontal:5
    },
    buttonText: {
        fontSize: 18,
        fontFamily: 'Gill Sans',
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        backgroundColor: 'transparent',
    },
    coverAndroid:{
        position :"absolute", 
        bottom:0 , 
        right:0 , 
        left:0,
        top : 0,
        alignItems:"center",
        justifyContent:"center"
    },
    bgWhite : {
        backgroundColor : "white"
    },
    fontBlack : {
        color: "#444"
    },
    fontBold : {
        fontFamily:"Barlow-SemiBold" , 
        fontSize : 18 
    },
    textCenter : {
        textAlign : "center"
    }
})
