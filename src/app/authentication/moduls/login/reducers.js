
const AuthState = {
    results: [],
    isLoading : false,
    error : false
}
 
export const LoginAuthReducer = (state = AuthState, action) => {
    switch (action.type) {
        case 'AUTH_LOGIN_REDUCER_PENDING':
            return {...state, isLoading : true, error:false}
        case 'AUTH_LOGIN_REDUCER_FULFILLED':
            return {...state  , results: action.payload.data  ,isLoading:false, error:false}
        
        case 'AUTH_LOGIN_REDUCER_REJECTED':
            return {...state , isLoading:false ,results:[],  error : true}
        
        default:
            return state
    }
}
  