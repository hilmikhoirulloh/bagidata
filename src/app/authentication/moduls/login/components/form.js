import React , {Component} from "react";

import { InputText } from "./input"; 
import validate from "../validate";
import asyncValidate from '../asyncValidate';

import {Field , reduxForm } from "redux-form";
import { Form as NForm} from "native-base"

class Form extends Component {
    icon = [ "ios-eye-outline" , "ios-eye-off-outline" ]
    state = {
        numIcon : 0,
        icon : "ios-eye-outline", 
    }
    changeIcon(){
        const numIcon = (this.state.numIcon+1)%2;    
        this.setState({
            numIcon ,
            icon : this.icon[numIcon]
        })

    }
    render(){
        const { handleSubmit} = this.props ;    
        const {icon , numIcon} = this.state;
        return(
            <NForm onSubmit={handleSubmit} style={{marginTop:30 , paddingHorizontal:10}}>
                <Field
                    name ="username"
                    hash = {false}
                    width = {25/1.5}
                    height = {30/1.5}
                    source = {require("../../../../../../img/icon/person.png")}
                    placeholder = "E-mail atau No Telepon"
                    component={InputText}
                /> 
                <Field
                    name ="password"
                    placeholder = "Kata Sandi"
                    hash= {!Boolean(numIcon)}
                    width = {23/1.5}
                    height = {30/1.5}
                    iconHash = {icon}
                    clickIcon = {this.changeIcon.bind(this)}
                    source = {require("../../../../../../img/icon/lock.png")}
                    component={InputText}
                />
                

            </NForm>
        )
}
  }
  
  
  export default reduxForm({
    form: 'login',
    validate,
    // asyncValidate,  
    asyncBlurFields: ['username']
  })(Form);