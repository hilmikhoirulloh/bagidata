import React from 'react';
import {Image , View , TouchableOpacity , Text} from "react-native";
import {Item , Icon , Input  } from "native-base"
import {Styles , COLOR_PRIMARY} from "../../../styles";
export function InputText({ 
    input, 
    placeholder ,
    hash,
    source,
    width,
    height,
    iconHash,
    clickIcon,
    meta: {  touched, error } 
  }){
    let hasError= false;
    if(error !== undefined && touched){
      hasError= true;
    }
    return( 
      <View>
        
        <View style={{paddingBottom:5}}>
          <Item  rounded style={[Styles.bgGrey , Styles.borderNone , Styles.inputText ]}>
            <Image source={source} style={{width , height, marginLeft:10}} resizeMode="stretch"/>
            <Input {...input}  placeholder={placeholder} secureTextEntry={hash} style={{fontSize:14 , color:"#444" , paddingLeft:15, fontFamily:"Barlow-Regular" }} placeholderTextColor="#aaa"/>
            {iconHash ?
            <TouchableOpacity onPress={clickIcon}>
              <Icon name={iconHash}/>        
            </TouchableOpacity>
            :
            <View />
            }
          </Item>
        </View>
        {hasError ? <Text style={{fontSize:10 , marginLeft:50 , color : COLOR_PRIMARY , paddingBottom:5}}>{error}</Text> : <Text />}
        
      </View>

    )
  }