import React , {Component} from "react";

import  {Image  , TouchableOpacity , View , Text , BackHandler } from "react-native" ;
import { Row , Container , Content , Grid ,  Button} from "native-base";
import {Styles , COLOR_PRIMARY  } from "../../../styles.js";


import { connect } from "react-redux";
import {handleSignIn} from "../actions";
import Form from "../components/form";
import Loading from "../components/loading";

import {changeAppInitialized} from "../../../../../actionApp"
const TAKE_AWAY = "../../../../../../";
class Login extends Component {
  constructor(props){
    super(props); 
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  hasLogin = 0;

  onNavigatorEvent(event) {
    
    switch (event.id) {
      case 'willAppear':
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        break;
      case 'willDisappear':
        this.backPressed = 0;
        this.backHandler.remove();
        break;
      default:
        break;
    }
  }
  
  handleBackPress = () => {
    this.props.navigator.pop({
      animated: false
    });
    return true;
  }

  handleWasSubmit = async (values) => {
    await this.handleHasLogin()
    
    if(this.hasLogin >= 5){
      this.props.navigator.resetTo(
        { 
          screen : `bagidata.Register` ,
          animationType: 'none'
        }
      )
    }else{
      await this.props.dispatch(handleSignIn(this.props.form.values)); 
      if(!this.props.auth.error){
        if(this.props.auth.results.success){
          this.props.dispatch(changeAppInitialized("after-login"))
        }
      }else{
        data = {
          title : "Koneksi tidak stabil",
          body : "Silakan cek kembali jaringan anda",
          button : [
              {
                name : "Oke",
                style : {
                  color:COLOR_PRIMARY
                },
                onPress : this.handleOke
              }
            ]
        }
        this.openMessage(data) 
      }
    }
  };

  openMessage(data) {
    this.props.navigator.showLightBox({
      screen :"bagidata.Auth.Alert",
      passProps : {
        data : ()=> (data)
      },
      style: {
        backgroundColor:"#00000080"
      }
    })
  }
  handleBack = () =>{
    this.props.navigator.resetTo(
      { 
        screen : `bagidata.Register` ,
        animationType: 'none'
      }
    )  
  }
  handleChangePage = (page)=>{
    this.props.navigator.resetTo(
      { 
        screen : `bagidata.${page}` ,
        animationType: 'none'
      }
    )
  }
  handleHasLogin(){
    this.hasLogin++;
  }
  render(){
    let error = false;
    if(this.props.form){
      if(this.props.form.syncErrors){
        error = true;
      }else{
        error = false;
      }   
    }else{
      error = false;
    }
    return(
      <Container style={{backgroundColor:"white" }} >
        <View style={Styles.wrapperBackground}>
          <Image source={require(`${TAKE_AWAY}img/background/auth.png`)} style={Styles.background}/>
        </View>
        {this.props.auth.isLoading ? 
          <Loading  />
        :
          null
        }
       
        <Content padder > 
          <Grid >            
            <Row justifyContent="center" alignItems="center" >
              <View style={{width:182, marginVertical:105}}>
                <Image source={require(`${TAKE_AWAY}img/brand.png`)} style={[Styles.background , { height:52}]} resizeMode="stretch"/>
              </View>
            </Row>
            
            
            <Form onSubmit={this.handleWasSubmit.bind(this)} ref="form"/>
             
          </Grid>
              
          <Grid style={{marginTop:10}}>
              <Row alignItems="center" justifyContent="center">
                  <View style={{width:"70%"}}>
                      <Button full  rounded disabled={
                        this.props.form ? 
                        Boolean(this.props.form.syncErrors) 
                        : 
                        true 
                        } style={[{backgroundColor: error ? "#aaa"  : COLOR_PRIMARY} , Styles.flat  ]} onPress={()=>{this.handleWasSubmit()}}>
                        <Text style={Styles.fontButton}>Masuk</Text>
                      </Button>
                  </View>
              </Row>
              <Row alignItems="center" justifyContent="center" style={{marginVertical:10}}>
                <TouchableOpacity onPress={()=> {this.handleChangePage("ForgetPassword")}}>
                  <Text style={{fontSize:13  , fontWeight: "200" , color:"#999"}}>
                    Lupa kata sandi ? 
                  </Text>
                </TouchableOpacity>
              </Row>

              <Row alignItems="center" justifyContent="center" style={{ marginTop:50}}>
                <Text style={{fontSize:13  , fontWeight: "normal" , color:"#999"}}>
                  Belum punya akun?    
                </Text>
                <TouchableOpacity onPress={()=>{this.handleBack()}}>
                  <Text style={{fontSize:13  , fontFamily: "Barlow-SemiBold" , color:COLOR_PRIMARY , paddingLeft:5}}>
                    Daftar 
                  </Text>
                </TouchableOpacity>
              </Row>
          </Grid>
        </Content>
      </Container>
    )
  }
}

const mapsStateToProps = (state) => ({
  auth : state.LoginAuthReducer , 
  form : state.form.login
})
export default connect(mapsStateToProps)(Login);