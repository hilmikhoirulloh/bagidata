import axios from "axios";
import * as Config from "../../../../../config";

export  function handleSignIn(data){
	const username = data.username;
	const password = data.password;
	return {
		type : "AUTH_LOGIN_REDUCER",
		payload : axios.post(`${Config.URL_API}/api/v1/login` ,
			{
				email : username,
				password : password
			},
			{
				timeout: 1000
			}
		)
	}	
}
