import axios from "axios";
import * as Config from "../../../../../config";


export  function AuthSignUp(username){	
	return {
		type : "AUTH_REGISTER_REDUCER",
		payload : axios.post(`${Config.URL_API}/api/v1/register` ,
			{
				email : username
			},
			{
				timeout: 1000
			}	
		)
	}
}

export function HandleSignUp(data){	
	return async (dispatch) => {
		await dispatch(AuthSignUp(data.username))
	}
}