import React , {Component} from "react";
import  {Image  , TouchableOpacity , View , Text , BackHandler} from "react-native" ;
import { Row , Container , Content , Grid , Col , Button} from "native-base"
import {Styles  ,COLOR_PRIMARY } from "../../../styles.js";
import { connect } from "react-redux";
import {HandleSignUp } from "../actions";
import Form from "../components/form";
import Loading from "../components/loading";

const TAKE_AWAY= "../../../../../../";

class Register extends Component {
  constructor(props){
    super(props);    
  }
  state = {
    username : null,
    error : false
  }
  handleOke = () => {
    this.props.navigator.dismissLightBox();
  }
  handleWasSubmit = async () => {
    this.setState({
      username : this.props.form.values.username
    })
    await this.props.dispatch(HandleSignUp(this.props.form.values))
    
    if(!this.props.auth.error){
      if(this.props.auth.results.success){
        this.handleChangePage("VerifyCode" , {
          username : this.state.username ,
          type : "register"
        },"push");
      }else{
        if(this.props.auth.results.status == 400){
          data = {
            title : "Maaf anda gagal register",
            body : "Silakan mendaftar dengan username yang berberda ",
            button : [
              {
                name : "Oke",
                style : {
                  color:COLOR_PRIMARY
                },
                onPress : this.handleOke
              }
            ]
          }
        }
      }
    }else{
      data = {
        title : "Koneksi tidak stabil",
        body : "Silakan cek kembali jaringan anda",
        button : [
          {
            name : "Oke",
            style : {
              color:COLOR_PRIMARY
            },
            onPress : this.handleOke
          }
        ]
      }
      this.openMessage(data)
    }
  };

  openMessage(data) {
    this.props.navigator.showLightBox({
      screen :"bagidata.Auth.Alert",
      passProps : {
        data : ()=> (data)
      },
      style: {
        backgroundColor:"#00000080"
      }
    })
  }
  componentWillMount(){
   
  }
  handleChangePage = (page , passProps , method )=>{
    
    this.props.navigator[method](
      { 
        screen : `bagidata.${page}`,
        animationType: 'none' , 
        passProps 
      }
    )
  }

  render(){
    let error = false;
    if(this.props.form){
      if(this.props.form.syncErrors){
        error = true;
      }else{
        error = false;
      }   
    }else{
      error = false;
    }
    return(
      <Container style={{backgroundColor:"white"}}>
        
          <View style={{flex:1}}>
            <View style={Styles.wrapperBackground}>
              <Image source={require(`${TAKE_AWAY}img/background/auth.png`)} style={Styles.background}/>
            </View>
            {this.props.auth.isLoading  ? 
              <Loading  />
            :
              null
            }
            <Content padder  > 
            
              <Grid >            
                <Row justifyContent="center" alignItems="center" >
                  
                  <View style={{width:182, marginVertical:105}}>
                    <Image source={require(`${TAKE_AWAY}img/brand.png`)} style={[Styles.background , { height:52}]} resizeMode="stretch"/>
                  </View>
                </Row>
                <Form onSubmit={this.handleWasSubmit.bind(this)} ref="form"/>
              </Grid>    
              <Grid style={{marginTop:79.5}}>
                <Row alignItems="center" justifyContent="center">
                    <View style={{width:"70%"}}>
                      
                      <Button full  rounded disabled={
                        this.props.form ? 
                        Boolean(this.props.form.syncErrors) 
                        : 
                        true 
                        } style={[{backgroundColor: error ? "#aaa"  : COLOR_PRIMARY} , Styles.flat  ]} onPress={()=>{this.handleWasSubmit()}}>
                          <Text style={Styles.fontButton}>Daftar</Text>
                      </Button>
                    </View>
                </Row>
                <Row alignItems="center" justifyContent="center" style={{ marginTop:88}}>
                  <Text style={{fontSize:13  , fontWeight: "normal" , color:"#999"}}>
                    Sudah punya akun?    
                  </Text>
                  <TouchableOpacity  onPress={()=>{this.handleChangePage("Login",{},"resetTo")}} >
                    <Text style={{fontSize:13  , fontFamily: "Barlow-SemiBold" , color:COLOR_PRIMARY , paddingLeft:5}}>
                      Masuk 
                    </Text>
                  </TouchableOpacity>
                </Row>
              </Grid>
            </Content>
          </View>       
       
      </Container>
    )
  }
}


const mapsStateToProps = (state) => ({
  auth : state.RegisterAuthReducer , 
  form : state.form.register
})
export default connect(mapsStateToProps)(Register);