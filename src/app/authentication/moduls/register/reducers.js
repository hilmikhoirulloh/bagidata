const AuthState = {
    results: [],
    isLoading : false,
    error : false
}
const GetSmsTokenState = {
    results: [],
    isLoading : false,
    error : false
}
export const RegisterAuthReducer = (state = AuthState, action) => {
    switch (action.type) {
        case 'AUTH_REGISTER_REDUCER_PENDING':
            return {...state, isLoading : true, error: false}
        case 'AUTH_REGISTER_REDUCER_FULFILLED':
            return {...state, results: action.payload.data  , isLoading:false, error: false}
        
        case 'AUTH_REGISTER_REDUCER_REJECTED':
            return {...state , results:[] , isLoading:false , error : true}
        
        default:
            return state
    }
}
  

export const GetSmsTokenReducer = (state = GetSmsTokenState, action) => {
    switch (action.type) {
        case 'GET_SMS_TOKEN_REDUCER_PENDING':
            return {...state, isLoading : true}
        case 'GET_SMS_TOKEN_REDUCER_FULFILLED':
            return {...state, results: action.payload  , isLoading:false}
        
        case 'GET_SMS_TOKEN_REDUCER_REJECTED':
            return {...state  , isLoading:false , error : true}
        default:
            return state
    }   
}

