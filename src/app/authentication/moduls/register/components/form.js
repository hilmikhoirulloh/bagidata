import React from "react";

import { InputText } from "./input"; 
import validate from "../validate";
import asyncValidate from '../asyncValidate';
import {Field , reduxForm } from "redux-form";

import { Form as NForm} from "native-base"
// import {TouchableOpacity} from "react-native";
// import {Styles , HEIGHT_WINDOW , WIDTH_WINDOW } from "../styles.js";



const Form = props => {
    const { handleSubmit, pristine, reset, submitting ,valid , anyTouched , onSubmit} = props ;    

    return(
        <NForm onSubmit={handleSubmit} style={{marginTop:30 , paddingHorizontal:10}}>
            
            <Field
                    name ="username"
                    hash = {false}
                    width = {25/1.5}
                    height = {30/1.5}
                    source = {require("../../../../../../img/icon/person.png")}
                    placeholder = "E-mail atau No Telepon"
                    component={InputText}
                />
         
        </NForm>
    )
  }
  
  
  export default reduxForm({
    form: 'register',
    validate,
    // asyncValidate,  
    // asyncBlurFields: ['username']
  })(Form);