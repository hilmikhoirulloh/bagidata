import React from 'react';
import {View , TouchableOpacity , Text , Image} from "react-native";
import {Item , Icon , Input } from "native-base"
import {Styles ,  COLOR_PRIMARY} from "../../../styles";
// import Image from "react-native-svg-uri"
export function InputText({ 
    input, 
    placeholder ,
    hash,
    source,
    width,
    height,
    iconHash,
    clickIcon,
    meta: {  touched, error } 
  }){ 
    let hasError= false;
    if(error !== undefined && touched){
      hasError= true;
    }
    return( 
      <View>
        <View style={{paddingBottom:5}}>
          <Item  rounded style={[Styles.bgGrey , Styles.borderNone , Styles.inputText ]}>
            <Image source={source} resizeMode="stretch"  style={{marginLeft:10 , height , width}} />
            <Input {...input}  placeholder={placeholder} secureTextEntry={hash} style={{fontSize:14 , fontFamily:"Barlow-Regular" , color:"#444" , paddingLeft:15 }} placeholderTextColor="#aaa" />
            {iconHash ?
            <TouchableOpacity onPress={clickIcon}>
              <Icon name={iconHash}/>        
            </TouchableOpacity>
            :
            <View />
            }
          </Item>
        </View>
      </View>

    )
  }