const validateEmail  = (email) => {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
const validatePhone = (number) => {
    var re = /^[0-9\-\+]{9,15}$/;
    return re.test(number);
}
const validate = values => {
    const errors = {};
    if (!values.username){
        errors.username = "Yuk diisi yang benar"
    }
    if (parseInt(values.username)){    
        if(!validatePhone(values.username)){
            errors.username = "Yuk diisi yang benar"
        }
    }else{
        if (!validateEmail(values.username)){
            errors.username = "Yuk diisi yang benar"        
        }
    }
    return errors;
}

export default validate;