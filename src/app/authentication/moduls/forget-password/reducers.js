const ForgetPasswordState = {
    results: [],
    isLoading : false,
    error : false
}


export const ForgetPasswordAuthReducer = (state = ForgetPasswordState, action) => {
    switch (action.type) {
        case 'FORGET_PASSWORD_REDUCER_PENDING':
            return {...state, isLoading : true, error:false}
        case 'FORGET_PASSWORD_REDUCER_FULFILLED':
            return {...state, results: action.payload.data  , isLoading:false, error :false}
        
        case 'FORGET_PASSWORD_REDUCER_REJECTED':
            return {...state , isLoading:false , error : true}
        
        default:
            return state
    }
}
  