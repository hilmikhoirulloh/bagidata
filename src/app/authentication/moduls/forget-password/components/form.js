import React from "react";

import { InputText } from "./input"; 
import validate from "../validate";
import {Field , reduxForm } from "redux-form";

import {Text , Row , Grid , Col , Item , Form as NForm, Button} from "native-base"
import {TouchableOpacity} from "react-native";
import asyncValidate from "../asyncValidate";

const Form = props => {
    const { handleSubmit, pristine, reset, submitting ,valid , anyTouched , onSubmit} = props ;    
    
     
    return(
        <NForm onSubmit={handleSubmit} style={{marginTop:30 , paddingHorizontal:10}}>
            <Field
                name ="username"
                hash = {false}
                width = {25/1.5}
                height = {30/1.5}
                source = {require("../../../../../../img/icon/mail-phone.png")}
                placeholder = "E-mail atau No Telepon"
                component={InputText}
            />  
        </NForm>
    )
  }
  
  
  export default reduxForm({
    form: 'forget_password',
    validate,
  })(Form);