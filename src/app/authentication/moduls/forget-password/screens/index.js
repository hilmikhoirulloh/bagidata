import React , {Component} from "react";

import  {Image  , Text , TouchableOpacity , View , StyleSheet, BackHandler} from "react-native" ;
import { Row , Container , Content , Grid , Col , H3 , Button } from "native-base"
import {Styles , COLOR_PRIMARY} from "../../../styles.js";

import { connect } from "react-redux";
import {HandleForgetPassword} from "../actions";
import Form from "../components/form";

import Loading from "../components/loading";

const TAKE_AWAY = "../../../../../../";
class ForgetPassword extends Component {
  state = {
    username : null
  }
  constructor(props){
    super(props);    
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    
  }
  onNavigatorEvent(event) { 
    switch (event.id) {
      case 'willAppear':
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        break;
      case 'willDisappear':
        this.backPressed = 0;
        this.backHandler.remove();
        break;
      default:
        break;
    }
  }
  handleBackPress = () => {
    this.props.navigator.pop({
      animated: false
    });
    return true;
  }
  handleOke = () => {
    this.props.navigator.dismissLightBox()
  }
  handleWasSubmit = async () => {  
    let data = null;
    await this.props.dispatch(HandleForgetPassword(this.props.form.values));
    
    if(this.props.auth.error){
      if(this.props.auth.results.success){
        await this.handleChangePage("VerifyCode" , {
          username : this.props.form.values.username,
          type : "forgot"
        },
        "push"
      )
      }else{
        
      }
    }else{
      data = {
            title : "Koneksi tidak stabil",
            body : "Silakan cek kembali jaringan anda",
            button : [
                {
                  name : "Oke",
                  style : {
                    color:COLOR_PRIMARY
                  },
                  onPress : this.handleOke
                }
              ]
          }
          this.openMessage(data)
        }

    };
        
  openMessage(data) {
    this.props.navigator.showLightBox({
      screen :"bagidata.Auth.Alert",
      passProps : {
        data : ()=> (data)
      },
      style: {
        backgroundColor:"#00000080"
      }
    })
  }
  handleChangePage = (page , passProps, method)=>{
    this.props.navigator[method](
      { 
        screen : `bagidata.${page}` ,
        animationType: 'none',
        passProps 
      }
    )
  }
  render(){
    let error = false;
    if(this.props.form){
      if(this.props.form.syncErrors){
        error = true;
      }else{
        error = false;
      }   
    }else{
      error = false;
    }
      return(      
        <Container style={{backgroundColor:"white"}}>
        
          <View style={Styles.wrapperBackground}>
            <Image source={require(`${TAKE_AWAY}img/background/auth.png`)} style={Styles.background}/>
          </View>
          {this.props.auth.isLoading ? 
            <Loading  />
          :
            null
          }
          <Content padder  > 
            <Grid >            
              <Row  style={{marginVertical:50}}>
                <Grid>
                  <Row justifyContent="center" alignItems="center">    
                    <View style={{width:40}}>
                      <Image source={require(`${TAKE_AWAY}img/icon/lock.png`)} style={[Styles.background , { height:55}]} resizeMode="stretch"/>
                    </View>
                  </Row>
                  <Row justifyContent="center" alignItems="center">
                    <H3 style={{marginTop:20 , marginBottom:10 , fontFamily:"Barlow-SemiBold"}}>
                      Lupa Password
                    </H3>
                  </Row>
                  <Row justifyContent="center" alignItems="center">
                    <View style={{width:250}}>
                      <Text style={{fontWeight:"normal" , textAlign:"center" ,  fontFamily:"Barlow-Light"}}>
                        Kami butuh e-mail/no telepone kamu yang  terdaftar untuk  mengirim pengaturan ulang kata sandi
                      </Text>
                    </View>

                  </Row>
                </Grid>
              </Row>
              
              <Form onSubmit={this.handleWasSubmit.bind(this)}/>

              <Grid style={{marginTop:77.5}}>
                <Row alignItems="center" justifyContent="center">
                    <View style={{width:"70%"}}>
                      <Button full  rounded disabled={
                          this.props.form ? 
                          Boolean(this.props.form.syncErrors) 
                          : 
                          true 
                          } style={[{backgroundColor: error ? "#aaa"  : COLOR_PRIMARY} , Styles.flat  ]} onPress={()=>{this.handleWasSubmit()}}>
                          <Text style={Styles.fontButton}>Atur Ulang Sekarang</Text>
                      </Button>
                    </View>
                </Row>
                <Row alignItems="center" justifyContent="center" style={{ marginTop:89.5}}>
                  <Text style={{fontSize:13  , fontWeight: "normal" , color:"#999"}}>
                    Belum punya akun?    
                  </Text>
                  <TouchableOpacity onPress={()=>{this.handleChangePage("Register", {}, "resetTo")}} >
                    <Text style={{fontSize:13  , fontFamily: "Barlow-SemiBold" , color:COLOR_PRIMARY , paddingLeft:5}}>
                      Daftar
                    </Text>
                  </TouchableOpacity>
                </Row>
              </Grid>
            </Grid> 

          </Content>
        </Container>
    )
  }
}



const mapsStateToProps = (state) => ({
  auth : state.ForgetPasswordAuthReducer , 
  form : state.form.forget_password
})
export default connect(mapsStateToProps)(ForgetPassword);
