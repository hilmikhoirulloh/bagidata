import axios from "axios";
import * as Config from "../../../../../config"

export  function AuthForgetPassword(username){
	return {
		type : "FORGET_PASSWORD_REDUCER",
		payload : axios.post(`${Config.URL_API}/api/v1/forgot`, {
			email : username
		},
		{
			timeout: 1000
		})
	}
	
}



export function HandleForgetPassword(data){	
	return async (dispatch) => {
		await dispatch(AuthForgetPassword(data.username))
	}
}