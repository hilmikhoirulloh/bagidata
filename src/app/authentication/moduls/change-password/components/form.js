import React , {Component} from "react";

import { InputText } from "./input"; 
import validate from "../validate";

import {Field , reduxForm } from "redux-form";
import { Form as NForm} from "native-base" ; 

class Form extends Component {
    icon = [ "ios-eye-outline" , "ios-eye-off-outline" ]
    state = {
        numIcon : 0,
        icon : "ios-eye-outline",
        numIconSecond : 0,
        iconSecond : "ios-eye-outline",
        
    }
    changeIcon(){
        const numIcon = (this.state.numIcon+1)%2;
        
        this.setState({
            numIcon ,
            icon : this.icon[numIcon]
        })
    }
    changeIconSecond(){
        const numIconSecond = (this.state.numIcon+1)%2;    
        this.setState({
            numIconSecond ,
            iconSecond : this.icon[numIconSecond]
        })
    }
    render(){
        const { handleSubmit, pristine, reset, submitting ,valid , anyTouched , onSubmit} = this.props ;    
        const {icon , numIcon , iconSecond , numIconSecond} = this.state;
        return(
            <NForm onSubmit={handleSubmit} style={{marginTop:30 , paddingHorizontal:10}}>
                
                <Field
                    name ="password"
                    placeholder = "Masukan kata sandi"
                    hash= {!Boolean(numIcon)}
                    width = {23/1.5}
                    height = {30/1.5}
                    iconHash = {icon}
                    clickIcon = {this.changeIcon.bind(this)}
                    source = {require("../../../../../../img/icon/lock.png")}
                    component={InputText}
                />

                <Field
                    name ="verify_password"
                    placeholder = "Ulang kata sandi"
                    hash= {!Boolean(numIconSecond)}
                    width = {23/1.5}
                    height = {30/1.5}
                    iconHash = {iconSecond}
                    clickIcon = {this.changeIconSecond.bind(this)}
                    source = {require("../../../../../../img/icon/lock.png")}
                    component={InputText}
                />
                

            </NForm>
        )
}
  }
  
  
  export default reduxForm({
    form: 'setPassword',
    validate,
  })(Form);