import React  , {Component} from "react";
import {
    ActivityIndicator , 
    Image , 
    View,
} from "react-native";

import {Styles , COLOR_SECONDARY} from "../../../styles";

export default class Loading extends Component {
    constructor(props){
        super(props);
    }
    render(){
        return(
            <View style={[Styles.coverAndroid , {zIndex: 1000  }]}>
                <View style={[Styles.coverAndroid , {zIndex:1000  , backgroundColor: "#222e61" , opacity:0.95}]}>
                </View>
                <View style={[Styles.coverAndroid , {zIndex:1000}]}>
                    <ActivityIndicator size={80} color="#ffffff"  />
                </View>
                <View style={[Styles.coverAndroid , {zIndex:1000 }]}>
                    <Image source={require("../../../../../../img/icon_logo.png")} style={{width:282/8 , height:245/8}}/>
                </View>
            </View>

        )
    }
}
