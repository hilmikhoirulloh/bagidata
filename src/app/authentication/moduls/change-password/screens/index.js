import React , {Component} from "react";

import  {Image  , TouchableOpacity , View  , Text} from "react-native" ;
import { Row , Container , Content , Grid  , Button } from "native-base"
import {Styles , COLOR_PRIMARY } from "../../../styles.js";

import { connect } from "react-redux";
import {handleSetPassword} from "../actions";
import Form from "../components/form";
import Loading from "../components/loading";

const TAKE_AWAY = "../../../../../../";

class SetPassword extends Component {
  constructor(props){
    super(props); 

  }
  
  handleWasSubmit = async () => {
    let data= null;
    await this.props.dispatch(handleSetPassword(this.props.form , this.props.username , this.props.type , this.props.otp));
      if(!this.props.auth.error){
        if(this.props.auth.results.success){
          this.props.navigator.resetTo({
            screen : "bagidata.Login"
          });
        }
      }else{
        data = {
          title : "Koneksi tidak stabil",
          body : "Silakan cek kembali jaringan anda",
          button : [
              {
                name : "Oke",
                style : {
                  color:COLOR_PRIMARY
                },
                onPress : this.handleOke
              }
            ]
        }
        this.openMessage(data)
      }

  };
  openMessage(data) {
    this.props.navigator.showLightBox({
      screen :"bagidata.Auth.Alert",
      passProps : {
        data : ()=> (data)
      },
      style: {
        backgroundColor:"#00000080"
      }
    })
  }
  handleBack = () =>{
    this.props.navigator.resetTo({
      screen : "bagidata.Login"
    });
  }
  handleChangePage = (page)=>{
    this.props.navigator.push(
      { 
        screen : `bagidata.${page}` ,
        animationType: 'none'
      }
    )
  }
  render(){    
    let error = false;
    
    if(this.props.form){
      if(this.props.form.syncErrors){
        error = true;
      }else{
        error = false;
      }   
    }
    return(
        <Container  >
          <View style={Styles.wrapperBackground}>
            <Image source={require(`${TAKE_AWAY}img/background/auth.png`)} style={Styles.background}/>
          </View>
          {this.props.auth.isLoading ? 
            <Loading  />
          :
            null
          }
          <Content padder > 
            <Grid >            
              <Row justifyContent="center" alignItems="center" >
                <View style={{width:182, marginVertical:105}}>
                  <Image source={require(`${TAKE_AWAY}img/brand.png`)} style={[Styles.background , { height:52}]} resizeMode="stretch"/>
                </View>
              </Row>
              <Form onSubmit={this.handleWasSubmit.bind(this)}/>
            </Grid>    
            <Grid style={{marginTop:10}}>
                <Row alignItems="center" justifyContent="center">
                    <View style={{width:"70%"}}>
                        <Button full  rounded disabled={
                            this.props.form ? 
                            Boolean(this.props.form.syncErrors) 
                            : 
                            true 
                            } style={[{backgroundColor: error ? "#aaa"  : COLOR_PRIMARY} , Styles.flat  ]} onPress={()=>{this.handleWasSubmit()}}>
                            <Text style={Styles.fontButton}>Masuk Invest!</Text>
                        </Button>
                    </View>
                </Row>
                <Row alignItems="center" justifyContent="center" style={{ marginTop:90}}>
                  <Text style={{fontSize:13  , fontWeight: "normal" , color:"#999"}}>
                    Sudah punya akun data?    
                  </Text>
                  <TouchableOpacity onPress={()=>{this.handleBack()}}>
                    <Text style={{fontSize:13  , fontFamily: "Barlow-SemiBold" , color:COLOR_PRIMARY , paddingLeft:5}}>
                      Masuk
                    </Text>
                  </TouchableOpacity>
                </Row>
            </Grid>
          </Content>
        </Container>
    )
  }
}


const mapsStateToProps = (state) => ({
  form : state.form.setPassword,
  auth : state.SetPasswordReducer
})
export default connect(mapsStateToProps)(SetPassword);