const validate = values => {
    const errors = {};
    if (values.verify_password != values.password){
        errors.verify_password = "Kata sandi tidak cocok"
    }
    return errors;
}

export default validate;