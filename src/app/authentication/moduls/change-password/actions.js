import axios from "axios";
import * as Config from "../../../../../config";

export function handleSetPassword(data , username , type , otp =false){
    const password = data.values.password;
    let variable = null;
    if (type=="forgot"){
        variable = {
            password : password ,
            email  : username,
            otp
        }
    }else{
        variable = {
            password : password ,
            email  : username
        }
    }
    return {
        type : "SET_PASSWORD_REDUCER" , 
        payload : axios.post(`${Config.URL_API}/api/v1/${type == "register"? "" : "forgot/"}complation` , 
        variable,
        {
            timeout: 1000
        })
    }
} 

