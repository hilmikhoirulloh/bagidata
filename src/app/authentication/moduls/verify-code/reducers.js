const VerifyCodeState = {
    results : [],
    isLoading : false,
    error : false
}
const ResendOtpState = {
    results : [],
    isLoading : false ,
    error : false
}

export function VerifyCodeReducer(state = VerifyCodeState , action){
    switch(action.type){
        case "VERIFY_CODE_REDUCER_PENDING":
            return {...state, isLoading : true, error:false }

        case "VERIFY_CODE_REDUCER_FULFILLED":
            return {...state , isLoading :false , results :action.payload.data , error: false}
        
        case "VERIFY_CODE_REDUCER_REJECTED":
            return {...state , isLoading :false , results :[] , error: true}
        
        default:
            return state
    }
}


export function ResendOtpReducer(state = ResendOtpState, action){
    switch(action.type){
        case "RESEND_OTP_REDUCER_PENDING":
            return {...state, isLoading:true, error:false}
        case "RESEND_OTP_REDUCER_FULFILLED":
            return {...state, isLoading:false, error:false, results:action.payload.data}
        case "RESEND_OTP_REDUCER_REJECTED":
            return {...state, isLoading:false, error:true, results:[]}
        default:
            return state
    }
}