import React from "react";

import { InputText } from "./input"; 
import validate from "../validate";

import {Field , reduxForm } from "redux-form";
import {Form as NForm} from "native-base";


const Form = props => {
    const { handleSubmit, pristine, reset, submitting ,valid , anyTouched , onSubmit} = props ;    
    
    
    const handleChangePage = (page)=>{
      page = !page ? "" : page; 
      props.navigator.push(
        { 
          screen : `bagidata.${page}` 
        }
      )
    }
  
    return(
        <NForm onSubmit={handleSubmit}>
            <Field
                label="Input Code"
                name ="otp"
                hash = {false}
                placeholder = "Kode"
                component={InputText}
            /> 
        </NForm>
    )
  }
  
  
  export default reduxForm({
    form: 'verify_code',
    validate,
    // asyncBlurFields: ['otp'],
    // destroyOnUnmount: false
  })(Form);