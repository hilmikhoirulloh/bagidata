import React from 'react';
import {Item , Label , Input  , Text} from "native-base"
import {Styles} from "../../../styles"
export function InputText({ 
    input, 
    label, 
    type, 
    placeholder ,
    hash,
    meta: {  touched, error } 
  }){
    let hasError= false;
    if(error !== undefined && touched){
      hasError= true;
    }
    return( 
      <Input {...input}  placeholder={placeholder} secureTextEntry={hash} style={{ fontFamily:"Barlow-Regular" , textAlign:"center"}} />
      
    )
  }