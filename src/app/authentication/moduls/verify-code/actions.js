import axios from "axios";
import * as Config from "../../../../../config";

export function GoneVerifyCode(username , otp){
   
    return {
        type : "VERIFY_CODE_REDUCER" , 
        payload : axios.post(`${Config.URL_API}/api/v1/confirmation` , {
            email : username , 
            otp 
        })
    }
} 
export function ResendOtp(username){
    console.log("hilmi");
    return{
        type : "RESEND_OTP_REDUCER" , 
        payload : axios.post(`${Config.URL_API}/api/v1/confirmation/resend` , {
            email : username
        },
        {
            timeout: 1000
        })
    }
}