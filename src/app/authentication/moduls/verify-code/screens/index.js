import React , {Component} from "react";
import  {Text , Image  , View , BackHandler } from "react-native" ;
import { Row , Container , Content , Grid , Header , Left , Body , Button , Title , Icon , Right ,Item} from "native-base"
import {Styles , COLOR_PRIMARY } from "../../../styles.js";
import { connect } from "react-redux";
import {GoneVerifyCode ,ResendOtp} from "../actions";
import Form from "../components/form";

import Loading from "../components/loading";
const TAKE_AWAY = "../../../../../../";
class VerifyCode extends Component {
  constructor(props){
    super(props);     
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    this.state = {
      timeSend : false,
      time : 60
    }
  }

  onNavigatorEvent(event) { 
    switch (event.id) {
      case 'willAppear':
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        break;
      case 'willDisappear':
        this.backPressed = 0;
        this.backHandler.remove();
        break;
      default:
        break;
    }
  }
  handleBackPress = () => {
    this.props.navigator.pop({
      animated: false
    });
    return true;
  }

  
  
  handleOke = () => {
    this.props.navigator.dismissLightBox();
    this.timeOut();
  }
  handleCancle = () => {
    this.handleOke();
    this.props.navigator.pop({
      animationType:"none"
    });
    
  }
  
  componentDidMount(){
    if(this.props.type == "register"){
      let data = null;
      if(this.props.register.results.msg == "GTF"){
        data = {
          title : "Ingin menajutkan pendaftaran",
          body : "Isi kode yang sudah terkirim dan isikan",
          onClose : this.handleCancle,
          button : [
            
            {
              name : "Cancle",
              onPress : this.handleCancle
            },
            {
              name : "Oke",
              style : {
                color:COLOR_PRIMARY
              },
              onPress : this.handleOke
            }
          ]
        }
        this.openMessage(data);
        return;
      }
    }

    this.timeOut() 

  }
  
  

  openMessage(data) {
    this.props.navigator.showLightBox({
      screen :"bagidata.Auth.Alert",
      passProps : {
        data : ()=> (data)
      },
      style: {
        backgroundColor:"#00000080"
      }
    })
  }
  componentWillUnmount(){
    clearInterval(this.time);
  }

  handleWasSubmit = async (values) => {
    await this.props.dispatch(GoneVerifyCode(this.props.username , this.props.form.values.otp));
     if(this.props.auth.results.success){
      this.props.navigator.resetTo({
        animationType: "none",
        screen : "bagidata.SetPassword",
        passProps : {
          username : this.props.username,
          type : this.props.type,
          otp: this.props.form.values.otp
        }
      })
    }else{
      if(this.props.auth.results.status == 400){
        data = {
          title : "Maaf anda gagal melanjutkan",
          body : "Silakan masukan kode otp dengan benar ",
          button : [
            {
              name : "Oke",
              style : {
                color:COLOR_PRIMARY
              }
            }
          ]
        }
      }

      this.props.navigator.showLightBox({
        screen :"bagidata.Auth.Alert",
        passProps : {
          data : ()=> (data)
        },
        style: {
          backgroundColor:"#00000080"
        }
      })
    }
  };
  handleBack = () => {
    this.props.navigator.pop();
    clearInterval(this.time);
  }
  
  handleChangeResend(timeSend){
    this.setState({
      timeSend 
    })
  }
  
  time = null;
  
  timeOut = () => {
    let int = 60;
    $this = this;
    this.time = setInterval(function(){
      int--;
      if(int < 0){
          clearInterval($this.time);
          $this.handleChangeResend(true)
          $this.setState({
            time : 60
          })
        }else{
          $this.setState({
            time : int
          })
        }
      } , 1000);  
  }
  
  handleCallCode = async () => {  
    if (this.state.timeSend){
      await this.props.dispatch(ResendOtp(this.props.username))
      await this.handleChangeResend(false)
      await this.timeOut();
    }
  }
  
  
  render(){
    let error = false;
    if(this.props.form){
      if(this.props.form.syncErrors){
        error = true;
      }else{
        error = false;
      }   
    }  
    return(
      <Container style={Styles.bgWhite}>
        {this.props.resendOtp.isLoading  ? 
            <Loading  />
          :
            null
        }
        <Header style={[Styles.bgWhite,{marginBottom:3 }]} androidStatusBarColor="#444">
            <Left>
                <Button transparent onPress={()=>{this.handleBack()}}>
                    <Icon name='arrow-back' style={Styles.fontBlack} />
                </Button>
            </Left>
            <Body>
                <Title style={Styles.fontBlack}>
                    Daftar
                </Title>
            </Body>
            <Right />
        </Header>
        <Content padder  > 
          <Grid >

            <Row justifyContent="center" alignItems="center" >
              <View style={{width:182/1.1, marginTop:10, marginBottom:15}}>
                <Image source={require(`${TAKE_AWAY}img/brand.png`)} style={[Styles.background , { height:52/1.1}]} resizeMode="stretch"/>
              </View>
            </Row>     
            
            <Row justifyContent="center" alignItems="center">
              <View style={{width:"90%"}}>
                <Text style={{textAlign:"center" ,  color:"#aaa" , fontWeight: "100"}}>
                  Masukan Kode Verifikasi yang telah kami kirimkan melalui SMS atau Email anda yang didaftarkan. JANGAN  berikan kode  kepada SIAPAPUN
                </Text>
              </View>
            </Row>
            
            <Row alignItems="center" justifyContent="center" style={{marginBottom: 10 , marginTop:20}}>   
              <Text style={[Styles.fontBlack , Styles.fontBold]}>   
                {this.props.username}
              </Text>   
            </Row>
            
            <Form onSubmit={this.handleWasSubmit.bind(this)}/>
            
            <Item style={{borderBottomWidth:0 , justifyContent:"center"}} onPress={ this.handleCallCode.bind(this) } >
                <Icon name="ios-mail-outline" style={{color:this.state.timeSend ? "#42a5f5" : "#aaa"}}/>
                <Text style={{color:this.state.timeSend ? "#42a5f5" : "#aaa"}}>Kirim ulang kode </Text>
                <Text style={{color:"#42a5f5"}}>0{this.state.time == 60 ? 1 : 0}:{this.state.time== 60 ? "00" : this.state.time }</Text>
            </Item>

            <Row alignItems="center" justifyContent="center" style={{marginBottom: 10 , marginTop:20}}>   
              {this.props.register.results.data == "GTF" ?
                <Text>
                  Silakan isi Ulang Kode
                </Text>
              :
                null
              } 
            </Row>
          </Grid>

          <Grid style={{marginTop:170}}>
            <Row alignItems="center" justifyContent="center">
                <View style={{width:"95%"}}>
                  <Button full  rounded disabled={
                    this.props.form ? 
                    Boolean(this.props.form.syncErrors) 
                    : 
                    true 
                    } style={[{backgroundColor: error ? "#aaa"  : COLOR_PRIMARY} , Styles.flat  ]} onPress={()=>{this.handleWasSubmit()}}>
                    <Text style={Styles.fontButton}>Verifikasi</Text>
                  </Button>
                </View>
            </Row>
          </Grid>
        </Content>
      </Container>
    )
  }
}



const mapsStateToProps = (state) => ({
  auth : state.VerifyCodeReducer , 
  form : state.form.verify_code , 
  register : state.RegisterAuthReducer,
  resendOtp : state.ResendOtpReducer  
})
export default connect(mapsStateToProps)(VerifyCode);
