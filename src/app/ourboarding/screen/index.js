import React, { Component  } from 'react';
import { Button  } from "native-base";

import {
  TouchableOpacity,
  View , 
  Image,
  Text
} from 'react-native';

import {changeAppInitialized} from "../../../actionApp";

import {Styles , HEIGHT_WINDOW } from "../styles";
import Swiper from 'react-native-swiper';

import {connect} from "react-redux";

class OurBoarding extends Component {
    i =  0;
    state = {
        name : "Selanjutnya"
    }
    changeName(){
        console.log(this.refs.swiper.state.index);
        if(this.refs.swiper.state.index > 1){
            this.setState({
                name : "Mulai Invest"
            })
        }
    }
    pageLogin(){
        this.props.dispatch(changeAppInitialized("register"))
    }
    nextSwipe(){
        if(this.refs.swiper.state.index < 3 && this.state.name !="Mulai Invest"){
            this.refs.swiper.scrollBy(this.i + 1)
            this.changeName()   
        }else{
            this.pageLogin()
        }
    }
    async handleSkip(){
        await this.refs.swiper.scrollBy(3-this.refs.swiper.state.index);     
        await this.changeName();
    }
    Swipe = ()=>{
        if(this.refs.swiper.state.index < 3){
            this.changeName()               
        }
    }
    render(){

    return (
    <View style={{flex:1 ,}}>
        
        <View style={{flex:10}}>
            <Swiper showsButtons={true } onIndexChanged={this.Swipe} ref="swiper" activeDotStyle={{marginHorizontal:10}} dotStyle={{marginHorizontal:10}} showsButtons={false} loop={false} dotColor="#bbb" activeDotColor="#444">
                <View style={{flex:1 ,}}>
                    <TouchableOpacity onPress={()=>{this.handleSkip()}} style={Styles.skipButton}>
                        <Text style={Styles.textSkip}>
                            Skip
                        </Text>
                    </TouchableOpacity>
                
                    <Image source={require("../../../../img/onboarding/(1).png")}  style={{position:"absolute" , top: 0 , bottom:0 , left:0 , right:0 , width:"100%", height:"100%"}}/>
                    <View style={[{marginTop:HEIGHT_WINDOW-(HEIGHT_WINDOW/2.1) } ,Styles.center  ]}>
                        <View style={[Styles.center ,{width:"80%"}]}>
                            <View  style={Styles.padder}>
                                <Text style={Styles.text} >
                                    Selama ini data kamu telah disalah gunakan banyak pihak tanpa kamu ketahui
                                </Text>
                            </View>
                            <View  style={Styles.padder}>
                                <Text style={Styles.text} >
                                    Dan kami percaya bahawa setiap orang berhak mendapat penghasilan dari datanya 
                                </Text>
                            </View>
                        </View>            
                    </View>
                    
                </View>
                <View style={{flex:1}}>
                    <TouchableOpacity onPress={()=>{this.handleSkip()}} style={Styles.skipButton}>
                        <Text style={Styles.textSkip}>
                            Skip
                        </Text>
                    </TouchableOpacity>
            
                    <Image source={require("../../../../img/onboarding/(2).png")}  style={{position:"absolute" , top: 0 , bottom:0 , left:0 , right:0 , width:"100%", height:"100%"}}/>
                    <View style={[{marginTop:HEIGHT_WINDOW-(HEIGHT_WINDOW/2.2) } ,Styles.center  ]}>
                        <View style={[Styles.center ,{width:"80%"}]}>
                            <View  style={Styles.padder}>
                                <Text style={Styles.text} >
                                    Penghasilan pasif bisa kamu dapat dari aktivitas media sosial, struk belanja, bahkan data pribadi kamu sangat berharga
                                </Text>
                            </View>
                            
                        </View>            
                    </View>
                </View>
                <View style={{flex:1}}>
                    <TouchableOpacity onPress={()=>{this.handleSkip()}} style={Styles.skipButton}>
                        <Text style={Styles.textSkip}>
                            Skip
                        </Text>
                    </TouchableOpacity>
                
                    <Image source={require("../../../../img/onboarding/(3).png")}  style={{position:"absolute" , top: 0 , bottom:0 , left:0 , right:0 , width:"100%", height:"100%"}}/>
                    <View style={[{marginTop:HEIGHT_WINDOW-(HEIGHT_WINDOW/2.2) } ,Styles.center  ]}>
                        <View style={[Styles.center ,{width:"80%"}]}>
                            <View  style={Styles.padder}>
                                <Text style={Styles.text} >
                                    Lalu bagaiamana  kamu dapat penghasilan pasif?
                                </Text>
                            </View>
                            <View  style={Styles.padder}>
                                <Text style={Styles.text} >
                                    Bagidata dapat memberikan penghasilan  data kamu 
                                </Text>
                            </View>
                            
                        </View>            
                    </View>
                </View>
                <View style={{flex:1}}>
                    <Image source={require("../../../../img/onboarding/(4).png")}  style={{position:"absolute" , top: 0 , bottom:0 , left:0 , right:0 , width:"100%", height:"100%"}}/>
                    <View style={[{marginTop:HEIGHT_WINDOW-(HEIGHT_WINDOW/2.2) } ,Styles.center  ]}>
                        <View style={[Styles.center ,{width:"80%"}]}>
                            <View  style={Styles.padder}>
                                <Text style={Styles.text} >
                                    Data yang kamu berikan akan kami  analsisa  dan kamu jadikan anatomi
                                </Text>
                            </View>
                            <View  style={Styles.padder}>
                                <Text style={Styles.text} >
                                    Dan kami  memastikan data  kamu akan  aman sesuai  peraturan GDPR(General Data Protection Regulation)
                                </Text>
                            </View>
                        </View>            
                    </View>
                </View>
            </Swiper>
        </View>
        <View style={{flex:1}}>
            <Button full rounded style={[Styles.flat , Styles.bgPrimary  , {marginHorizontal:10}]} onPress={()=>{this.nextSwipe()}}>
                <Text style={Styles.textButton}>
                    {this.state.name}
                </Text>
            </Button>
        </View >
    </View>
    );
  }
}

export default connect()(OurBoarding)