import {
    StyleSheet,
    Dimensions
} from 'react-native';

export  const HEIGHT_WINDOW  = Dimensions.get("screen").height;
export const Styles = StyleSheet.create({
    center: {
        
        alignItems: 'center',
        flexDirection: "column"
    },
    text: {
        fontFamily : "Barlow-Thin",
        fontSize : 16,
        textAlign:"center"
    },
    textButton : {
        color: "white",
        fontFamily : "Barlow-SemiBold",
        fontSize : 17
    },
    textSkip : {
        fontSize:17 , 
        color:"#aaa" ,
        fontFamily : "Barlow-SemiBold"
    },
    padder : {
        marginVertical :5 
    },
    flat :{
        elevation:0
    },
    bgPrimary : {
        backgroundColor:"#ec008c"
    } , 
    skipButton : {
        position:"absolute" , 
        zIndex:1000 , 
        top:10 , 
        right:10
    }
})