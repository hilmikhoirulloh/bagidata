import { StyleSheet , Dimensions } from "react-native"
export const COLOR_PRIMARY = "#ec008c";
export const COLOR_SECONDARY = "#222e61";
export const WINDOW_WIDTH = Dimensions.get("window").width;
export const WINDOW_HEIGHT = Dimensions.get("window").height;
export const Styles = StyleSheet.create({
    bgWhite : {
        backgroundColor: "white"
    },
    center : {
        alignContent: 'center',
        justifyContent: 'center',
    },
    pointSize: {
        width:80/2.5 , 
        height:46/2.5,
        marginLeft:5,
        marginTop:3
    },
    flat: {
        elevation : 0
    },
    logoSize : {
        width:110 , 
        height:30
    },
    point : {
        backgroundColor:"#efc75e" , 
        width:"100%" , 
        height:"60%" , 
        borderTopLeftRadius:20, 
        borderBottomLeftRadius:20 
    },
    centerVertical :{
        justifyContent: 'center',
    },
    wrapperPoint : {
        position:"absolute" ,
        top:0 , 
        bottom:0  , 
        left:40 
    },
    hr  : {
        backgroundColor:"#ddd" , 
        height:1 , width:"106%" , 
        marginRight:-10 , 
        marginLeft:-10
    },
    bgGrey : {
        backgroundColor: "#f8f8f8"
    },
    itemTransfer:{ 
        paddingLeft:15, 
        marginTop:10,  
        height:50
    },
    wrapperTextBottom : {
        paddingTop:35, 
        paddingBottom : 20
    },
    fontBold : {
        fontFamily:"Barlow-SemiBold" , 
        fontSize:16 , 
        color:"black"
    },
    wrapperPopup : {
        width:320   , 
        borderRadius:20, 
        backgroundColor:"white" , 
        paddingHorizontal:10 
    },
    fontBlack : {
        color :"#444",
    },
    fontThin : {
        fontSize:11 , 
        fontWeight : "100",
        fontFamily : "Barlow-Light"
    },
    bottomBorder: {
        backgroundColor:"#e83aaa",
        position:"absolute",
        bottom:0,
        left:10, 
        right:10
    },
    smallBold : {
        textAlign:"center" , 
        fontSize:11 , 
        fontFamily:"Barlow-SemiBold" 
    },
    wrapperPanelBottom : {
        flexDirection: 'row', 
        justifyContent:'space-between' , 
        alignItems:"flex-end"
    },
    buttonBuy : {
        height:30 , 
        paddingHorizontal:15 , 
        borderRadius:5,
        backgroundColor : COLOR_PRIMARY
    },
    bgPrimary : {
        backgroundColor : COLOR_PRIMARY
    },
    textColorPrimary : {
        color : COLOR_PRIMARY
    },
    textColorSecondary : {
        color : "#222e61"
    },
    wrapperFlexWrap : {
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    sizeIconCaseCategory:{
        width:30,
        height: 30
    },
    bottomButton : {
        position:"absolute" , 
        bottom:10,
        left:10,
        right :10 ,
        elevation: 0
    },
    borderButton : {
        borderColor: COLOR_PRIMARY,
        borderWidth: 2,
    },
    itemGopay : {
        borderTopWidth:0 , 
        borderLeftWidth:0 ,
        borderRightWidth:0 ,  
        borderBottomWidth:0 , 
        marginTop:0,
        paddingLeft: 10,
        height:40
    },
    bottomAction: {
        borderWidth:1 ,
        borderColor : "#ddd",
        borderRadius : 10,
        padding:10,
        bottom: 5
    },
    coverAndroid:{
        position :"absolute", 
        bottom:0 , 
        right:0 , 
        left:0,
        top : 0,
        alignItems:"center",
        justifyContent:"center"
    }
}) 