import React , {Component} from "react";
import {Header, Left, Icon, Body, Title, Right} from "native-base";
import {Styles} from "../../../styles";
// import {}
export default class HeaderComponent extends Component{
    render(){
        return(
            <Header style={[Styles.bgWhite,{marginBottom:3 }]}>
                <Left>
                    <Button transparent onPress={()=>{onBack()}}>
                        <Icon name='arrow-back' style={Styles.fontBlack}/>
                    </Button>
                </Left>
                <Body>
                    <Title style={Styles.fontBlack}>
                        Reward
                    </Title>
                </Body>
                <Right>
                    <Icon name='arrow-back' style={Styles.fontBlack}/>                
                </Right>
            </Header>
        )
    }
} 