import React, {Component} from "react";
import {View, Text, Image, Stylesheet } from "react-native";
import {Grid, Col, Row } from "native-base";
import {Styles, COLOR_PRIMARY, COLOR_SECONDARY} from "../../../styles";

export default class Segmentation extends Component {
    render(){
        const {data} = this.props;
        return (
            <View>
                <View style={StylesComponent.segmentation}>
                    <Text style={[Styles.fontThin, {fontSize : 10, color:"white"}]}>
                        + 10 Poin
                    </Text>
                    {/* <Image source={} style={{width:10 , height:10}}/> */}
                    <Text style={[Styles.fontBold , {backgroundColor:COLOR_PRIMARY, color:"white"}]}>
                        =====
                    </Text>
                </View>
            </View>
        )
    }
}

const StylesComponent = Stylesheet.create({
    segmentation : {
        flexDirection : "row" , 
        alignItems:"center", 
        justifyContent:"center", 
        backgroundColor:COLOR_SECONDARY, 
        borderRadius:10
    }
})