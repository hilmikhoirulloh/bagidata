import  React, {Component} from "react";
import Header from "../components/header";
import {Image, Text, ActivityIndicator, ProgressBarAndroid } from "react-native";
import {Container, Content} from "native-base";
import Collapse from "../components/collapse";
import Segmentation from "../components/segmentation";

import {Styles} from "../../../styles";
import {connect} from "react-redux";


class GamificationReward extends Component {
    render(){
        return(
            <Container>
                <Header/>
                <Content >
                    <View >
                        {/* <Image source={} style={{flex:1 ,  width:null, height:null}}/> */}
                    </View>
                    <Grid style={{borderBottomWidth:5, borderBottomColor:"#f8f8f8"}}>
                        <Row>
                            <Col style={{flex:1}}>
                                <Text style={[Styles.fontBold]}>
                                    
                                    XP 1500
                                    <Text style={[Styles.fontThin]}>
                                        / 3000
                                    </Text>
                                </Text>
                            </Col>
                            <Col style={{flex:2}}>
                                <View style={{ borderRadius:10 , overflow:"hidden" , flex:1 }}>
                                    <ProgressBarAndroid
                                        styleAttr="Horizontal"
                                        indeterminate={false}
                                        progress={0.5}
                                        style={{transform: [{ scaleX: 1.0 }, { scaleY: 2.5 }]  }}    
                                    />
                                </View>
                            </Col>
                        </Row>
                    </Grid>
                    
                    <View style={{borderBottomWidth:1, borderBottomColor:"#f8f8f8"}}>
                        <Text style={[Styles.fontBold]}>
                            Misi Harian
                        </Text>
                    </View>
                    
                    <View>
                        <Collapse/>
                    </View>

                    <View style={{borderBottomWidth:5, borderBottomColor:"#f8f8f8"}}>
                        <Collapse data={null}/>
                    </View>

                    <View >
                        <Text style={[Styles.fontBold]}>
                            Tambah Keuntungan
                        </Text>
                    </View>

                    <View>
                        <Segmentation dataArray={[]}/>
                    </View>
                </Content>
            </Container>
        )
    }
}

const mapStateToProps = (state)=>({

})
export default connect()(GamificationReward)