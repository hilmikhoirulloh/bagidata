import React , { Component } from "react" ;

import {Text  ,Image  } from "react-native" ;
import { Styles } from "../../../styles";

import FormEachPaymentPage from "../components/form";
import {Container , Content , Input   , Button , Item , Grid , Col , Row}  from "native-base";
import Header from "../components/header";

import { connect } from "react-redux";

const TAKE_AWAY = "../../../../../../";

class EachPaymentPage extends Component {
    constructor(props){
        super(props);
        this.props.navigator.setStyle({
            navBarHidden : true
        })
    }
    handleBack = () => {
        this.props.navigator.pop();
    }
    render(){
        const {wallet} = this.props;
        return(
            <Container>
                <Header onBack={this.handleBack}/>
                <Content style={{backgroundColor:"white" }}>
                    <Grid style={[Styles.bgGrey , {paddingVertical:10 , paddingHorizontal:30 }]}>
                        <Row>
                            <Col justifyContent="center">
                                <Image square source={require(`${TAKE_AWAY}img/icon/payment.png`)} resizeMode="stretch" style={{width:70/2.1 , height:40/2.1}} />                      
                            </Col>
                            <Col style={Styles.center} alignItems="flex-end">
                                <Text style={[Styles.fontBold ,Styles.textColorSecondary, {fontSize:18}]}>
                                    Rp. {wallet.result ? wallet.result.cash : "-"}
                                </Text >
                            </Col>
                        </Row>
                    </Grid>
                    
                    <Grid style={{padding:10 }}>
                        <Row alignItems="center" >
                            <Col  alignItems="center" justifyContent="center">
                                <Item style={[Styles.bgGrey , Styles.itemGopay , {borderRadius:5}  ]}>
                                    <Input placeholder="Masukan Nominal" style={{fontSize:12}}/>
                                </Item>
                            </Col>
                        </Row>
                    </Grid >

                    <FormEachPaymentPage {...this.props}/>
                    
                    <Grid>
                        <Row style={{paddingHorizontal:20 , paddingVertical:10 }}>
                            <Col>
                                <Text style={[Styles.fontBold]}>
                                    Detail Pembayaran              
                                </Text> 
                            </Col>
                        </Row>
                        <Row style={{paddingVertical:10 , paddingHorizontal:20 }}>
                            <Col>
                                <Text style={[Styles.fontThin]}>
                                    BPJS -              
                                </Text> 
                            </Col>
                            <Col>
                                <Text style={[Styles.fontThin , {textAlign:"right"}]}>
                                    Rp. -
                                </Text> 
                            </Col>
                        </Row>
                        <Row style={{paddingVertical:10 , paddingHorizontal:20 }}>
                            <Col>
                                <Text style={[Styles.fontThin]}>
                                    Denda 1 Hari -              
                                </Text> 
                            </Col>
                            <Col>
                                <Text style={[Styles.fontThin , {textAlign:"right"}]}>
                                    Rp. -
                                </Text> 
                            </Col>
                        </Row>
                        <Row style={{padding:20 , backgroundColor : "#222e61" }}>
                            <Col>
                                <Item style={{borderBottomWidth:0}}>
                                    <Text style={[Styles.smallBold  ,{color:"white" ,  marginRight:3}]}>
                                        Uangku               
                                    </Text>
                                    <Image source={require(`${TAKE_AWAY}img/icon/white_money.png`)} style={{width:10 , height:10}} />                                    
                                </Item>
                            </Col>
                            <Col>
                                <Text style={[Styles.smallBold , {textAlign:"right" , color:"white"}]}>
                                    Rp. -
                                </Text> 
                            </Col>
                        </Row>
                    </Grid>
                </Content>
                <Button style={[Styles.bottomButton , Styles.bgPrimary]} full rounded>
                    <Text style={{color:"white"}}>
                        Beli
                    </Text>
                </Button>
            </Container>
        )
    }
} 
const setStateToProps = (state) =>({
    wallet : state.WalletReducer
});
export default connect(setStateToProps)(EachPaymentPage);