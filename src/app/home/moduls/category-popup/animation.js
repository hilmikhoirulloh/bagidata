export const  CustomLayoutSpring = {
    duration: 400,
    create: {
      type: "spring",
      property:"scaleXY",
      springDamping: 0.7,
    },
    update: {
      type:"spring",
      springDamping: 0.7,
    },
};