import React , { Component } from "react" ;

import {View , Text  ,Image  , TouchableOpacity} from "react-native" ;
import { Styles , WINDOW_WIDTH } from "../../../styles";
import {Container , Content }  from "native-base";


import Header from "../components/header";

import { connect } from "react-redux";

const TAKE_AWAY = "../../../../../../";
const widthWrapperCategories = (WINDOW_WIDTH-20)/4;

class Buy extends Component {
    constructor(props){
        super(props);
        this.props.navigator.setStyle({
            navBarHidden : true
        })
    }
    handleBack = () =>{
        this.props.navigator.pop({
            animationType: 'none'              
        });
    }
    handleChangePage = (page)=>{
        this.props.navigator.push({
            screen : `bagidata.Layout.Home.${page}`,
            animationType: 'none' 
        })
    }
    render(){
        const {wallet} = this.props;

        return(
            <Container>
                <Header onBack={this.handleBack} cash={wallet.result ? wallet.result.cash : "-"}/>
                <Content padder style={{backgroundColor:"white"}}>
                    <View style={[Styles.wrapperFlexWrap , {paddingTop:20}]}>
                        <View style={{width:widthWrapperCategories}}>
                            <TouchableOpacity onPress={()=>{this.handleChangePage("ChargePulsa")}} style={{flex:1 , alignItems :"center" , justifyContent : "center"}}>
                                <Image source={require(`${TAKE_AWAY}img/icon/value_pulsa.png`)} style={[Styles.sizeIconCaseCategory]}/>
                                <Text style={[Styles.smallBold , {textAlign:"center"}]}>
                                    Isi Pulsa
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{width:widthWrapperCategories}} >
                            <TouchableOpacity onPress={()=>{this.handleChangePage("PackageData")}} style={{flex:1 , alignItems :"center" , justifyContent : "center"}}>                        
                                <Image source={require(`${TAKE_AWAY}img/icon/pakcage_data.png`)} style={[Styles.sizeIconCaseCategory]}/>
                                <Text style={[Styles.smallBold , {textAlign:"center"}]}>
                                    Paket Data
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{width:widthWrapperCategories}}>
                            <TouchableOpacity  onPress={()=>{this.handleChangePage("Gopay")}} style={{flex:1  , alignItems :"center" , justifyContent : "center"}}>                        
                                <Image source={require(`${TAKE_AWAY}img/icon/go_pay.png`)} style={[Styles.sizeIconCaseCategory]}/>
                                <Text style={[Styles.smallBold , {textAlign:"center"}]}>
                                    Go Pay
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Content>
            </Container>
        )
    }
} 

const mapStateToProps= (state)=>({
    wallet : state.WalletReducer
})
export default connect(mapStateToProps)(Buy)