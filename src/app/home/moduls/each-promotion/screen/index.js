import React , { Component } from "react" ;

import {View , Text  ,Image  , TouchableOpacity} from "react-native" ;
import { Styles , WINDOW_WIDTH } from "../../../styles";
import {Container , Content , Header , Tab , Tabs , TabHeading ,H3 }  from "native-base";

import PROMOTION from "../../../variables/PROMOTION.json";

import {connect} from "react-redux";

class EachPromotion extends Component {
        
    
    constructor(props){
        super(props);
        this.state = {
            item : PROMOTION[this.props.index]
        }
    }
    render(){
        return(
            <Container>
                <Content style={{backgroundColor:"white" }}>
                    <Image source={{"uri" : this.state.item.url }} style={{flex:1, height:150}} />
                
                    <View style={{paddingHorizontal:10 , paddingTop:10}}>
                        <Text style={{paddingBottom:5}}>
                            {this.state.item.provider}
                        </Text>
                        <H3 style={Styles.fontBold}>
                            {this.state.item.title}                        
                        </H3>
                        <Text style={[Styles.fontThin , {paddingTop:5}]}>
                            {this.state.item.available}
                        </Text>
                    </View>
                    
                    <Tabs 
                        tabBarUnderlineStyle={{backgroundColor:"#ec008c"}}
                        tabContainerStyle={{elevation:0}}
                        >
                        <Tab 
                            heading="Ringkasan" 
                            tabStyle={{backgroundColor: 'white'}} 
                            activeTabStyle={{backgroundColor: 'white'}} 
                            textStyle={{color: '#aaa' , fontFamily:"Barlow-SemiBold"}}
                            activeTextStyle={{color: '#444', fontFamily:"Barlow-SemiBold"}}
                        >
                            <Text>
                                {this.state.item.summary}
                            </Text>
                        </Tab>
                        <Tab 
                            heading="Penggunaan" 
                            tabStyle={{backgroundColor: 'white'}} 
                            activeTabStyle={{backgroundColor: 'white'}} 
                            textStyle={{color: '#aaa' , fontFamily:"Barlow-SemiBold"}}
                            activeTextStyle={{color: '#444', fontFamily:"Barlow-SemiBold"}}
                        >
                            
                            <Text>
                                {this.state.item.use}
                            </Text>
                        </Tab>
                        <Tab 
                            heading="S&K" 
                            tabStyle={{backgroundColor: 'white'}} 
                            textStyle={{color: '#aaa' , fontWeight:"bold"}}
                            activeTabStyle={{backgroundColor: 'white'}} 
                            activeTextStyle={{color: '#444', fontWeight: 'bold'}}
                        >
                            <Text>
                                {this.state.item.summary}
                            </Text>
                        </Tab>
                    </Tabs>
                </Content>
            </Container>
        )
    }
} 

const mapStateToProps = (state) => ({

})
export default connect(mapStateToProps)(EachPromotion);