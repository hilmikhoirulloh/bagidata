import React , { Component } from "react" ;

import {View , Text  ,Image  , TouchableOpacity , FlatList , TouchableHighlight} from "react-native" ;
import { Styles , WINDOW_WIDTH , COLOR_PRIMARY} from "../../../styles";

import {Container , Content , Button , Item , Grid , Col , Row}  from "native-base";
import PAKAGE_DATA from "../../../variables/PACKAGEDATA.json";

import Header from "../components/header";

import {connect} from "react-redux";

const TAKE_AWAY  = "../../../../../../";

class BuyPackageData extends Component {
    state = {
        pakageData : PAKAGE_DATA,
        number : "-"
    }
    constructor(props){
        super(props);
        this.props.navigator.setStyle({
            navBarHidden : true
        })
    }
    handleBack = () =>{
        this.props.navigator.resetTo({
            screen : "bagidata.Layout.Home.Buy"
        })
    }
    handlePress(i){
        
        const alldata = PAKAGE_DATA;
        for( data in alldata)
            alldata[data].status = false
        
        alldata[i].status = true;
        this.setState({
            epulsa : alldata,
            number: alldata[i].number
        })
    }
    render(){
        const {wallet} = this.props;
        return(
            <Container>
                <Header onBack={this.handleBack}/>
                <Content style={{backgroundColor:"white" }}>
                    <Grid style={[Styles.bgGrey , {paddingVertical:10 , paddingHorizontal:30 }]}>
                        <Row>
                            <Col justifyContent="center">
                                <Image square source={require(`${TAKE_AWAY}img/icon/payment.png`)} resizeMode="stretch" style={{width:70/2.1 , height:40/2.1}} />                      
                            </Col>
                            <Col style={Styles.center} alignItems="flex-end">
                                <Text style={[Styles.fontBold , Styles.textColorSecondary ,{fontSize:18}]}>
                                    Rp. {wallet.result ? wallet.result.cash : "-"}
                                </Text >
                            </Col>
                        </Row>
                    </Grid>
                    
                    <Grid style={{paddingTop:10}}>
                        <Row style={{paddingBottom:10 , paddingHorizontal:20}}>
                            <Col justifyContent="center">
                                <Item  style={{borderBottomWidth:0}}>   
                                    <Image source={require(`${TAKE_AWAY}img/icon/phone.png`)} style={{width:18*0.8, height:30*0.8 , marginRight:10}} resizeMode="stretch"/>
                                    <View>
                                        <Text style={[Styles.smallBold ,{textAlign : "left"}]}>
                                            Hilmi
                                        </Text>
                                        <Text style={[{fontFamily:"Barlow-SemiBold" , marginTop:-5}, Styles.fontBlack]}>
                                            08987797989
                                        </Text>
                                    </View>              
                                </Item>                     
                            </Col>
                            <Col style={Styles.center} alignItems="flex-end">
                                <Image source={require(`${TAKE_AWAY}img/icon/contact.png`)} style={{width:30*0.8, height:33*0.8 }} resizeMode="stretch"/>                            
                            </Col>
                        </Row>
                        
                        <View style={[Styles.hr ,{marginLeft:-20 , width:"120%"} ]}/>
                        <Row>
                            <Col>
                                <Text style={[Styles.fontBold , {paddingVertical:20, textAlign:"center"}]}>
                                    Paket Data              
                                </Text> 
                            </Col>
                        </Row>
                    </Grid>
                    
                    <View style={Styles.hr  }/>
                    <View style={{paddingBottom:10}}>
                    {this.state.pakageData.map((data , index)=>(
                        <TouchableOpacity key={index} onPress={()=>{this.handlePress(index)}}>
                            <Grid  >
                                <Row style={{paddingVertical:15 , paddingHorizontal:20 , backgroundColor:data.status ? "#ffcaf5":"white"}} alignItems="center" justifyContent="center">
                                    <Col style={{flex:0.5}}>
                                        <Text>
                                            {data.number}
                                        </Text>
                                    </Col>
                                    <Col style={{flex:0.4}}>
                                        <Item style={{borderBottomWidth:0}}>
                                            <Image source={require(`${TAKE_AWAY}img/icon/package_data.png`)} style={{width:10 , height:10}} resizeMode="stretch"/>                                
                                            <Text style={[{marginLeft:3},  Styles.fontThin]}>
                                                {data.packageData}                                            
                                            </Text>
                                        </Item>
                                    </Col>
                                    <Col style={{flex:0.4}}>
                                        <Item style={{borderBottomWidth:0}}>
                                            <Image source={require(`${TAKE_AWAY}img/icon/telephone.png`)} style={{width:10 , height:10}} resizeMode="stretch"/>                                
                                            <Text style={[{marginLeft:3},  Styles.fontThin]}>
                                                {data.telephone}
                                            </Text>
                                        </Item>
                                    </Col>
                                    <Col style={{flex:0.4}}>
                                        <Item style={{borderBottomWidth:0}}>
                                            <Image source={require(`${TAKE_AWAY}img/icon/sms.png`)} style={{width:12 , height:10}} resizeMode="stretch"/>                                
                                            <Text style={[{marginLeft:3},  Styles.fontThin]}>
                                                {data.sms}
                                            </Text>
                                        </Item>
                                    </Col>
                                </Row>
                            </Grid>                    
                        </TouchableOpacity>
                    ))}
                    </View>
                    
                    <Grid >
                        <Row style={{paddingHorizontal:20}}>
                            <Col>
                                <Text style={[Styles.fontBold]}>
                                    Detail Pembayaran              
                                </Text> 
                            </Col>
                        </Row>
                        <Row style={{paddingHorizontal:20 , paddingVertical:10}}>
                            <Col>
                                <Text style={[Styles.fontThin]}>
                                    Paket {this.state.number}              
                                </Text> 
                            </Col>
                            <Col>
                                <Text style={[Styles.fontThin , {textAlign:"right"}]}>
                                    Rp. {this.state.number}
                                </Text> 
                            </Col>
                        </Row>
                        <Row style={{paddingVertical:20 , paddingHorizontal:20 , backgroundColor : "#222e61" }}>
                            <Col>
                                <Item style={{borderBottomWidth:0}}>
                                    <Text style={[Styles.smallBold  ,{color:"white" ,  marginRight:3}]}>
                                        Uangku               
                                    </Text>
                                    <Image source={require(`${TAKE_AWAY}img/icon/white_money.png`)} style={{width:10 , height:10}} />                                    
                                </Item>
                            </Col>
                            <Col>
                                <Text style={[Styles.smallBold , {textAlign:"right" , color:"white"}]}>
                                    Rp. {this.state.number}
                                </Text> 
                            </Col>
                        </Row>
                    </Grid>
                </Content>
                <Button style={[Styles.bottomButton , Styles.bgPrimary]} full rounded>
                    <Text style={{color:"white" , fontFamily:"Barlow-Bold"}}>
                        Beli
                    </Text>
                </Button>
            </Container>
        )
    }
} 

const mapStateToProps = (state) => ({
    wallet : state.WalletReducer
})

export default connect(mapStateToProps)(BuyPackageData);