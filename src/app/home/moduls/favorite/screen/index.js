import React , { Component } from "react" 
import {View , Text , TouchableOpacity} from "react-native" 
import { Container , Content , Grid , Col , Row } from "native-base";
import {Styles} from "../../../styles";

import Header from "../components/header";

import {connect} from "react-redux";

class Favorite extends Component {
    constructor(props){
        super(props);
        this.props.navigator.setStyle({
            navBarHidden : true
        })
    }
    handleBack = () => {
        this.props.navigator.pop({
            animationType: 'none' 
        });
    }
    render(){
        return(
            <Container>
                <Header goBack={this.handleBack}/>
                <Content style={[Styles.bgWhite , {paddingTop:10 }]}>
                    <TouchableOpacity>
                        <Grid>
                            <Row style={[Styles.bgGrey , { padding:10}]}>
                                <Col ><Text style={{fontSize:16}}>Transfer</Text></Col>
                                <Col  alignItems="flex-end" ><Text style={Styles.smallBold}>Mandiri Annaz - Bagas Rivaldi</Text></Col>
                            </Row>
                        </Grid>
                    </TouchableOpacity>
                    <TouchableOpacity>                    
                        <Grid>
                            <Row style={[Styles.bgGrey , { padding:10}]}>
                                <Col ><Text style={{fontSize:16}}>Beli</Text></Col>
                                <Col  alignItems="flex-end"><Text style={Styles.smallBold}>Pulsa - 092932190321</Text></Col>
                            </Row>
                        </Grid>
                    </TouchableOpacity>
                </Content>
            </Container>
        )
    }
} 

const mapStateToProps = () => ({

})

export default connect(mapStateToProps)(Favorite);