import React from 'react';

import {Item  , Input} from "native-base";
import {Styles} from "../../../styles";

export function InputText({
    input ,
    placeholder,
    keyboardType,
    meta : {  touched, error } 
})
{
    return(
        <Item rounded style={[Styles.bgGrey , Styles.itemTransfer , {borderColor:"transparent"}]}>
            <Input placeholder={placeholder} {...input} style={{fontFamily : "Barlow-Regular"}} keyboardType={keyboardType} />
        </Item>
    )  
}
