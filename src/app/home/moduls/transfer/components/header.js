import React, {Component} from "react";
import {Header, Left, Button, Icon, Body, Title, Right} from "native-base";
import {Styles} from "../../../styles" ;

export default class HeaderComponent extends Component {
    render(){
        const {onBack} = this.props; 
        return(
            <Header style={[Styles.bgWhite,{marginBottom:3 }]}>
                <Left>
                    <Button transparent onPress={()=>{onBack()}}>
                        <Icon name='arrow-back' style={Styles.fontBlack}/>
                    </Button>
                </Left>
                <Body>
                    <Title style={Styles.fontBlack}>
                        Transaksi
                    </Title>
                </Body>
                <Right/>
            </Header>
        )
    }
}