import React , {Component} from  "react";

import {Styles} from "../../../styles";
import {Item , Picker , Text , Icon ,Button , Form ,Input} from "native-base";

import validate from "../validate";

import {Field , reduxForm} from "redux-form";
import { InputText } from "./input";

class FormTransfer extends Component{
    state={
        selected2 : undefined,
        hash : true,
        passwordIcon : "ios-eye-outline"
    }
    onValueChange2(value) {
        this.setState({
            selected2: value
        });
    }
    render(){
        return(
            <Form style={{paddingHorizontal:10}}>         
                <Field
                    name= "code"
                    placeholder="Masukan Kode"
                    component={InputText}
                />
                
                <Item picker rounded style={[Styles.bgGrey , Styles.itemTransfer , {borderColor:"transparent"}]}>
                    <Picker
                        mode="dropdown"
                        iosIcon={<Icon name="ios-arrow-down-outline" />}
                        style={{ color : "#999" }}
                        placeholder="Select your SIM"
                        
                        selectedValue={this.state.selected2}
                        onValueChange={this.onValueChange2.bind(this)}
                    >
                        <Picker.Item label="Wallet" value="key0" />
                        <Picker.Item label="ATM Card" value="key1" />
                        <Picker.Item label="Debit Card" value="key2" />
                        <Picker.Item label="Credit Card" value="key3" />
                        <Picker.Item label="Net Banking" value="key4" />
                    </Picker>
                </Item>

                <Button transparent light style={{paddingRight:10 , marginTop:10}}>
                    <Icon name="ios-add-circle-outline" style={{color:"#444"}}/>
                    <Text style={[Styles.smallBold , {color:"#444"}]}>
                        Tambah Akun
                    </Text>
                </Button>
                
                <Field
                    name= "number"
                    placeholder="Masukan Nominal"
                    keyboardType="numeric"
                    component={InputText}
                />
                {/* <Item rounded style={[Styles.bgGrey , Styles.itemTransfer , {borderColor:"transparent" }]}>
                    <Input placeholder="Masukan Nominal" secureTextEntry={this.state.hash}/>
                    <Icon name={this.state.passwordIcon} onPress={this.togglePassword.bind(this)} />
                </Item> */}
            </Form>
        )
    }
}

export default reduxForm({
    form : 'transfer',
})(FormTransfer);