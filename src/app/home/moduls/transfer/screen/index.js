import React , { Component } from "react" ;
import {View , Text  , Image } from "react-native" ;
import { Styles , WINDOW_WIDTH } from "../../../styles";
import {Container , Content , Button , Grid , Col , Row}  from "native-base";


import FormTransfer from "../components/form"
import {connect} from "react-redux";

import Header from "../components/header";

const TAKE_AWAY = "../../../../../../";

class Transfer extends Component {
    
    state={
        selected2 : undefined,
        hash : true,
        passwordIcon : "ios-eye-outline"
    }
    
    constructor(props){
        super(props);
        this.props.navigator.setStyle({
            navBarHidden : true
        })
    }
    
    togglePassword(){
        this.setState({
            hash : !this.state.hash,
            passwordIcon : this.state.hash ? "ios-eye-off-outline" : "ios-eye-outline"
        })
    }
    onValueChange2(value) {
        this.setState({
            selected2: value
        });
    }
    handleBack= ()=>{
        this.props.navigator.pop({
            animationType: 'none' 
        });
    }
    render(){
        const {wallet} = this.props;
        return(
            <Container>
                <Header onBack={this.handleBack}/>
                <Content style={{backgroundColor:"white" }}>
                    <Grid style={[Styles.bgGrey , {paddingVertical:10 , paddingHorizontal:30 }]}>
                        <Row>
                            <Col justifyContent="center">
                                <Image square source={require(`${TAKE_AWAY}img/icon/payment.png`)} resizeMode="stretch" style={{width:70/2.1 , height:40/2.1}} />                      
                            </Col>
                            <Col style={Styles.center} alignItems="flex-end">
                                <Text style={[Styles.fontBold , Styles.textColorSecondary , {fontSize:18}]}>
                                    Rp. {wallet.result ? wallet.result.cash : "-" }
                                </Text >
                            </Col>
                        </Row>
                    </Grid>

                    <FormTransfer {...this.props} />    
                    
                </Content>
                <Button style={[Styles.bottomButton ,  Styles.bgPrimary]} full rounded>
                    <Text style={{color:"white" , fontFamily : "Barlow-SemiBold"}}>
                        Submit
                    </Text>
                </Button>
            </Container>
        )
    }
} 

const mapStateToProps = (state) => ({
 wallet : state.WalletReducer
})

export default connect(mapStateToProps)(Transfer);