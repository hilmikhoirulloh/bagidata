import React, {Component} from "react";
import {ProgressBarAndroid, Image, Text, StyleSheet, View} from "react-native";
import {Styles, WINDOW_WIDTH} from "../../../styles";

import  {Circle} from "react-native-progress"; 
// import CircularProgressbar from "react-circular-progressbar";
const MARGIN_HORIZONTAL =  (WINDOW_WIDTH - 200)  /2;
export default class StatusExperience extends Component{
    render(){
        // console.log(Progress);
        return(
            <View>
                <View style={{marginHorizontal:MARGIN_HORIZONTAL}}>
                <Circle 
                    size={200} 
                    thickness={10}
                    indeterminate={false}  
                    borderWidth={0}
                    unfilledColor="#eee" 
                    progress={0.5}
                    color="teal"
                />
                </View>

                <View style={[StylesComponent.wrapperImage, StylesComponent.centerVertical]}>
                    <Image/>
                    <Text style={[{textAlign:"center" , fontSize:19} , Styles.fontBlack]}>
                        XP 1500
                        <Text style={{fontSize:14}}>
                        /5000
                        </Text>
                    </Text>
                </View>
            </View>
        )
    }
}

const StylesComponent = StyleSheet.create({
    wrapperImage : {
        position : "absolute",
        top:0,
        bottom:0,
        right:0,
        left:0
    },
    centerVertical : {
        flexDirection : "column",
        alignItems:"center",
        justifyContent:"center"
    }
})