import React, {Component} from "react";
import {connect} from "react-redux";
import Header from "../components/header";
import {Container, Content, Button} from "native-base";
import {Text} from "react-native";
import {Styles, COLOR_PRIMARY} from "../../../styles";
import StatusLv from "../components/statusLevel";
import StatusExp from "../components/statusExperience";

class DetailStatus extends Component{
    handleBack = () => {
        this.props.navigator.dismissModal({
            animationType: "none"
        })
    }
    render(){
        return(
            <Container>
                <Header onBack={this.handleBack}/>
                <Content padder>
                    <StatusExp/>
                    <StatusLv/>
                    <Button full rounded style={[{backgroundColor:COLOR_PRIMARY}, Styles.flat]}>
                        <Text style={{color:"white"}}>
                            Tambah Experience
                        </Text>
                    </Button>
                </Content>
            </Container>
        )
    }
}

const mapStateToProps = (state) =>({

})
export default connect(mapStateToProps)(DetailStatus);