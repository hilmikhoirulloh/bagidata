import React , { Component } from "react" ;
import {View , Text  ,Image  , TouchableOpacity} from "react-native" ;
import { Styles , WINDOW_WIDTH } from "../../../styles";
import {Container , Content}  from "native-base";

import Header from "../components/header";

import { connect } from "react-redux";


const TAKE_AWAY = "../../../../../../";
const widthWrapperCategories = (WINDOW_WIDTH-20)/4;


class Payment extends Component {
    constructor(props){
        super(props);
        this.props.navigator.setStyle({
            navBarHidden : true
        })
    }
    handleBack = ()=>{
        this.props.navigator.pop({
            animationType: 'none' 
        });
    }
    handleEachSinglePage(type){
        this.props.navigator.push({
            screen  : "bagidata.Layout.Home.EachPagePayment",
            passProps : {
                type 
            },
            animationType: 'none' 
        })
    }
    render(){
        const {wallet} = this.props;
        return(
            <Container>
                <Header onBack={this.handleBack} cash={wallet.result ? wallet.result.cash: "-" }/>
                <Content padder style={{backgroundColor:"white" ,height:"100%"}}>
                    <View style={[Styles.wrapperFlexWrap , {paddingTop:20}]}>
                        <View style={{width:widthWrapperCategories}}>
                            <TouchableOpacity onPress={()=>{this.handleEachSinglePage("")}} style={{flex:1 , alignItems :"center" , justifyContent : "center"}}>
                                <Image source={require(`${TAKE_AWAY}img/icon/token_electricty.png`)} style={[Styles.sizeIconCaseCategory,{marginHorizontal:(widthWrapperCategories-30)/2}]}/>
                                <Text style={[Styles.smallBold , {textAlign:"center"}]}>
                                    Token Listrik
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{width:widthWrapperCategories}}>
                            <TouchableOpacity onPress={()=>{this.handleEachSinglePage("")}} style={{flex:1 , alignItems :"center" , justifyContent : "center"}}>                        
                                <Image source={require(`${TAKE_AWAY}img/icon/pln_bill.png`)} style={[Styles.sizeIconCaseCategory,{marginHorizontal:(widthWrapperCategories-30)/2}]}/>
                                <Text style={[Styles.smallBold , {textAlign:"center"}]}>
                                    Tagihan PLN
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{width:widthWrapperCategories}}>
                            <TouchableOpacity onPress={()=>{this.handleEachSinglePage("")}} style={{flex:1 , alignItems :"center" , justifyContent : "center"}}>                        
                                <Image source={require(`${TAKE_AWAY}img/icon/bpjs.png`)} style={[Styles.sizeIconCaseCategory,{marginHorizontal:(widthWrapperCategories-30)/2}]}/>
                                <Text style={[Styles.smallBold , {textAlign:"center"}]}>
                                    BPJS
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Content>
            </Container>
        )
    }
} 

const mapStateToProps = (state) => ({
    wallet : state.WalletReducer
})
export default connect(mapStateToProps)(Payment)