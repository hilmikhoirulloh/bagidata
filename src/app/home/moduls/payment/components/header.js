import React , {Component} from "react";
import {Header, Left, Button, Right, Grid, Row, Body, Title, Icon} from "native-base";
import {Text , Image} from "react-native";
import {Styles} from "../../../styles";

const TAKE_AWAY = "../../../../../../";
export default class HeaderComponent extends Component{
    render(){
        const {cash , onBack} = this.props
        return(
            <Header style={[Styles.bgWhite,{marginBottom:3 }]}>
                <Left>
                    <Button transparent onPress={()=>{onBack()}}>
                        <Icon name='arrow-back' style={Styles.fontBlack}/>
                    </Button>
                </Left>
                <Body>
                    <Title style={Styles.fontBlack}>
                        Bayar
                    </Title>
                </Body>
                <Right style={{marginTop:-10}}>
                    <Grid>
                        <Row justifyContent="flex-end" alignItems="flex-end">
                            <Image source={require(`${TAKE_AWAY}img/icon/tbuy-money.png`)} style={{width:30*1.5 ,height:10*1.5 }}  resizeMode="stretch"/>
                        </Row>
                        <Row justifyContent="flex-end" alignItems="flex-start">
                            <Text style={[Styles.fontBold ]} >
                                
                                Rp. {cash}
                            </Text>
                        </Row>
                    </Grid>
                </Right >
            </Header>
        )
    }
}