export const CustomLayoutLinear = {
    duration: 100,
    create: {
        type: "easeInEaseOut",property: "opacity"
    },
    delete: {
        type: "easeInEaseOut",property: "opacity"
    },
    update: {
        type: "easeInEaseOut"
    }
};
