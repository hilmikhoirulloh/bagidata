export const FULL_DATA = [
    {   
        id  : 1,
        resource : require("../../../../../img/icon/travel_active.png"),
        disableResource : require("../../../../../img/icon/travel.png"),
        name : "Perjalanan",
        status : true
    },
    {
        id  : 2,        
        resource : require("../../../../../img/icon/food_active.png"),
        disableResource : require("../../../../../img/icon/food.png"),
        name : "Makanan" , 
        status : true
    },
    {
        id  : 3,
        resource : require("../../../../../img/icon/electronic_active.png"),
        disableResource : require("../../../../../img/icon/electronic.png"),
        name : "Elektronink",
        status : true
    },
    {
        id  : 4,        
        resource : require("../../../../../img/icon/hobby_active.png"),
        disableResource : require("../../../../../img/icon/hobby.png"),
        name : "Hobi",
        status : true
    },
    {
        id  : 5,
        resource : require("../../../../../img/icon/new_product_active.png"),
        disableResource : require("../../../../../img/icon/new_product.png"),
        name : "Penawaran Baru",
        status : false
    },
    {
        id  : 6,        
        resource : require("../../../../../img/icon/document_active.png"),
        disableResource : require("../../../../../img/icon/document.png"),
        name : "E-Commerce",
        status : false
    },
    {
        id  : 7,        
        resource : require("../../../../../img/icon/clothes_active.png"),
        disableResource : require("../../../../../img/icon/clothes.png"),
        name : "Gaya Hidup",
        status :false
    },
    {
        id  : 8,        
        resource : require("../../../../../img/icon/game_active.png"),
        disableResource : require("../../../../../img/icon/game.png"),
        name : "Permainan",
        status : false
    },
    {
        id  : 9,        
        resource : require("../../../../../img/icon/global_active.png"),
        disableResource : require("../../../../../img/icon/global.png"),
        name : "Wilayah",
        status : false
    },
    {
        id  : 10,        
        resource :require("../../../../../img/icon/liked_active.png"),
        disableResource : require("../../../../../img/icon/liked.png"),
        name : "Kesehatan",
        status : false
    },
    {
        id  : 11,        
        resource :require("../../../../../img/icon/makeup_active.png"),
        disableResource : require("../../../../../img/icon/makeup.png"),
        name : "Kecantikan",
        status : false
    },
    {
        id  : 12,        
        resource : require("../../../../../img/icon/party_active.png"),
        disableResource : require("../../../../../img/icon/party.png"),
        name : "Hiburan",
        status : false
    },
    {
        id  : 13,        
        resource : require("../../../../../img/icon/tools_active.png"),
        disableResource : require("../../../../../img/icon/tools.png"),
        name : "Layanan",
        status : false
    },
    {
        id  : 14,        
        resource : require("../../../../../img/icon/reference_active.png"),
        disableResource : require("../../../../../img/icon/reference.png"),
        name : "Pendidikan",
        status : false
    }
];
