import React , {Component} from "react";
import {View , Text , TouchableOpacity ,Image , LayoutAnimation, ScrollView} from "react-native";
import {Button , Icon} from "native-base";
import { Styles, WINDOW_HEIGHT, WINDOW_WIDTH} from "../../../styles";

import {SetChangeIcon , SetDefaultIcon} from "../actions"
import {CustomLayoutSpring} from "../animation";

const MARGIN_HORIZONTAL = WINDOW_WIDTH * 5 / 100 / 2 ;
const MARGIN_VERTICAL = WINDOW_HEIGHT * 10 / 100 / 2 ;

import {connect} from "react-redux";

class Popup extends Component {
    
    FieldIconActive = [0,1,2,3];
    
    constructor(props){
        super(props)
        let dataArr = [];
        
        
        let arrayTrue = this.props.icon.results.filter((data)=> (data.status == true));
        for( int in arrayTrue  )
            dataArr.push(arrayTrue[int].id_kategori_barang)

        this.state = {
            condition : 0,
            dataArr,
            allIcon : this.props.icon.results,
            hasSave : false
        }
    }
    componentWillUnmount(){
        const {allIcon, dataArr, condition, hasSave} = this.state;
        
        if(!hasSave){
            allIcon.map((data)=>{
                if(!Boolean(dataArr.find((dataId)=>(dataId == data.id_kategori_barang)))){
                    data.status = false
                }else{
                    data.status = true
                }
            });       
        }
    }

    handlePullIcon(id){
        LayoutAnimation.configureNext(CustomLayoutSpring)
        if(this.state.condition){            
            const {allIcon} = this.state;
                
            const arrayOne = allIcon.filter((data,index)=>(data.id_kategori_barang==id));
            const index = allIcon.findIndex((data)=>(data.id_kategori_barang == arrayOne[0].id_kategori_barang));
            
            allIcon[index].status = false
            
            this.setState({allIcon})
        }        
    }
    handlePushIcon(id){
        LayoutAnimation.configureNext(CustomLayoutSpring)
        const length = this.props.icon.results.filter((data)=>(data.status==true)).length;
        if(this.state.condition && length < 4){        
            const {allIcon} = this.state;            
            
            const arrayOne = allIcon.filter((data,index)=>(data.id_kategori_barang==id));
            const index = allIcon.findIndex((data)=>(data.id_kategori_barang == arrayOne[0].id_kategori_barang));
            allIcon[index].status = true
            
            this.setState({allIcon});
        }
    }

    handleExitPopup(){
        this.props.navigator.dismissLightBox();
    }

    async handleChangeIcon(){
        await this.props.dispatch(SetChangeIcon(this.state.allIcon));
        await this.setState({
            hasSave :true
        })
        await this.handleExitPopup();
    }
    handlePressEdit(){
        this.setState({
            condition : !this.state.condition
        })
    }
    render(){
        const activeIcon  = this.state.allIcon.filter((data)=>(data.status == true))
        const disableIcon  = this.state.allIcon.filter((data)=>(data.status == false))
       
        return(
                <View style={{backgroundColor:"white",  height:"90%" , borderRadius:10,  marginVertical : MARGIN_VERTICAL, marginHorizontal: MARGIN_HORIZONTAL , padding:10}}>
                        
                        <TouchableOpacity onPress={this.handleExitPopup.bind(this)}  >
                            <Icon style={[Styles.fontBold , {textAlign : "right"}]} name="close"/>   
                        </TouchableOpacity>
                        
                        <Text style={[Styles.fontBold , {paddingLeft:15 , paddingBottom:10}]}>
                            Atur Rekomendasimu
                        </Text>
                        
                        <View style={{flexWrap:"wrap" , alignItems:"stretch"  , flexDirection:"row", borderBottomColor:"#ddd", borderBottomWidth:1 }}>
                            {   
                                this.FieldIconActive.map((i)=>{
                                if(activeIcon[i]){
                                    return(
                                    <TouchableOpacity key={i}  onPress={()=>{this.handlePullIcon(activeIcon[i].id_kategori_barang)}} style={{width:75 , alignItems:"center" , justifyContent:"center" , paddingVertical:10}} key={i}>                                    
                                        <Image source={{uri  : activeIcon[i].icon_active}} style={{width:30 , height:30}}/>
                                        <Text style={[Styles.smallBold , {fontSize:10 , textAlign:"center" , color:"#888"}]}>
                                            {activeIcon[i].kategori_barang}
                                        </Text>
                                    </TouchableOpacity>                 
                                    )
                                }else{                 
                                    return(
                                        <View key={i} style={{width:75, height:50, alignItems:"center" , justifyContent:"center" }}>
                                            <Icon name="md-add-circle" style={{fontSize:30 , color:"#ccc"}}/>
                                        </View>
                                    )
                                }
                         
                            })} 
                        </View>
                            {/* <View style={Styles.hr}/> */}
                        <ScrollView >
                            <View style={{flexWrap:"wrap" , alignItems:"stretch"  , flexDirection:"row" , paddingBottom: !this.state.condition ? 50 : 100  }}>                        
                                {disableIcon.map((data, i )=>{
                                    return(
                                        <TouchableOpacity onPress={()=>{this.handlePushIcon(data.id_kategori_barang)}} style={{width:75 , alignItems:"center" , justifyContent:"center" , paddingVertical:10}} key={i}>
                                            <Image source={{uri  :data.icon_nonactive}} style={{width:30 , height:30}}/>
                                            <Text style={[Styles.smallBold , {fontSize:10 , textAlign:"center" , color:"#888"}]}>
                                                {data.kategori_barang}
                                            </Text>
                                        </TouchableOpacity>
                                    )
                                })} 
                            </View>
                        </ScrollView>        
                            
                        {!this.state.condition? 
                        <View style={[Styles.bgWhite,{position:"absolute" , bottom:5 , left:10 , right:10 , paddingTop:5}]}>                        
                            <Button full rounded transparent  style={[ Styles.borderButton , { marginBottom:5}]} onPress={()=>{this.handlePressEdit()}}>
                                <Text style={[Styles.fontBold , Styles.textColorPrimary]}>
                                    Edit
                                </Text>
                            </Button>
                        </View>

                        :
                        <View style={[Styles.bgWhite,{position:"absolute" , bottom:5 , left:10 , right:10 ,  paddingTop:5}]}>
                            <Button full rounded transparent  style={[ Styles.borderButton , { marginBottom:5}]} >
                                <Text style={[Styles.fontBold , Styles.textColorPrimary]}>
                                    Atur Otomatis
                                </Text>
                            </Button>
                            <Button full rounded transparent onPress={()=>{this.handleChangeIcon()}} style={[Styles.borderButton , {backgroundColor:"#ec008c" }]} >
                                <Text style={[Styles.fontBold , {color : "white"}]}>
                                    Save
                                </Text>
                            </Button>
                        </View>
                        
                        }
                    </View>
        )
    }
} 

const setStateToProps = (state)=>({
    icon : state.HandleIconHomeReducer
})
export default connect(setStateToProps)(Popup);