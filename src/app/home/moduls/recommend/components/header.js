import React , {Component} from "react";
import {Header , Left , Button , Body , Title, Right , Text , Icon } from "native-base" ;
import {Image} from "react-native"

import {Styles} from "../../../styles";

export default class HeaderComponent extends Component {
    render(){
        const {point , onBack} = this.props;
        return(
            <Header style={[Styles.bgWhite,{marginBottom:3 }]}>
                <Left>
                    <Button transparent onPress={()=>{onBack()}}>
                        <Icon name='arrow-back' style={Styles.fontBlack}/>
                    </Button>
                </Left>
                <Body>
                    <Title style={Styles.fontBlack}>
                        Rekomendasi
                    </Title>
                </Body>

                <Right style={{alignItems:"center"}}>
                    <Text style={[Styles.fontBold , ]}>
                        {point}
                    </Text>
                    <Image source={require(`../../../../../../img/icon/point.png`)} resizeMode="stretch" style={Styles.pointSize} />                    
                </Right >
            </Header>
        )
    }
} 