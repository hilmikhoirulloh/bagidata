import React , {Component} from "react";
import {View , Text , TouchableOpacity , Image , TouchableWithoutFeedback} from "react-native";
import {Grid, Col, Row , Button , Title , H3} from "native-base";
import { Styles } from "../../../styles";
import message from "../../../variables/MESSAGE.json";
import Validator from "validator";
export default class Alert extends Component {
    constructor(props){
        super(props);
        this.state = {
            message : message[1] 
        }
    }

    handleButtonAlert(action){
        if(!action){
            this.props.navigator.dismissLightBox();
        }else if(!Validator.isBoolean(action)){

        }else{
            
        }
    }
    render(){
        const {message} = this.state;
        return(
            <Grid >
                <Row justifyContent="center" alignItems="center">
                    <View style={[Styles.wrapperPopup , {height:190,paddingHorizontal:20 , paddingVertical:20}]}>
                        <H3 style={{fontFamily:"Barlow-SemiBold"}}>
                            {message.title}
                        </H3>
                        <View style={{paddingVertical:5}}>
                            <Text style={{fontSize: 16 }}>
                                {message.body}
                            </Text>
                        </View>
                        <View style={{paddingTop:20}}>
                            <Grid>
                                <Row alignItems="center" justifyContent="flex-end">
                                    {message.button.map((data , i)=>(
                                        <Text key={i} style={[{color:data.color , paddingLeft:20,fontSize:17}]} onPress={() =>{ this.handleButtonAlert(data.action)}}>{data.name}</Text>                                     
                                    ))}
                                </Row>
                            </Grid>
                        </View>
                    </View>
                </Row>
            </Grid>
        )
    }
} 