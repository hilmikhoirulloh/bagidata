import React , {Component} from "react";
import {View , Text  , TouchableOpacity} from "react-native";


import CardPromotion from "./cardPromotion";

import  {Styles} from "../../../styles";

const TAKE_AWAY  = "../../../../../../";

export default class Promotion extends Component{
    componentWillMount(){
        
    }
    handleLoadImage = (condition) => {
        // console.log("console");\
        this.setState({
            loading :condition
        })
    }
    

    render(){
        const {dataArray , handleViewRecomend , willBuy} = this.props
        return(
            <View style={{padding:10}}> 
                {dataArray.map((data , i)=> (
                    <TouchableOpacity onPress={()=>{handleViewRecomend(i)}} key={i}>
                         <CardPromotion data={data} willBuy={willBuy} key={i} />              
                    </TouchableOpacity>
                ))}  
            </View>  
        )
    }
}