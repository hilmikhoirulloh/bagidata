import React , { Component } from "react" ;

import {View , Text   ,Image  , TouchableOpacity , RefreshControl, ScrollView, LayoutAnimation} from "react-native" ;
import { Styles , WINDOW_WIDTH , COLOR_SECONDARY} from "../../../styles";
import {CustomLayoutLinear} from "../animation";
import {Container , Thumbnail , Icon , Form ,Input  , Item  , Grid , Col , Row , Tab , Tabs }  from "native-base";

import Header from "../components/header";
import Promotion from "../components/promotion";

import { SetDefaultIcon } from "../actions";
import { connect } from "react-redux";
// import { persistor } from "../../../../redux/store";

import PROMOTION from "../../../variables/PROMOTION.json";

const MARGIN_HORIZONTAL = (((WINDOW_WIDTH-20)/5)-30)/2;
const TAKE_AWAY = "../../../../../../";


class Recommend extends Component {

    state = {
        selected2 : undefined,
        page : 0,
        promotion : PROMOTION,
        allIcon : [],
        scroll :true
    }
    constructor(props){
        super(props);
        this.props.navigator.setStyle({
            navBarHidden : true
        })
    }
    async componentDidMount(){
        console.log(this.props);
        if(!this.props.icon.hasSet){
           await this.props.dispatch(SetDefaultIcon());
        }
        this.setState({
            allIcon : this.props.icon.results
        })
    }
    handleHome = () => {
        this.props.navigator.dismissModal({
            animationType: 'none' 
        });
    }
    handleOpenPopUp (){
        this.props.navigator.showLightBox({
            screen: 'bagidata.Layout.Home.Recommend.Popup',
            passProps: {
                
            },
            style: {
                backgroundBlur: 'dark', 
                backgroundColor: '#44444480', 
                tapBackgroundToDismiss: true 
            }
        })
    }

    handleButtonBuy = ()=>{
        
        this.props.navigator.showLightBox({
            screen: 'bagidata.Layout.Home.Recommend.Alert',
            passProps : {
                type : 0
            },
            style: {
                backgroundBlur: 'dark', 
                backgroundColor: '#44444480', 
                tapBackgroundToDismiss: true 
            }
        })
    }

    handleChange(e){
        this.setState({
            page : e.i        
        })
    }
    handleOpenModalPromotion = (index) => {
        this.props.navigator.push({
            screen:"bagidata.Layout.Home.Recommend.EachPromotion",
            passProps:{
                index  
            }
        })
    }
    handlePageSearch = () =>{
        this.props.navigator.push({
            screen : "bagidata.Layout.Home.SearchPromotion",
            animationType : "none"
        })
    }

    scroll =null;
    handleScroll = (scroll) =>{
        LayoutAnimation.configureNext(CustomLayoutLinear);

        if(!scroll){
            this.scroll = setTimeout(()=>{
                LayoutAnimation.configureNext(CustomLayoutLinear);
                this.setState({
                    scroll:false 
                })
            }, 400);    
        }else{
            clearTimeout(this.scroll);
            this.setState({
                scroll 
            })
        }
    }
    render(){
        
        if(this.props.wallet){
        const {wallet} = this.props
        const {allIcon} =this.state ;
        const activeIcon = allIcon.filter((data)=>(data.status == true))
        
            return(
                <Container>
                    <Header onBack={this.handleHome} point={wallet.result ? wallet.result.point : "-"}/>
                        <View >
                            <Grid  style={[Styles.bgGrey , {paddingVertical:25 , paddingHorizontal:20 }]}>
                                <Row style={{marginTop:-3}}>
                                    <Col justifyContent="center">
                                        <Image square source={require(`${TAKE_AWAY}img/icon/payment.png`)} resizeMode="stretch" style={{width:70/2.1 , height:40/2.1}} />                      
                                    </Col>
                                    <Col style={Styles.center} alignItems="flex-end">
                                        <Text style={[Styles.fontBold , Styles.textColorSecondary , {fontSize:18}]}>
                                            Rp. {wallet.result ? wallet.result.cash : "-"}
                                        </Text >
                                    </Col>
                                </Row>
                            </Grid>
                        </View>

                        <Form style={{paddingHorizontal:10}}>
                            <Item rounded style={[Styles.bgGrey , Styles.itemTransfer , {borderColor:"transparent"}]}>
                                <Icon name="ios-search-outline" fontSize="11" color="#aaa"/>
                                <Input placeholder="Cari vocher kamu disini" onFocus={this.handlePageSearch} style={[{ fontSize:15 , color:"#666", textAlign:"left" , fontFamily:"Barlow-Regular"}]} placeholderTextColor="#aaa" />
                            </Item>
                        </Form>
                
                        <Tabs 
                            onChangeTab={(e)=>{this.handleChange(e)}}
                            tabBarUnderlineStyle={{backgroundColor:"#ec008c"}}
                            tabContainerStyle= {{elevation:0 }}
                            locked={true}
                            >
                            <Tab 
                                heading="Beli Voucher" 
                                tabStyle={{backgroundColor: 'white'}} 
                                activeTabStyle={{backgroundColor: 'white'}} 
                                textStyle={{color: '#aaa' , fontFamily:"Barlow-SemiBold"}}
                                activeTextStyle={{color: '#444', fontFamily:"Barlow-SemiBold"}}
                            >
                                <ScrollView 
                                    ref="cek"
                                    

                                    onScrollBeginDrag={()=>{this.handleScroll(true)}}
                                    onMomentumScrollBegin={()=>{this.handleScroll(true)}}
                                    
                                    onScrollEndDrag={()=>{this.handleScroll(false)}}
                                    onMomentumScrollEnd={()=>{this.handleScroll(false)}}
                                    refreshControl={
                                        <RefreshControl
                                            style={{backgroundColor: '#E0FFFF'}}
                                            refreshing={false}
                                            tintColor="#ff0000"
                                            title="Loading..."
                                            titleColor="#00ff00"
                                            colors={[COLOR_SECONDARY]}
                                            progressBackgroundColor="white"
                                    />}
                                >
                                    
                                    <View style={{marginBottom:20 , paddingTop:10, paddingHorizontal:10}}>
                                        <Text style={Styles.fontBold}>
                                            Sorotan
                                        </Text>
                                        <Text style={Styles.fontThin}>
                                            Sorotan promo yang mungkins kamu butuhkan
                                        </Text>
                                    </View>

                                    <Promotion dataArray={PROMOTION} willBuy={this.handleButtonBuy} handleViewRecomend={this.handleOpenModalPromotion}/>
                                    <View
                                        style={{height:90}}>
                                        
                                    </View>
                                </ScrollView>
                            
                            </Tab>
                            <Tab 
                                heading="Voucher Saya" 
                                tabStyle={{backgroundColor: 'white'}} 
                                textStyle={{color: '#aaa' , fontFamily:"Barlow-SemiBold"}}
                                activeTabStyle={{backgroundColor: 'white'}} 
                                activeTextStyle={{color: '#444' , fontFamily:"Barlow-SemiBold"}}
                                
                            >
                                <ScrollView 
                             
                                    refreshControl={
                                        <RefreshControl
                                            style={{backgroundColor: '#E0FFFF'}}
                                            refreshing={false}
                                            tintColor="#ff0000"
                                            title="Loading..."
                                            titleColor="#00ff00"
                                            colors={[COLOR_SECONDARY]}
                                            progressBackgroundColor="white"
                                        />}
                                    > 
                                    <Promotion dataArray={PROMOTION} willBuy={this.handleButtonBuy} handleViewRecomend={this.handleOpenModalPromotion}/>

                                </ScrollView>
                            </Tab>
                        </Tabs>
                    {
                    !this.state.page && !this.state.scroll ?
                        <View style={[Styles.bottomButton , Styles.bgWhite , Styles.bottomAction]}>
                            <Text style={[{textAlign : "center"}]}>
                                Hubungkan dan dapatkan keuntungan!
                            </Text>
                            <Grid style={{paddingTop:13}}>
                                <Row>
                                    <Col alignItems="center" justifyContent="center">
                                        <Image source={require("../../../../../../img/icon/facebook.png")} style={{width:37/3, height:69/3}}/>
                                    </Col>
                                    <Col alignItems="center" justifyContent="center">
                                        <Image source={require("../../../../../../img/icon/youtube.png")} style={{width:71/3, height:49/3}}/>
                                    </Col>
                                    <Col alignItems="center" justifyContent="center">
                                        <Image source={require("../../../../../../img/icon/twitter.png")} style={{width:71/3, height:58/3}}/>
                                    </Col>
                                    <Col alignItems="center" justifyContent="center">
                                        <Image source={require("../../../../../../img/icon/google.png")} style={{width:71/3, height:46/3}}/>
                                    </Col>
                                    <Col alignItems="center" justifyContent="center">
                                        <Image source={require("../../../../../../img/icon/facebook.png")} style={{width:37/3, height:69/3}}/>
                                    </Col>
                                </Row>
                            </Grid>
                        </View>
                    :
                    <View/>
                    }
                </Container>
            )
        }
        else{
            this.props.navigator.showModal({
                screen : "bagidata.Component.Loading"
            })
            return null;
        }
    }
} 

const mapStateToProps = (state)=>({
    icon : state.HandleIconHomeReducer,
    wallet : state.WalletReducer
})
export default connect(mapStateToProps)(Recommend);