const HandleIconState = {
    results : [],
    change: 0,
    isLoading : false,
    error :false,
    hasSet:false
}

export function HandleIconHomeReducer(state = HandleIconState , action) {
    switch(action.type){
        case "SET_DEFAULT_RECOMMEND_ICON_REDUCER_PENDING": 
            
            return {
                ...state , 
                isLoading : true,
                error : false,
                results :[]      
            }
        case "SET_DEFAULT_RECOMMEND_ICON_REDUCER_FULFILLED":     
            let get_data = action.payload.data;
            get_data.map((data , i) =>{
                if(i < 4){
                    data["status"] = true
                }else{
                    data["status"] = false
                }
            })
            return {
                ...state , 
                isLoading : false,
                error : false,
                results :get_data,     
                hasSet : true
            }
        case "SET_DEFAULT_RECOMMEND_ICON_REDUCER_REJECTED":     
            return {
                ...state , 
                isLoading : false,
                error : true,
                results :action.payload
            }
        case "SET_RECOMMEND_ICON_REDUCEER" :
            return {
                ...state,
                results : action.payload
            }
        default :
            return state    
    }
}
