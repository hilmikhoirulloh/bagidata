import React , { Component } from "react" ;
import {View , Text  ,Image  , TouchableOpacity } from "react-native" ;
import { Styles } from "../../../styles";
import {Container , Content , Button , Item  , Grid , Col , Row}  from "native-base";
import Header from "../components/header";

import EPULSA from "../../../variables/EPULSA.json";

import { connect } from "react-redux"; 


const TAKE_AWAY = "../../../../../../";
class ChargePulsa extends Component {
    state = {
        epulsa : EPULSA,
        nominal : "-"
    }
    constructor(props){
        super(props);
        this.props.navigator.setStyle({
            navBarHidden : true
        })
    }
    handleBack  = () => {
        this.props.navigator.resetTo({
            screen : "bagidata.Layout.Home.Buy"
        })
    }
    handlePressNominal(i , c){
        
        const alldata = EPULSA;
        for( data in alldata)
            for(microdata in alldata[data])
                alldata[data][microdata].status = false
        
        alldata[i][c].status = true;
        this.setState({
            epulsa : alldata,
            nominal: alldata[i][c].nominal 
        })
    }
    render(){
        const {wallet} = this.props
        return(
            <Container>
                <Header onBack={this.handleBack} /> 
                <Content style={{backgroundColor:"white" }}>
                    <Grid style={[Styles.bgGrey , {paddingVertical:10 , paddingHorizontal:30 }]}>
                        <Row>
                            <Col justifyContent="center">
                                <Image square source={require(`${TAKE_AWAY}img/icon/payment.png`)} resizeMode="stretch" style={{width:70/2.1 , height:40/2.1}} />                      
                            </Col>
                            <Col style={Styles.center} alignItems="flex-end">
                                <Text style={[Styles.fontBold , Styles.textColorSecondary , {fontSize:18}]}>
                                    Rp. {wallet.result ?  wallet.result.cash : "-"}
                                </Text >
                            </Col>
                        </Row>
                    </Grid>
                    
                    <Grid style={{paddingVertical:10 , paddingHorizontal:20 }}>
                        <Row style={{paddingBottom:10}}>
                            <Col justifyContent="center">
                                <Item  style={{borderBottomWidth:0}}>   
                                    <Image source={require(`${TAKE_AWAY}img/icon/phone.png`)} style={{width:18*0.8, height:30*0.8 , marginRight:10}} resizeMode="stretch"/>
                                    <View>
                                        <Text style={[Styles.smallBold ,{textAlign : "left"}]}>
                                            Hilmi
                                        </Text>
                                        <Text style={[{fontFamily:"Barlow-SemiBold" , marginTop:-5}, Styles.fontBlack]}>
                                            08987797989
                                        </Text>
                                    </View>              
                                </Item>                     
                            </Col>
                            <Col style={Styles.center} alignItems="flex-end">
                                <Image source={require(`${TAKE_AWAY}img/icon/contact.png`)} style={{width:30*0.8, height:33*0.8 }} resizeMode="stretch"/>                            
                            </Col>
                        </Row>
                        <View style={[Styles.hr ,{marginLeft:-20 , width:"120%"} ]}/>
                        <Row>
                            <Col>
                                <Text style={[Styles.fontBold , {paddingTop:20 , paddingBottom:10 , textAlign:"center"}]}>
                                    E - Pulsa              
                                </Text> 
                            </Col>
                        </Row>
                    </Grid>
                    <Grid>
                        {
                            this.state.epulsa.map((item , index)=> (
                            <Row key ={index}>
                                {item.map((col , cIndex)=>(
                                    
                                    <TouchableOpacity key={cIndex}    onPress={()=>{this.handlePressNominal(index,cIndex)}} style={[{height:55 , borderWidth:1  , borderColor:"#f8f8f8" , flex:1   , backgroundColor:col.status ? "#ffcaf5": "white" , alignItems:"center" , justifyContent:"center"}]}>        
                                        <Text >
                                            {col.nominal}
                                        </Text>
                                    </TouchableOpacity>                                         
                                    
                                ))}
                            </Row>
                        ))}
                        
                        
                    </Grid>
                    <Grid>
                        <Row style={{paddingHorizontal:20 , paddingVertical:10 }}>
                            <Col>
                                <Text style={[Styles.fontBold]}>
                                    Detail Pembayaran              
                                </Text> 
                            </Col>
                        </Row>
                        <Row style={{paddingVertical:10 , paddingHorizontal:20 }}>
                            <Col>
                                <Text style={[Styles.fontThin]}>
                                    Pulsa {this.state.nominal}              
                                </Text> 
                            </Col>
                            <Col>
                                <Text style={[Styles.fontThin , {textAlign:"right"}]}>
                                    Rp. {this.state.nominal}
                                </Text> 
                            </Col>
                        </Row>
                        <Row style={{padding:20 , backgroundColor : "#222e61" }}>
                            <Col>
                                <Item style={{borderBottomWidth:0}}>
                                    <Text style={[Styles.smallBold  ,{color:"white" ,  marginRight:3}]}>
                                        Uangku               
                                    </Text>
                                    <Image source={require(`${TAKE_AWAY}img/icon/white_money.png`)} style={{width:10 , height:10}} />                                    
                                </Item>
                            </Col>
                            <Col>
                                <Text style={[Styles.smallBold , {textAlign:"right" , color:"white"}]}>
                                    Rp. {this.state.nominal}
                                </Text> 
                            </Col>
                        </Row>
                    </Grid>
                </Content>
                <Button style={[Styles.bottomButton , Styles.bgPrimary]} full rounded>
                    <Text style={{color:"white" , fontFamily : "Barlow-SemiBold"}}>
                        Beli
                    </Text>
                </Button>
            </Container>
        )
    }
} 

const setStateToPorps = (state) => ({
    wallet : state.WalletReducer
})
export default connect(setStateToPorps)(ChargePulsa);