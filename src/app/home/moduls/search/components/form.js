import React , {Component} from "react";
import { reduxForm , Field} from "redux-form";
import {View} from "react-native"
import {SearchInput} from "./input";

class FormComponent extends Component {
    render(){
        return(
            <Field
                name = "search"
                placeholder="Cari vocher kamu disini"
                component={SearchInput}
            />
        )
    }
}

export default reduxForm({
    form : "search"
})(FormComponent)