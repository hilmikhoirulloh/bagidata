import React , {Component} from "react";
import {Header, Left, Icon, Body, Title, Right, Button} from "native-base";
import {View} from "react-native";
import {Styles} from "../../../styles";
import Form from "./form";

export default class HeaderComponent extends Component{
    render(){
        const {onBack} = this.props; 
        return(
            <Header style={[Styles.bgWhite,{marginBottom:3 }]}>
        
                <Button transparent onPress={()=>{onBack()}} style={{marginLeft : -15}}>
                    <Icon name='arrow-back' style={Styles.fontBlack}/>
                </Button>

                <View style={{flex:1 , paddingTop:2}}>
                    <Form/>
                </View>
            </Header>
            
        )
    }
}
