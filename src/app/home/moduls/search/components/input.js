import React , {Component} from "react";
import {Item , Icon , Input} from "native-base";
import {Styles} from "../../../styles";

export class SearchInput extends Component {
    render(){
        const {input , placeholder , meta }  = this.props;    
        return(
            <Item rounded style={[Styles.bgGrey , {borderColor:"transparent"}]}>
                <Icon name="ios-search-outline" fontSize="11" color="#aaa"/>
                <Input autoFocus={true} {...input} placeholder={placeholder}  style={[{fontSize:15, color:"#666", textAlign:"left" , fontFamily:"Barlow-Regular"}]} placeholderTextColor="#aaa" />
            </Item>
        )
    }
}