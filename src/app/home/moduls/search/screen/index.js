import React , {Component} from "react";
import {Container , Content} from "native-base";
import Header from "../components/header"; 
import {connect} from "react-redux";
import Promotion from "../components/cardPromotion";

class SearchComponent extends Component {
    handleBack = ()=>{
        this.props.navigator.pop({
            animationType: 'none' 
        })
    }
    handleButtonBuy = (index)=>{
        
        this.props.navigator.showLightBox({
            screen: 'bagidata.Layout.Home.Recommend.Alert',
            passProps : {
                type : 0
            },
            style: {
                backgroundBlur: 'dark', 
                backgroundColor: '#44444480', 
                tapBackgroundToDismiss: true 
            }
        })
    }
    render(){
        return(
            <Container>
                <Header onBack={this.handleBack} />
                <Content padder>
                    <Promotion willBuy={this.handleButtonBuy} />
                </Content>
            </Container>
        )
    }
}

export default connect()(SearchComponent);