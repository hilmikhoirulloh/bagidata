import React  from "react";
import {Item , Input} from "native-base";
import {Styles} from "../../../styles";


export function InputText({
    input,
    placeholder,
    meta: {  touched, error } 
})
{
    return(
        <Item style={[Styles.bgGrey , Styles.itemGopay , {borderRadius:5}  ]}>
            <Input {...input} placeholder={placeholder} style={{fontSize:12 , fontFamily:"Barlow-Regular"}} keyboardType="numeric"/>
        </Item>
    )
}