import  React , {Component} from "react";

import {Styles , COLOR_PRIMARY} from "../../../styles";
import {reduxForm , Field} from "redux-form";
import validate from "../validate";
import {InputText} from "./input";

import {Grid, Row , Col , Button , Text } from "native-base";


class FormChangeGopay extends Component {
    render(){
        const {set} = this.props;
        return(
            <Grid style={{padding:10 }}>
                <Row alignItems="center" >
                    <Col style={{flex: 0.9 , paddingRight:5  }} alignItems="center" justifyContent="center">
                        <Field
                            placeholder = "input data"
                            name = "money"
                            component = {InputText}
                        />
                    </Col>
                    <Col style={{flex: 0.2 }} >
                        <Button  onPress={()=>{set()}} style={{backgroundColor:COLOR_PRIMARY , borderRadius:5 ,  paddingHorizontal:5 , height:35}}>
                            <Text style={[{color:"white"} , Styles.smallBold]}>
                                OK
                            </Text>
                        </Button>
                    </Col>
                </Row>
            </Grid >
        )
    }
}

export default reduxForm({
    form : 'buyGopay',
})(FormChangeGopay);