import React , { Component } from "react" ;

import {View , Text  ,Image  } from "react-native" ;
import { Styles } from "../../../styles";
import {Container , Content  ,  Button  , Item  , Grid , Col , Row}  from "native-base";
import Header from "../components/header";

import FormChangeGopay from "../components/form";

import {connect} from "react-redux";

const TAKE_AWAY = "../../../../../../";

class BuyGopay extends Component {
    state = {
        nominal : false
    }
    constructor(props){
        super(props);
        this.props.navigator.setStyle({
            navBarHidden : true
        })
    }
    handleBack = () => {
        this.props.navigator.resetTo({
            screen : "bagidata.Layout.Home.Buy"
        })
    }
    handleSubmit = ()=>{
        this.setState({
            nominal : this.props.nominal.values.money
        })
    }
    render(){
        const {wallet} = this.props;
        const {nominal} = this.state;
        return(
            <Container>
                <Header onBack={this.handleBack}/>
                <Content style={{backgroundColor:"white" }}>
                    <Grid style={[Styles.bgGrey , {paddingVertical:10 , paddingHorizontal:30 }]}>
                        <Row>
                            <Col justifyContent="center">
                                <Image square source={require(`${TAKE_AWAY}img/icon/payment.png`)} resizeMode="stretch" style={{width:70/2.1 , height:40/2.1}} />                      
                            </Col>
                            <Col style={Styles.center} alignItems="flex-end">
                                <Text style={[Styles.fontBold ,Styles.textColorSecondary, {fontSize:18}]}>
                                    Rp. {wallet.result ?  wallet.result.cash : "-" } 
                                </Text >
                            </Col>
                        </Row>
                    </Grid>
                    
                    <Grid style={{paddingTop:10}}>
                        <Row style={{paddingBottom:10 , paddingHorizontal:20}}>
                            <Col justifyContent="center">
                                <Item  style={{borderBottomWidth:0}}>   
                                    <Image source={require(`${TAKE_AWAY}img/icon/phone.png`)} style={{width:18*0.8, height:30*0.8 , marginRight:10}} resizeMode="stretch"/>
                                    <View>
                                        <Text style={[Styles.smallBold ,{textAlign : "left"}]}>
                                            Hilmi
                                        </Text>
                                        <Text style={[{fontFamily:"Barlow-SemiBold" , marginTop:-5}, Styles.fontBlack]}>
                                            08987797989
                                        </Text>
                                    </View>              
                                </Item>                     
                            </Col>
                            <Col style={Styles.center} alignItems="flex-end">
                                <Image source={require(`${TAKE_AWAY}img/icon/contact.png`)} style={{width:30*0.8, height:33*0.8 }} resizeMode="stretch"/>                            
                            </Col>
                        </Row>
                        
                        <View style={[Styles.hr ,{marginLeft:-20 , width:"120%"} ]}/>
                        <Row>
                            <Col alignItems="center" justifyContent="center" style={{paddingVertical:20}}>
                                <Image source={require(`${TAKE_AWAY}img/icon/text_gopay.png`)} style={{width:100 , height:20 }} resizeMode="stretch" />
                            </Col>
                        </Row>
                    </Grid>
                    
                    <View style={Styles.hr  }/>
                    
                    <FormChangeGopay {...this.props} set={this.handleSubmit}/>
                        
                    <Grid >
                        <Row style={{paddingHorizontal:20}}>
                            <Col>
                                <Text style={[Styles.fontBold]}>
                                    Detail Pembayaran              
                                </Text> 
                            </Col>
                        </Row>
                        <Row style={{paddingHorizontal:20 , paddingVertical:10}}>
                            <Col>
                                <Text style={[Styles.fontThin]}>
                                    Gopay {nominal? nominal : "-" }              
                                </Text> 
                            </Col>
                            <Col>   
                                <Text style={[Styles.fontThin , {textAlign:"right"}]}>
                                    Rp. {nominal? nominal : "-" }
                                </Text> 
                            </Col>
                        </Row>
                        <Row style={{paddingVertical:20 , paddingHorizontal:20 , backgroundColor : "#222e61" }}>
                            <Col>
                                <Item style={{borderBottomWidth:0}}>
                                    <Text style={[Styles.smallBold  ,{color:"white" ,  marginRight:3}]}>
                                        Uangku               
                                    </Text>
                                    <Image source={require(`${TAKE_AWAY}img/icon/white_money.png`)} style={{width:10 , height:10}} />                                    
                                </Item>
                            </Col>
                            <Col>
                                <Text style={[Styles.smallBold , {textAlign:"right" , color:"white"}]}>
                                    Rp. {nominal? nominal : "-" }
                                </Text> 
                            </Col>
                        </Row>
                    </Grid>
                </Content>
                <Button style={[Styles.bottomButton , Styles.bgPrimary]} full rounded>
                    <Text style={{color:"white" , fontFamily:"Barlow-SemiBold"}}>
                        Beli
                    </Text>
                </Button>
            </Container>
        )
    }
} 

const mapStateToProps = (state) => ({
    nominal : state.form.buyGopay,
    wallet : state.WalletReducer,
})

export default connect(mapStateToProps)(BuyGopay);