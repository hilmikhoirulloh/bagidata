import React, {Component} from "react";
import Header from '../components/header';
import {Container, Content, Grid, Row, Col} from 'native-base';
import {Text, Image} from "react-native"; 
import Choose from "../components/choose";
import {Styles} from "../../../styles";

import {connect} from "react-redux";

class Transaction extends Component{
    handleBack = ()=>{
        this.props.navigator.dismissModal({
            animationType: 'none' 
        });
    }
    handleChangePage = (page)=>{
        this.props.navigator.push({
            screen:`bagidata.Layout.Home.${page}`,
            animationType:"none"
        });
    }
    render(){ 
        const {wallet} = this.props;
        return(
            <Container>
                <Header onBack={this.handleBack}/>
                <Content>
                    <Grid style={[Styles.bgGrey , {paddingVertical:10 , paddingHorizontal:30 }]}>
                        <Row>
                            <Col justifyContent="center">
                                <Image square source={require(`../../../../../../img/icon/payment.png`)} resizeMode="stretch" style={{width:70/2.1 , height:40/2.1}} />                      
                            </Col>
                            <Col style={Styles.center} alignItems="flex-end">
                                <Text style={[Styles.fontBold ,Styles.textColorSecondary, {fontSize:18}]}>
                                    Rp. {wallet.result ?  wallet.result.cash : "-" } 
                                </Text >
                            </Col>
                        </Row>
                    </Grid>
                    <Choose onPressItem={this.handleChangePage}/>
                </Content>
            </Container>
        )
    }
}

const mapStateToProps = (state) => ({
    wallet : state.WalletReducer
})
export default connect(mapStateToProps)(Transaction); 