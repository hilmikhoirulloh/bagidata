import React , {Component} from "react";
import {View, TouchableWithoutFeedback, Text, Image, StyleSheet} from "react-native";
import {Grid, Col} from "native-base";
import {Styles} from "../../../styles"; 

const StylesComponent = StyleSheet.create({
    disabledBoxConnection:{
        width:"100%" , 
        height:90 ,  
        borderRadius:10,
        backgroundColor:"#f8f8f8",
        elevation :2
    }
})
export default class Choose extends Component{
    render(){
        const {onPressItem} = this.props;
        return(
            <View style={{ paddingVertical:10 ,paddingHorizontal:20 , marginHorizontal:-5 , flexDirection:"row" , flexWrap :"wrap" }}>           
                <View style={{width:"50%" , padding:5}}>
                    <TouchableWithoutFeedback onPress={()=>{onPressItem("Buy")}}>                
                        <View style={StylesComponent.disabledBoxConnection}>
                            <Grid>
                                <Col alignItems="center" justifyContent="center">
                                    <Image source={require("../../../../../../img/icon/buy.png")} style={{width:67/2,height:67/2}} resizeMode="stretch"/>
                                    <Text style={[Styles.colorGrey , Styles.boldSmall]} >
                                        Beli
                                    </Text>
                                </Col>
                            </Grid>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                <View style={{width:"50%" , padding:5}}>
                    <TouchableWithoutFeedback onPress={()=>{onPressItem("Pay")}}>                
                        <View style={StylesComponent.disabledBoxConnection}>
                            <Grid>
                                <Col alignItems="center" justifyContent="center">
                                    <Image source={require("../../../../../../img/icon/pay.png")} style={{width:67/2,height:67/2}} resizeMode="stretch"/>
                                    <Text style={[Styles.colorGrey , Styles.boldSmall]} >
                                        Bayar
                                    </Text>
                                </Col>
                            </Grid>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                <View style={{width:"50%" , padding:5}}>
                    <TouchableWithoutFeedback onPress={()=>{onPressItem("Transfer")}}>                
                        <View style={StylesComponent.disabledBoxConnection}>
                            <Grid>
                                <Col alignItems="center" justifyContent="center">
                                    <Image source={require("../../../../../../img/icon/transfer.png")} style={{width:67/2,height:67/2}} resizeMode="stretch"/>
                                    <Text style={[Styles.colorGrey , Styles.boldSmall]} >
                                        Transfer
                                    </Text>
                                </Col>
                            </Grid>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                <View style={{width:"50%" , padding:5}}>
                    <TouchableWithoutFeedback onPress={()=>{onPressItem("Favorite")}}>                
                        <View style={StylesComponent.disabledBoxConnection}>
                            <Grid>
                                <Col alignItems="center" justifyContent="center">
                                    <Image source={require("../../../../../../img/icon/favorite.png")} style={{width:67/2,height:67/2}} resizeMode="stretch"/>
                                    <Text style={[Styles.colorGrey , Styles.boldSmall]} >
                                        Favorite
                                    </Text>
                                </Col>
                            </Grid>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </View>
        )
    }
} 