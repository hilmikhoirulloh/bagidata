import axios from "axios";
import {URL_API} from "../../../../../config";
import {header} from "../../../../helper";
// import {store} from "../../../redux/store";

export function SetDefaultIcon(){
    return {
        type : "SET_DEFAULT_RECOMMEND_ICON_REDUCER",
        payload : axios.get(`${URL_API}/api/v1/category`, header())
    }
}

export function SetChangeIcon(data){   
    return{
        type : "SET_RECOMMEND_ICON_REDUCEER",
        payload : data,
        change : Math.random() 
    }
}
