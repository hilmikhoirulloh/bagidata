import React,{ Component } from "react";

import {View, Text ,Image, TouchableWithoutFeedback, RefreshControl} from "react-native";
import {Container, Row, Col, Grid, Content, Card, CardItem, Icon } from "native-base"

import Header from "../components/header";
import Category from "../components/category";
import Rank from "../components/rank";
import Mission from "../components/mission";
import AddProfit from "../components/addProfit";

import {Styles , WINDOW_WIDTH  , COLOR_SECONDARY} from "../../../styles";

import {SetDefaultIcon, SetChangeIcon} from "../action";
import Promotion from "../components/promotion";
import {connect} from "react-redux";


const TAKE_AWAY  = "../../../../../../";
class Main extends Component{
    state = {
        allIcon : false
    }
    constructor(props){
        super(props)
        this.props.navigator.setStyle({
            navBarHidden : true
        })
    }
    async componentDidMount(){
        console.log(this.props.icon);
        if(!this.props.icon.hasSet){
           await this.props.dispatch(SetDefaultIcon());
        }
        this.setState({
            allIcon : this.props.icon.results
        })
        
    }
    handleChangePage = (page) => {        
        this.props.navigator.showModal({
            screen: `bagidata.Layout.Home.${page}`,
            animationType : "none" 
        });
            
    }
    handleViewRecomend = (index) =>{
        this.props.navigator.push({
            screen:"bagidata.Layout.Home.EachPromotion",
            alnimationType : "none" ,
            passProps:{
                index  
            }
        })
    }
    openPopupCategory  = ()=>{
        this.props.navigator.showLightBox({
            screen: 'bagidata.Layout.Home.PopupCategory',
            style: {
                backgroundBlur: 'dark', 
                backgroundColor: '#44444480', 
                tapBackgroundToDismiss: true 
            }
        })
    }
    handleRefresh = () => {
        console.log("hilmi");
    }
    render(){
        if(! this.props.wallet.isLoading){
        const {wallet} = this.props;
        return(
            <Container>
                <Header point={wallet.result ? wallet.result.point : "-" } takeAway={TAKE_AWAY}/>
                    <Content 
                        padder
                        refreshControl={
                            <RefreshControl
                                style={{backgroundColor: '#E0FFFF'}}
                                refreshing={false}
                                tintColor="#ff0000"
                                title="Loading..."
                                titleColor="#00ff00"
                                colors={[COLOR_SECONDARY]}
                                progressBackgroundColor="white"
                            />}
                    >
                    <Grid style={{paddingHorizontal:30 , paddingVertical:10 , backgroundColor:"#f8f8f8" , marginTop:-10, marginHorizontal:-25}}>
                        <Row>
                            <Col style={{flex:1}} justifyContent="center">
                                <Image square source={require(`${TAKE_AWAY}img/icon/payment.png`)} resizeMode="stretch" style={{width:70/1.4 , height:40/1.4}} />                      
                            </Col >
                            <Col style={{flex:2}} >
                                <TouchableWithoutFeedback onPress={()=>{this.handleChangePage("Transaction")}}>
                                    <View>
                                        <Row alignItems="center" justifyContent="flex-end">
                                            <Text style={[{textAlign:'right' , fontFamily:"Barlow-SemiBold"  , marginLeft:0 , fontSize:30} , Styles.textColorSecondary]}>
                                                Rp. {wallet.result ? wallet.result.cash : "-"}
                                            </Text>
                                            <Icon type="FontAwesome" name="chevron-right" style={{fontSize:12 , paddingLeft:10}}/>
                                        </Row>
                                    </View>
                                </TouchableWithoutFeedback>
                            </Col>
                        </Row>
                    </Grid>                  
                    <View style={{marginHorizontal:-10}}>
                        <Rank onPress={() => {this.handleChangePage("DetailStatus")}}/>
                        <Category onPressMore={this.openPopupCategory} activeIcon={[]}/>                    
                        <Mission data={[1 , 2]}/>
                        <AddProfit />
                    </View>
                    
                    <Promotion dataArray={[1]} handleViewRecomend={this.handleViewRecomend}/>
                    

                </Content>
            </Container>
        )

        }else{
            this.props.navigator.showModal({
                screen : "bagidata.Component.Loading"
            })
            return null;
        }
    }
}


const mapStateToProps = (state) => ({
    wallet : state.WalletReducer,
    icon :state.HandleIconHomeReducer
})

export default connect(mapStateToProps)(Main)