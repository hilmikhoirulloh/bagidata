import React, {Component} from "react";
import {Text,  View, Image, ProgressBarAndroid, TouchableWithoutFeedback, StyleSheet} from "react-native";
import {Styles, COLOR_SECONDARY, COLOR_PRIMARY} from "../../../styles";
import {Row, Col} from "native-base";
const TAKE_AWAY = "../../../../../../";
const StylesComponent= StyleSheet.create({
    wrapper :{
        backgroundColor:COLOR_SECONDARY, 
        height:110, 
        alignItems:"center", 
        marginRight:-15, 
        flexDirection:"row", 
        justifyContent:"space-around"
    }
})
export default class RankComponent extends Component{
    render(){
        const {onPress} = this.props;
        return (
            <TouchableWithoutFeedback onPress={()=> {onPress ? onPress()  :null;}}>
                <View style={{borderBottomColor:"#eee", borderBottomWidth:5}}>
                    <View style={StylesComponent.wrapper}>
                        <View style={{flexDirection:"row" , paddingLeft:10}}>
                            <View>
                                <Image source={require(`${TAKE_AWAY}/img/gamification/7.png`)} style={{width:330/4.5, height:330/4.5}}/>
                            </View>
                            <View style={{paddingLeft:10}}>
                                <Text style={[Styles.fontBold, {fontSize:20, color:"white"}]}>
                                    DIAMOND
                                </Text>
                                <Text style={[Styles.fontThin , {fontSize:14, color:"white"}]}>
                                    Investor
                                </Text>
                                <Text style={{color:COLOR_PRIMARY, paddingVertical:10, fontSize:12}}>
                                    Lihat status investor
                                </Text>
                            </View>
                        </View>
                        <View>
                            <Image source={require(`${TAKE_AWAY}/img/gamification/7__.png`)} style={{width:467/3, height:331/3}}/>
                        </View>
                    </View>
                    <View style={{paddingHorizontal:20, paddingVertical:15, backgroundColor:"#f8f8f8"}}>
                        <Row>
                            <Col style={{flex:1}}>
                                <Text style={[{fontSize:15} , Styles.fontBlack]}>
                                XP 1500
                                    <Text style={[Styles.fontBold , {fontSize:11 , color: "#999"}]}>
                                        /5000
                                    </Text>
                                </Text>
                            </Col>
                            <Col justifyContent="center" style={{flex:2}} >
                                <View style={{ borderRadius:10 , width:"100%",  overflow:"hidden" }}>
                                    <ProgressBarAndroid
                                        styleAttr="Horizontal"
                                        indeterminate={false}
                                        progress={0.5}
                                        style={{transform: [{ scaleX: 1.0 }, { scaleY: 2.5 }], marginVertical:-3  }}    
                                    />
                                </View>
                            </Col>
                        </Row>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        )
    }
}