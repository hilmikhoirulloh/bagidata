import React, {Component} from "react";
import {ListItem, Left, Body, Right} from "native-base";
import {Text, TouchableWithoutFeedback, Image, View, ProgressBarAndroid} from "react-native";
import {Disable, InProcess, Finish}  from "../../../components/buttonMission";


export default class ListItemMissionComponent extends Component{
    TypeButton(type, data){
        switch(type){
            case "InProcess" :
                return <InProcess data={data}/>;
            case "Disable" :
                return <Disable data={data}/>;
            case "Finish":
                return <Finish/>;
        }
    }
    render(){
        return(
            <ListItem onPress={()=>{console.log("hilmi")}} icon style={{borderBottomColor:"#eee", borderBottomWidth:1 ,height:60 ,  marginLeft:-10 , paddingLeft:30}}>
                <Left style={{borderBottomWidth:0 , width:40}}>
                    <Image  />
                </Left>
                <Body style={{borderBottomWidth:0}}>
                    <View style={{ borderRadius:10 , width:"100%",  overflow:"hidden", paddingVertical:0}}>
                        <ProgressBarAndroid
                            styleAttr="Horizontal"
                            indeterminate={false}
                            progress={0.5}
                            style={{transform: [{ scaleX: 1.0 }, { scaleY: 2.5 }]  , marginVertical:-3 }}    
                        />
                    </View>
                </Body>
                <Right style={{borderBottomWidth:0, height:"100%", marginLeft:10, marginRight:5 }}>
                    {this.TypeButton("Finish" , {})}     
                </Right>
            </ListItem>
        )
    }
}