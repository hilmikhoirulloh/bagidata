import React, {Component} from "react";
import {View, Text, Image, TouchableOpacity, StyleSheet, TouchableWithoutFeedback} from "react-native";
import {Grid, Row, Col, Card, CardItem, Body} from "native-base";

import {Styles,COLOR_PRIMARY} from "../../../styles";

const TAKE_AWAY = "../../../../../../";
export default class CategoryComponent extends Component {
    render(){
        const {activeIcon, onPressMore, onPressComplete} = this.props;
        return(
            <View style={{borderBottomColor:"#eee", borderBottomWidth:5}}>
                <View style={[StylesComponent.padding ,{borderBottomColor:"#f8f8f8", borderBottomWidth:1}]}>
                    <Text style={[ Styles.fontBlack, {fontSize:17}]}>
                        Tambah Keuntungan
                    </Text>
                </View>
                
                <Grid style={{paddingHorizontal:20, paddingVertical:10}}>
                    <Row>
                        <Col>
                            <Card style={{borderRadius:10, overflow:"hidden"}}>
                                <CardItem>
                                    <Body>
                                        <Text>
                                            hilmi khoirulloh
                                        </Text>
                                    </Body>
                                </CardItem>
                            </Card>
                        </Col>
                        <Col>
                            <Card style={{borderRadius:10, overflow:"hidden"}}>
                                <CardItem>
                                    <Body>
                                        <Text>
                                            hilmi khoirulloh
                                        </Text>
                                    </Body>
                                </CardItem>
                            </Card>
                        </Col>
                        <Col>
                            <Card style={{borderRadius:10, overflow:"hidden"}}>
                                <CardItem>
                                    <Body>
                                        <Text>
                                            hilmi khoirulloh
                                        </Text>
                                    </Body>
                                </CardItem>
                            </Card>
                        </Col>
                    </Row>
                </Grid>
            </View>

        )
    }
}
const StylesComponent= StyleSheet.create({
    wrapperGroup : {
        flexWrap:"wrap" , 
        alignItems:"flex-start" , 
        justifyContent: "space-between"  , 
        flexDirection:"row" , 
        paddingVertical: 10,
        borderTopColor:"#f8f8f8",
        borderTopWidth:1,
        borderBottomColor:"#f8f8f8",
        borderBottomWidth:1,
    },
    wrapper : {
        width:68 , 
        alignItems:"center" , 
        justifyContent:"center" , 
        paddingVertical:10
    },
    padding : {
        paddingVertical:20, 
        paddingHorizontal:25
    }
})