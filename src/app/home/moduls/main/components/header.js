import React , { Component} from "react"
import {Header, Left, Body, Right, Text} from "native-base";
import {Image } from "react-native";
import {Styles} from "../../../styles";

export default class HeaderComponent extends Component {
    render(){
        
        return (
            <Header style={[Styles.bgWhite,{marginBottom:3 }]}>
                <Left>
                    <Image source={require(`../../../../../../img/logo.png`)} resizeMode="stretch" style={ Styles.logoSize} />                    
                </Left>
                <Body/>
                <Right style={{alignItems:"center" , paddingRight:5}}>
                    <Text style={[Styles.fontBold , ]}>
                        {this.props.point}
                    </Text>
                    <Image source={require(`../../../../../../img/icon/point.png`)} resizeMode="stretch" style={Styles.pointSize} />                    
                </Right >
            </Header>
        )
    }
}