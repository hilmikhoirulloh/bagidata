import React, {Component} from "react";
import {View, Text, Image, TouchableOpacity, StyleSheet, TouchableWithoutFeedback} from "react-native";
import {Styles,COLOR_PRIMARY} from "../../../styles";

const TAKE_AWAY = "../../../../../../";
export default class CategoryComponent extends Component {
    render(){
        const {activeIcon, onPressMore, onPressComplete} = this.props;
        return(
            <View style={{borderBottomColor:"#eee", borderBottomWidth:5}}>
                <View style={StylesComponent.padding}>
                    <Text style={[ Styles.fontBlack, {fontSize:17}]}>
                        Kategori
                    </Text>
                </View>
                
                <View  style={StylesComponent.wrapperGroup}>
                    {activeIcon.map((data , i)=> (
                        <TouchableOpacity  style={StylesComponent.wrapper} key={i}>
                            <Image source={{"uri": data.icon_active  }} style={{width:30 , height:30}}/>
                            <Text style={[Styles.smallBold]}>
                                {data.kategori_barang}
                            </Text>
                        </TouchableOpacity>
                    ))}
                    <TouchableOpacity onPress={() => {onPressMore()}} style={StylesComponent.wrapper}>
                            <Image source={require(`${TAKE_AWAY}img/icon/more_active.png`)} style={{width:30 , height:30 }}   />                                                    
                            <Text style={Styles.smallBold}>
                                More             
                            </Text>
                    </TouchableOpacity>
                </View>
                <View style={StylesComponent.padding}>
                    <TouchableWithoutFeedback onPress={()=> onPressComplete}>
                        <View style={{flex:1}}>
                            <Text style={[{color:COLOR_PRIMARY, textAlign:"right" , fontSize:16}]}>
                                Selengkapnya
                            </Text>
                        </View>

                    </TouchableWithoutFeedback>
                </View>
            </View>

        )
    }
}
const StylesComponent= StyleSheet.create({
    wrapperGroup : {
        flexWrap:"wrap" , 
        alignItems:"flex-start" , 
        justifyContent: "space-between"  , 
        flexDirection:"row" , 
        paddingVertical: 10,
        borderTopColor:"#f8f8f8",
        borderTopWidth:1,
        borderBottomColor:"#f8f8f8",
        borderBottomWidth:1,
    },
    wrapper : {
        width:68 , 
        alignItems:"center" , 
        justifyContent:"center" , 
        paddingVertical:10
    },
    padding : {
        paddingVertical:20, 
        paddingHorizontal:25
    }
})