import React , {Component} from "react";
import {Image , Text , View } from "react-native";
import {Card , CardItem , Item , Button} from "native-base";
import {Styles , COLOR_SECONDARY} from "../../../styles";

import {Bubbles} from "react-native-loader";

export default class CardPromotion extends Component{
    state ={
        loading : false
    }
    handleLoadImage  = (loading) => {
        this.setState({
            loading
        })
    }
    render(){
        return(
            <Card style={{padding:0 , borderRadius:10 , overflow:"hidden"}} >
                <CardItem cardBody >
                    <Image onLoadStart={()=> { this.handleLoadImage(true) }} onLoadEnd={()=> { this.handleLoadImage(false) }} source={{"uri":"https://www.ishn.com/ext/resources/900x550/airplane-plane-flight-900.jpg?1527776784"}} style={{flex:1 , height:110 , width:null }} />
                    {
                        this.state.loading ?
                            <View style={Styles.coverAndroid}>
                                <View>
                                    <Bubbles size={4} color={COLOR_SECONDARY} />
                                </View>
                            </View>
                    :
                        null
                    }
                </CardItem>
                <CardItem >
                    <Text style={[Styles.fontBold ,{fontSize:15}]}>
                        [Semarang-Jakarta] Gratis terbang dari Garuda Indonesia
                    </Text >
                </CardItem >
                <CardItem style={Styles.wrapperPanelBottom}>
                    <Item style={{borderBottomWidth:0}}>
                        <Image style={{height:15 , width:15 , marginRight:5 }} source={require(`../../../../../../img/icon/coin.png`)}/>
                        <Text style={Styles.smallBold}>
                            10.000 pts
                        </Text>
                    </Item>
                    <Button style={[Styles.buttonBuy , Styles.flat]}><Text style={[Styles.fontBold , {color:"white"} ]}> Beli </Text></Button>
                </CardItem >                            
            </Card>
        )
    }
}