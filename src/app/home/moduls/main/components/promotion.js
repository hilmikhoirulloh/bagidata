import React , {Component} from "react";
import {View , Text  , TouchableWithoutFeedback, StyleSheet} from "react-native";


import CardPromotion from "./cardPromotion";

import  {Styles} from "../../../styles";

const TAKE_AWAY  = "../../../../../../";

export default class Promotion extends Component{
    componentWillMount(){
        
    }
    handleLoadImage = (condition) => {
        // console.log("console");\
        this.setState({
            loading :condition
        })
    }
    

    render(){
        const {dataArray , handleViewRecomend} = this.props
        return(
            <View style={{marginHorizontal:-10}}>
                <View style={[StylesComponent.padding ,{borderBottomColor: "#f8f8f8", borderBottomWidth:1}]}>
                    <Text style={[ Styles.fontBlack, {fontSize:17 }]}>
                        Promo
                    </Text>
                </View>
                <View style={{paddingVertical:10 , paddingHorizontal:20}}>
                {dataArray.map((data , i)=> (
                    <TouchableWithoutFeedback onPress={()=>{handleViewRecomend(i)}} key={i}>
                    <CardPromotion data={data} key={i}/>              
                    </TouchableWithoutFeedback>
                ))}  
                </View>

            </View>  
        )
    }
}

const StylesComponent= StyleSheet.create({
    wrapperGroup : {
        flexWrap:"wrap" , 
        alignItems:"flex-start" , 
        justifyContent: "space-between"  , 
        flexDirection:"row" , 
        paddingVertical: 10,
        borderTopColor:"#f8f8f8",
        borderTopWidth:1,
        borderBottomColor:"#f8f8f8",
        borderBottomWidth:1,
    },
    wrapper : {
        width:68 , 
        alignItems:"center" , 
        justifyContent:"center" , 
        paddingVertical:10
    },
    padding : {
        paddingVertical:20, 
        paddingHorizontal:25
    }
})