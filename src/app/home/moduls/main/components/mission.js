import React, {Component} from "react";
import {View, Text, Image, TouchableOpacity, StyleSheet, TouchableWithoutFeedback, ProgressBarAndroid} from "react-native";
import {Separator, Item, Icon, Row, Col} from "native-base";
import {Styles, COLOR_PRIMARY, COLOR_SECONDARY} from "../../../styles";

import ListItem from "./listItemMission" ;
import {Collapse,CollapseHeader, CollapseBody} from 'accordion-collapse-react-native'

const TAKE_AWAY = "../../../../../../";
export default class CategoryComponent extends Component {
    state = {
        active : false
    }
    handleToggle = async (data) => {
        await this.setState({
            active : data
        });
        await this.refs.collapse.setState({
            show : data
        })
        
    }
    render(){
        const {activeIcon, onPressMore, onPressComplete, data} = this.props;
        return(
            <View style={{borderBottomColor:"#eee", borderBottomWidth:5}}>
                <View style={StylesComponent.padding}>
                    <Text style={[Styles.fontBlack, {fontSize:18}]}>
                        Misi Harian
                    </Text>
                </View>
                
                <View  style={StylesComponent.wrapperGroup}>
                    <Collapse  onToggle={this.handleToggle.bind(this)} ref="collapse" >
                        <CollapseHeader style={{backgroundColor:COLOR_SECONDARY, padding:20 ,borderRadius:10, flexDirection:"row" }}>
                            <Separator style={{backgroundColor:"transparent", height:"auto" , paddingLeft:0}}>
                                <Row>
                                    <Col>
                                        <Row alignItems="center" >
                                            <Text style={[{color:"white" , fontSize:17}]}>
                                                2/3
                                            </Text>
                                            <Icon name="ios-checkmark-circle" style={{color:"teal", fontSize:14 ,paddingLeft:5}}/>
                                        </Row>
                                    </Col>
                                    <Col style={{flex:2}}>
                                        <Text style={[{color:"white" , fontSize:17}]}>Misi Selesai</Text>
                                    </Col>
                                    <Col >
                                        <Row justifyContent="space-around" alignItems="center">
                                            <View>
                                                <Image/>
                                                <Text style={[{color:"white" , fontSize:17 }]}>
                                                    X
                                                </Text>    
                                            </View>
                                        
                                            <Text style={{color:"white" , fontSize:17 }}>
                                                10
                                            </Text>
                                        </Row>
                                    </Col>
                                </Row>
                                <Row alignItems="center" style={{paddingTop:5}} >
                                    <View style={{ borderRadius:10 , width:"100%",  overflow:"hidden" ,backgroundColor:"white", paddingVertical:0}}>
                                        <ProgressBarAndroid
                                            styleAttr="Horizontal"
                                            indeterminate={false}
                                            progress={0.5}
                                            style={{transform: [{ scaleX: 1.0 }, { scaleY: 2.5 }]  , marginVertical:-3 }}    
                                        />
                                    </View>
                                </Row>
                            </Separator>
                            <View style={{paddingLeft:10}}>
                            <Icon name={this.state.active ? "ios-arrow-up" : "ios-arrow-down" } style={{color:"#eee"}}/>
                            </View>
                        </CollapseHeader>    
                        <CollapseBody style={{marginHorizontal:-20}}>
                            
                            {data.map((data_children,j) =>(
                                <ListItem  key={j} />
                            ))}
                            
                        </CollapseBody>
                    </Collapse>
                </View>
     
                <View style={StylesComponent.padding}>
                    <TouchableWithoutFeedback onPress={()=> onPressComplete}>
                        <View style={{flex:1}}>
                        <Text style={[{fontSize:16,color:COLOR_PRIMARY, textAlign:"right"}]}>
                            Selengkapnya
                        </Text>
                        </View>

                    </TouchableWithoutFeedback>
                </View>
            </View>

        )
    }
}
const StylesComponent= StyleSheet.create({
    wrapperGroup : {
         padding: 10,
    },
    wrapper : {
        width:68 , 
        alignItems:"center" , 
        justifyContent:"center" , 
        paddingVertical:10
    },
    padding : {
        paddingVertical:20, 
        paddingHorizontal:25
    }
})