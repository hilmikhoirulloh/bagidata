import React, {Component} from "react";
import {Styles, COLOR_PRIMARY} from "../styles";
import {Button, Icon} from "native-base";
import {Text, Image, View} from "react-native";

export class InProcess extends Component {
    render(){
        return(
            <Button rounded style={[Styles.flat,{backgroundColor:COLOR_PRIMARY, paddingHorizontal:10, alignItems:"center"}]}>
                <Image/>
                <Text style={{color:"white", fontSize: 17}}>
                    x
                </Text>
                <Text style={{paddingLeft:5, color:"white", fontSize: 17}}>
                    10
                </Text>
            </Button>
        )
    }
}

export class Disable extends Component {
    render(){
        return(
            <Button rounded style={[Styles.flat,{backgroundColor:"#ddd" , paddingHorizontal:10, alignItems:"center"}]}>
                <Image/>
                <Text style={{color:"white", fontSize: 17}}>
                    x
                </Text>
                <Text style={{paddingLeft:5, color:"white", fontSize: 17}}>
                    10
                </Text>
            </Button>
        )
    }
}

export class Finish extends Component {
    render(){
        return(
            <View>
                <View style={{flex:1 , justifyContent:"center" }}>
                    <Icon name="md-checkmark" style={{color:"teal", fontSize: 30,  paddingLeft:0}}/>
                </View>
            </View>
        )
    }
}

