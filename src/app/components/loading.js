import React  , {Component} from "react";
import {
    ActivityIndicator , 
    Image , 
    View,
    StyleSheet
} from "react-native";

import {connect} from "react-redux";

class Loading extends Component {
    constructor(props){
        super(props);
    }

    render(){
        const {wallet} = this.props;
        
        if (!wallet.isLoading ){
            this.props.navigator.dismissModal({
                animationType : "none"
            })
        }
        return(
            <View style={[Styles.coverAndroid , {zIndex: 1000  }]}>
                <View style={[Styles.coverAndroid , {zIndex:1000  , backgroundColor: "#222e61" , opacity:0.95}]}>
                </View>
                <View style={[Styles.coverAndroid , {zIndex:1000}]}>
                    <ActivityIndicator size={80} color="#ffffff"  />
                </View>
                <View style={[Styles.coverAndroid , {zIndex:1000 }]}>
                    <Image source={require("../../../img/icon_logo.png")} style={{width:282/8 , height:245/8}}/>
                </View>
            </View>

        )
    }
}

const Styles=  StyleSheet.create({
    coverAndroid:{
        position :"absolute", 
        bottom:0 , 
        right:0 , 
        left:0,
        top : 0,
        alignItems:"center",
        justifyContent:"center"
    }
})

const mapStateToProps = (state)=>({
    wallet : state.WalletReducer
});
export default connect(mapStateToProps)(Loading)