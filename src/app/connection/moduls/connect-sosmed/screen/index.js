import React  , {Component } from "react";
import {View  ,Image , Dimensions} from "react-native";
import {Container  , Header , Left , Body , Title , Right , Icon  , Button , Text } from "native-base"

import Data from "../../../variables/warning.json";

import {Styles , WIDTH_WINDOW} from "../../../styles";

import {connect} from "react-redux";


const TAKE_AWAY = "../../../../../../";
class ConnectSosmed extends Component {
    constructor(props){
        super(props)
        this.props.navigator.setStyle({
            navBarHidden : true
        })
    }
    handleBack(){
        this.props.navigator.dismissModal({
            animationType: 'slide-down'
        });
    }
    render(){
        const {name , walpaperUrl} = this.props;
        return(
            <Container style={Styles.bgWhite}>
                <Header style={[Styles.bgWhite,{marginBottom:3 }]}>
                    <Left>
                        <Button transparent onPress={()=>{this.handleBack()}}>
                            <Icon name='arrow-back' style={Styles.fontBlack}/>
                        </Button>
                    </Left>
                    <Body>
                        <Title style={Styles.fontBlack}>
                            Media Sosial
                        </Title>
                    </Body>
                    <Right />
                </Header>
                    <View style={{flex:1}}>
                        <View style={{position : "absolute", top :30, left:0, right:0}}>
                            <Text style={[Styles.fontBlack, Styles.fontWeight, {fontSize:18, textAlign:"center"}]}>
                                {name}
                            </Text>
                        </View>
                        <Image source={walpaperUrl}  style={{width:"100%", height:WIDTH_WINDOW/2}}/>
                        <View style={{flex:1  , flexDirection:"column" , alignItems:"center"}}>
                            <View style={{marginVertical:10 , width:"90%"}}>
                                <Text style={{fontFamily:"Barlow-Thin" , fontSize:17 , textAlign:"center"}}>
                                    Kamu dapat 1 point  perhari dengan  menghubungkan  {name.toLowerCase()} mu  ke bagidata dan kamu bisa menggunakan poin kamu untuk ditukarkan dengan voucher yang kamu sukai
                                </Text>
                            </View>
                            <View style={{marginVertical:10 , width:"90%"}}>
                                <Text style={{fontFamily:"Barlow-Thin" , fontSize:17 , textAlign:"center"}}>
                                    Selain itu kamu  juga akan dapat uang  setiap kali data  kamu digunakan
                                </Text>
                            </View>
                            <View style={{marginVertical:10 , width:"90%"}}>
                                <Text style={{fontFamily:"Barlow-Thin" , fontSize:17 , textAlign:"center"}}>
                                    Bagidata akan menggunakan data {name.toLowerCase()} kamu setelah kamu hubungkan sebagai berikut
                                </Text>
                            </View>
                        </View>
                    </View >
                    <View style={Styles.bottomButton}>
                        <Button full rounded style={[Styles.flat ,  Styles.bgPrimary]}>
                            <Text> 
                                Hubungkan
                            </Text> 
                        </Button > 
                    </View>
                </Container>
            )
        }
    }

    const mapStateToProps = (state) =>({
        
    });

export default connect(mapStateToProps)(ConnectSosmed);