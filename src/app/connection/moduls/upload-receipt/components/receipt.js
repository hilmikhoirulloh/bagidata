import React , {Component} from "react";
import {View , Text , Image , TouchableWithoutFeedback } from "react-native";
import {Grid , Row , Col} from "native-base";

import {Styles} from "../../../styles";
export default class Receipt extends Component {
    render(){
        const {dataArray , onPress} = this.props;
        return (
            <View style={{ padding:10 , marginHorizontal:-5 , flexDirection:"row" , flexWrap :"wrap" }}>           
                {dataArray.map((data,i)=>(
                    <View style={{width:"50%" , padding:5}} key={i}>
                        <TouchableWithoutFeedback onPress={()=>{onPress(data)}}>                
                            <View style={Styles.disabledBoxConnection}>
                                <Grid>
                                    <Col alignItems="center" justifyContent="center">
                                        <Image source={data.icon.url} style={{width:data.icon.width/3.4,height:data.icon.height/3.4}} resizeMode="stretch"/>
                                        <Text style={[Styles.fontBlack , {fontSize:20}]}>
                                            {data.title}
                                        </Text>
                                        <Text style={[Styles.colorGrey , Styles.boldSmall]} >
                                            Struk sedang diproses
                                        </Text>
                                    </Col>
                                </Grid>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                ))}
            </View>
        )
    }
} 

