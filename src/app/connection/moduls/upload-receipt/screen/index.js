import React  , {Component } from "react";
import {View , Text ,Image , TouchableOpacity} from "react-native";
import {Container , Content , Header , Left , Body , Title , Right , Icon  ,Grid , Col , Row , Button  } from "native-base"

import {RECEIPT} from "../variables";

import {Styles} from "../../../styles";

import {connect} from "react-redux";

import Receipt from "../components/receipt";

const TAKE_AWAY = "../../../../../../";
class UploadReceipt extends Component {
    constructor(props){
        super(props)
        this.props.navigator.setStyle({
            navBarHidden : true
        })
    }
    handleBack(){
        this.props.navigator.dismissModal({
            animationType: 'slide-down'
        });
    }
    handlChangePage = (data)=>{
        // console.log(int)
        this.props.navigator.push({
            screen : "bagidata.Layout.Connection.PageWarning",
            passProps : {
                int : data.id
            }
        })
    }
    render(){
        return(
            <Container style={Styles.bgWhite}>
                <Header style={[Styles.bgWhite,{marginBottom:3 }]}>
                    <Left>
                        <Button transparent onPress={()=>{this.handleBack()}}>
                            <Icon name='arrow-back' style={Styles.fontBlack}/>
                        </Button>
                    </Left>
                    <Body>
                        <Title style={Styles.fontBlack}>
                            Unggah Struk
                        </Title>
                    </Body>
                    <Right />
                </Header>
                <Content>
                    <Grid style={{paddingHorizontal:30 , paddingVertical:15 , backgroundColor:"#f8f8f8" }}>
                        
                        <Row alignItems="center" justifyContent="center">
                            
                            <Col >
                                <Row alignItems="center" justifyContent="center">
                                    <Image source={require(`${TAKE_AWAY}img/icon/waiting.png`)} style={{width:25 , height:25}} resizeMode="stretch"/>
                                    <Text style={{ paddingLeft:5 , fontSize:17}}>
                                        15
                                    </Text>
                                </Row>
                            </Col>
                            <Col>
                                <Row alignItems="center" justifyContent="center">
                                    <Image source={require(`${TAKE_AWAY}img/icon/done.png`)} style={{width:25 , height:25}} resizeMode="stretch"/>
                                    <Text style={{ paddingLeft:5 , fontSize:17}}>
                                        15
                                    </Text>
                                </Row>
                            </Col>
                            <Col>
                                <Row alignItems="center" justifyContent="center">
                                    <Image source={require(`${TAKE_AWAY}img/icon/rejected.png`)} style={{width:25 , height:25}} resizeMode="stretch"/>
                                    <Text style={{ paddingLeft:5 , fontSize:17}}>
                                        15
                                    </Text>
                                </Row>
                            </Col>
                        </Row>
                    </Grid>

                    <Receipt dataArray={RECEIPT} onPress={this.handlChangePage} />
                    {/* <Grid style={{padding:20 , marginHorizontal:-5}}>
                        <Row>
                            <Col style={{padding:5}}>
                                <TouchableOpacity onPress={()=>{this.handlChangePage(0)}}>
                                    <View style={Styles.disabledBoxConnection}>
                                        <Grid>
                                            <Col alignItems="center" justifyContent="center">
                                                <Image source={require(`${TAKE_AWAY}img/icon/retail.png`)} style={{width:90/3.4,height:80/3.4}} resizeMode="stretch"/>
                                                <Text style={[Styles.fontWeight,Styles.fontBlack , {fontSize:18}]}>
                                                    Retail
                                                </Text>
                                                <Text style={[Styles.colorGrey , Styles.boldSmall]} >
                                                    Struk sedang diproses
                                                </Text>
                                            </Col>
                                        </Grid>
                                    </View>
                                </TouchableOpacity>
                            </Col> */}
                            {/* <Col style={{padding:5}}>
                                <TouchableOpacity  onPress={()=>{this.handlChangePage(1)}}>
                                    <View style={Styles.disabledBoxConnection}>
                                        <Grid>
                                            <Col alignItems="center" justifyContent="center">
                                                <Image source={require(`${TAKE_AWAY}img/icon/train.png`)} style={{width:70/3.4,height:90/3.4}} resizeMode="stretch"/>
                                                <Text style={[Styles.fontWeight,Styles.fontBlack , {fontSize:18}]}>
                                                    Kerata
                                                </Text>
                                                <Text style={[Styles.colorGrey , Styles.boldSmall]} >
                                                    Struk sedang diproses
                                                </Text>
                                            </Col>
                                        </Grid>
                                    </View>
                                </TouchableOpacity>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={{padding:5}}>
                                <TouchableOpacity  onPress={()=>{this.handlChangePage(2)}}>
                                    <View style={Styles.disabledBoxConnection}>
                                        <Grid>
                                            <Col alignItems="center" justifyContent="center">
                                                <Image source={require(`${TAKE_AWAY}img/icon/aircraft.png`)} style={{width:90/3.4,height:90/3.4}} resizeMode="stretch"/>
                                                <Text style={[Styles.fontWeight,Styles.fontBlack , {fontSize:18}]}>
                                                    Pesawat
                                                </Text>
                                                <Text style={[Styles.colorGrey , Styles.boldSmall]} >
                                                    Struk sedang diproses
                                                </Text>
                                            </Col>
                                        </Grid>
                                    </View>
                                </TouchableOpacity>
                            </Col>
                            <Col style={{padding:5}}>
                                <TouchableOpacity  onPress={()=>{this.handlChangePage(3)}}>
                                    <View style={Styles.disabledBoxConnection}>
                                        <Grid>
                                            <Col alignItems="center" justifyContent="center">
                                                <Image source={require(`${TAKE_AWAY}img/icon/bus.png`)} style={{width:84/3.4,height:90/3.4}} resizeMode="stretch"/>
                                                <Text style={[Styles.fontWeight,Styles.fontBlack , {fontSize:18}]}>
                                                    Bus
                                                </Text>
                                                <Text style={[Styles.colorGrey , Styles.boldSmall]} >
                                                    Struk sedang diproses
                                                </Text>
                                            </Col>
                                        </Grid>
                                    </View>
                                </TouchableOpacity>
                            </Col> */}
                        {/* </Row> */}
                        {/* <Row>
                            <Col style={{padding:5}}>
                                <TouchableOpacity  onPress={()=>{this.handlChangePage(4)}}>
                                    <View style={Styles.disabledBoxConnection}>
                                        <Grid>
                                            <Col alignItems="center" justifyContent="center">
                                                <Image source={require(`${TAKE_AWAY}img/icon/restaurant.png`)} style={{width:90/3.4,height:90/3.4}} resizeMode="stretch"/>
                                                <Text style={[Styles.fontWeight,Styles.fontBlack , {fontSize:18}]}>
                                                    Restoran
                                                </Text>
                                                <Text style={[Styles.colorGrey , Styles.boldSmall]} >
                                                    Struk sedang diproses
                                                </Text>
                                            </Col>
                                        </Grid>
                                    </View>
                                </TouchableOpacity>
                            </Col>
                            <Col style={{padding:5}}>
                                
                            </Col>
                            </Row> */}
                        {/* </Grid> */}
                    </Content>
                </Container>
            )
        }
    }

    const mapStateToProps = (state) =>({
        
    });

export default connect(mapStateToProps)(UploadReceipt);