import React ,{Component} from "react";
import {Grid, Row, Col, ListItem, List, Body, Right, Icon , Left} from "native-base";
import {Text, View, TouchableWithoutFeedback, Image} from "react-native";
import {Styles , COLOR_PRIMARY} from "../../../styles";

const TAKE_AWAY ="../../../../../../";
export default class ControlData extends Component {
    render(){
        const {changePage , dataArray , openPopUp} = this.props;
        return(
            <View>
                <Grid  style={{ paddingHorizontal:20 , paddingVertical : 15 , borderBottomColor:"#eee" , borderBottomWidth:1 }}>
                <Row alignItems="center" justifyContent="center">
                        <Col justifyContent="center" >
                            <Text style={[Styles.fontWeight  ,Styles.fontBlack , {fontSize:17}]}>Data Kontrol</Text>
                        </Col>
                        <Col justifyContent="center" alignItems="flex-end">
                            <TouchableWithoutFeedback onPress={()=>{openPopUp()}}>
                                <Icon name="ios-alert-outline" style={{color:COLOR_PRIMARY , fontSize:16 , fontWeight:"bold"}}/>
                            </TouchableWithoutFeedback>
                        </Col>
                    </Row>
                </Grid>
                
                <ListItem itemHeader first style={Styles.ListItem} onPress={()=>{changePage("DataControl")}}>
                    <Left >
                        <Image source={require(`${TAKE_AWAY}img/icon/setting.png`)} style={{height:15 , width:22}} resizeMode="stretch"/>
                    </Left>
                    <Body style={{marginLeft:-170}}>
                        <Text>Atur Penggunaan Data</Text>
                    </Body>
                    <Right>
                        <Icon name="ios-arrow-forward" />
                    </Right>
                </ListItem>

                <List>
                    {dataArray.map((data , j)=>(
                        <ListItem itemHeader first style={Styles.ListItem} key={j}>
                            <Left >
                                <Text>
                                    Politik
                                </Text>
                            </Left>
                            <Right>
                                <Text style={[Styles.fontWeight , {color:COLOR_PRIMARY}]}>
                                    Izinkan
                                </Text>
                            </Right>
                        </ListItem>
                    ))}
                </List>
            </View>
                
        )
    }
} 