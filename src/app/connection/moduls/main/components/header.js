import React , {Component } from "react";
import {Header, Body, Title, Right} from "native-base";
import {Styles} from "../../../styles";

export default class HeaderComponent extends Component{
    render(){
        return(
            <Header style={[Styles.bgWhite , {marginBottom: 3}]}>
                <Body style={{marginLeft:10}}>
                    <Title style={[Styles.fontBlack]}>
                        Koneksi 
                    </Title> 
                </Body> 
                <Right />
            </Header>
        )
    }
}