import React , {Component} from "react";
import {View , Text ,TouchableWithoutFeedback , Image } from "react-native";
import {Row , Grid , Col , Icon} from "native-base" ; 

import {ActiveBox , DisableBox} from "./typeConnectSosialMedia";

import {Styles, COLOR_PRIMARY} from "../../../styles";

const TAKE_AWAY ="../../../../../../";
export default class ConnectSosialMedia extends Component {
    box(data , i){
        switch (data.type) {
            case 1 :
                return <ActiveBox data={data} key={i} onPress={this.props.onPress}/>;
            case 0 :
                return <DisableBox data={data} key={i} onPress={this.props.onPress}/>;
        } 
    }
    render(){
        const {dataArray , openPopUp}=this.props;
        return(
            <View>
                <Grid  style={{ paddingHorizontal:20 , paddingVertical : 15 , borderBottomColor:"#eee" , borderBottomWidth:1 }}>
                    <Row alignItems="center" justifyContent="center">
                        <Col justifyContent="center" >
                            <Text style={[Styles.fontWeight  ,Styles.fontBlack , {fontSize:17}]}>Media Sosial</Text>
                        </Col>
                        <Col justifyContent="center" alignItems="flex-end">
                            <TouchableWithoutFeedback onPress={()=>{openPopUp("PopupSocialMedia")}}>
                                <Icon name="ios-alert-outline" style={{color:COLOR_PRIMARY , fontSize:16 , fontWeight:"bold"}}/>
                            </TouchableWithoutFeedback>
                        </Col>
                    </Row>
                </Grid>
                <View style={{ padding:10 ,flexDirection:"row" , flexWrap :"wrap" , borderBottomColor:"#eee" , borderBottomWidth:5 }}>
                    {this.props.dataArray.map((data , i)=> (
                        this.box(data,i)
                ))}
                </View>
            </View>
        )
    }
}
