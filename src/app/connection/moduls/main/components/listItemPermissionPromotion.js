import React , {Component} from "react";
import {ListItem, Left, Body, Right} from "native-base";
import {Styles , COLOR_SECONDARY } from "../../../styles";
import {Image, Text } from "react-native";
import { Switch } from 'react-native-switch';

const TAKE_AWAY = "../../../../../../";

export default class ListItemPermissionPromotion extends Component{
    state = {
        conditionSwitch  : false
    }
    handleToggleSwitch = () => { 
        this.setState({
            conditionSwitch : !this.state.conditionSwitch
        })
    }
    render(){
        const {data} = this.props;
        return(
            <ListItem itemHeader first style={Styles.ListItem}>
                <Left >
                    <Image source={require(`${TAKE_AWAY}img/icon/email_outline.png`)} style={{height:15 , width:22}} resizeMode="stretch"/>
                </Left>
                <Body style={{marginLeft:-170}}>
                    <Text>Email</Text>
                </Body>
                <Right>
                    <Switch
                        value={this.state.conditionSwitch}
                        onValueChange={this.handleToggleSwitch}
                        disabled={false}
                        activeText={'On'}
                        inActiveText={'Off'}
                        circleSize={22}
                        circleBorderWidth={1}
                        barHeight={22}
                        backgroundActive={COLOR_SECONDARY}
                        backgroundInactive={'gray'}
                        circleActiveColor={'white'}
                        circleInActiveColor={'white'}
                        changeValueImmediately={true}
                        innerCircleStyle={{ alignItems: "center", justifyContent: "center" }} 
                        outerCircleStyle={{}} 
                        renderActiveText={false}
                        renderInActiveText={false}
                        switchLeftPx={2} 
                        switchRightPx={2} 
                        switchWidthMultiplier={2}
                    />
                </Right>
            </ListItem>
        )
    }
}