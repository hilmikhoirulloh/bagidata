import React , {Component} from "react";
import {View, Text , TouchableOpacity , Image} from "react-native";
import {Grid , Row , Icon, H3 , Col , Container , Content} from "native-base";
import { Styles , HEIGHT_WINDOW} from "../../../styles";

const MARGIN_VERTICAL = HEIGHT_WINDOW * 55 / 100 / 2;

const TAKE_AWAY = "../../../../../../";
export default class PopupReceipt extends Component{
    handleDissPopup(){
        this.props.navigator.dismissLightBox();
    }
    render(){
        return(
            <Container style={{flex:1  ,  alignItems:"center" , justifyContent:"center" , backgroundColor:"transparent" , paddingVertical:MARGIN_VERTICAL}}>
                <Content style={[Styles.bgWhite , Styles.boxPopup]}>
                    <Grid style={{padding:20}} >
                        <Row justifyContent="flex-end"> 
                            <TouchableOpacity onPress={()=>{this.handleDissPopup()}}>
                                <Icon name="close" style={{fontSize:18}}/>
                            </TouchableOpacity>
                        </Row> 
                        <Row > 
                            <H3 style={{paddingTop:3}}>
                                Informasi Media Sosial
                            </H3>
                        </Row> 
                        <Row style={{paddingVertical:20}}>
                            <Image source={require(`${TAKE_AWAY}img/popup-walpaper/popup_connection.png`)} style={{flex:1 , height: 80 , width:null}} resizeMode="stretch"/>
                        </Row>
                       
                        <Row alignItems="center" justifyContent="center" style={{paddingBottom:20}} >
                            <View style={{width:"95%"}}>
                                <Text style={{textAlign:"center" , color:"#999" , fontWeight:"100"}}>
                                    Dengan menghubungkan media sosial kamu ke Bagidata kamu akan dapat banyak keuntungan dalam bentuk poin yang nanti bisa kamu lakukan dengan vocher 
                                </Text>
                            </View>
                        </Row>

                    </Grid>  
                </Content>
            </Container>
            

        )
    }
} 