import React , {Component} from "react";
import {View , Text ,TouchableWithoutFeedback , Image } from "react-native";
import {Row , Grid } from "native-base" ; 

import {Styles} from "../../../styles";

const TAKE_AWAY ="../../../../../../";

export class ActiveBox extends Component{
    render(){
        const {data ,onPress } =this.props; 
        return(
            <View style={{width:"33.3%" , padding:5}}>
                <TouchableWithoutFeedback onPress={()=>{onPress(data)}} >
                    <View style={Styles.boxConnection}>
                        <Grid>
                            <Row alignItems="center" justifyContent="center">
                                <Image source={data.icon.url} style={{width:data.icon.width/2,height:data.icon.height/2}} resizeMode="stretch"/>
                            </Row>
                            <Row alignItems="center" justifyContent="center">
                                <Text style={[Styles.fontBlack , Styles.fontWeight]}>+1</Text>
                                <Image source={require(`${TAKE_AWAY}img/icon/point.png`)} style={{width:18*1.5,height:10*1.5 ,marginLeft:3 , marginTop:3}} resizeMode="stretch"/>
                                <Text style={[{fontSize:8, marginTop:6} , Styles.fontWeight]}>/hari</Text>
                            </Row>
                            <Row alignItems="center" justifyContent="center">
                                <Image source={require(`${TAKE_AWAY}img/icon/before_connect.png`)} style={{width:22*2.5,height:6.5*2.5  }} resizeMode="stretch"/>
                            </Row>
                        </Grid>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        )
    }
}
export class DisableBox extends Component{
    render(){
        const {data } =this.props; 
        return(
            <View style={{width:"33.3%" , padding:5}}>
                <TouchableWithoutFeedback>
                    <View style={Styles["disabledBoxConnection"]}>
                        <Grid>
                            <Row alignItems="center" justifyContent="center">
                                <Image source={data.icon.url} style={{width:data.icon.width/2,height:data.icon.height/2}} resizeMode="stretch"/>
                            </Row>
                            <Row alignItems="center" justifyContent="center">
                                <Text style={[Styles.fontWeight ,{color:"#bbb"}]}>
                                    Segera
                                </Text>
                            </Row>
                        </Grid>
                    </View>            
                </TouchableWithoutFeedback>
            </View>
        )
    }
}
