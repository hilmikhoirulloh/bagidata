import React , {Component} from "react";
import {View, Text, Image, TouchableWithoutFeedback} from "react-native";
import {Grid, Row, Col, Icon, List} from "native-base";
import ListItem from "./listItemPermissionPromotion";
import {Styles , COLOR_PRIMARY} from "../../../styles";

export default class PermissionPromotion extends Component {
    render(){
        const {dataArray, openPopUp} = this.props;
        return (
            <View>
                <Grid  style={{ paddingHorizontal:10 , paddingVertical : 10 , borderBottomColor:"#eee" , borderBottomWidth:1 }}>
                    <Row alignItems="center" justifyContent="center">
                        <Col justifyContent="center" >
                            <Text style={[Styles.fontWeight  ,Styles.fontBlack , {fontSize:17}]}>Izin Promosi</Text>
                        </Col>
                        <Col justifyContent="center" alignItems="flex-end">
                            <TouchableWithoutFeedback onPress={()=>{openPopUp()}}>
                                <Icon name="ios-alert-outline" style={{color:COLOR_PRIMARY , fontSize:16 , fontWeight:"bold"}}/>
                            </TouchableWithoutFeedback>
                        </Col>
                    </Row>
                </Grid>
                
                <List>
                    {dataArray.map((data , j)=>(
                        <ListItem data={data} key={j}/>
                    ))}
                </List>
            </View>
        )
    }
} 