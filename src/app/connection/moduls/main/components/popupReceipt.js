import React , {Component} from "react";
import {View, Text , TouchableOpacity , Image} from "react-native";
import {Grid , Row , Icon, H3 , Col , Container , Content} from "native-base";
import { Styles , HEIGHT_WINDOW} from "../../../styles";

const MARGIN_VERTICAL = HEIGHT_WINDOW * 19 / 100 / 2;

const TAKE_AWAY = "../../../../../../";
export default class PopupReceipt extends Component{
    handleDissPopup(){
        this.props.navigator.dismissLightBox();
    }
    render(){
        return(
            <Container style={{flex:1  ,  alignItems:"center" , justifyContent:"center" , backgroundColor:"transparent",  paddingVertical:MARGIN_VERTICAL}}>
                <Content style={[Styles.bgWhite , Styles.boxPopup]}>
                    <Grid style={{padding:20}} >
                        <Row justifyContent="flex-end"> 
                            <TouchableOpacity onPress={()=>{this.handleDissPopup()}}>
                                <Icon name="close" style={{fontSize:18}}/>
                            </TouchableOpacity>
                        </Row> 
                        <Row > 
                            <H3 style={{paddingTop:3}}>
                                Informasi Unggah Struk
                            </H3>
                        </Row> 
                        <Row style={{paddingVertical:20}}>
                            <Image source={require(`${TAKE_AWAY}img/popup-walpaper/popup_receipt.png`)} style={{flex:1 , height: 70 , width:null}} resizeMode="stretch"/>
                        </Row>
                       
                        <Row alignItems="center" justifyContent="center" style={{paddingBottom:20}} >
                            <View style={{width:"95%"}}>
                                <Text style={{textAlign:"center" , color:"#999" , fontWeight:"100"}}>
                                    Dengan mengunggah struk kamu ke bagidata kamu akan mendapatakan keuntungan dalam bentuk uang,  yang nantinya bisa kamu gunakan untuk beli dan bayar ini itu
                                </Text>
                            </View>
                        </Row>

                        <Row style={{ height:60}}  alignItems="center" justifyContent="center">
                            <Col style={{flex:1 }}>
                                <View style={{flex:1 ,paddingRight:20 , paddingTop:10 , paddingBottom:15}}>
                                    <Image source={require(`${TAKE_AWAY}img/icon/scan_active.png`)} style={{flex:1 , width:null , height:null}} resizeMode="stretch" />
                                </View>
                            </Col>
                            <Col style={{flex:4}}>
                                <Text style={{color:"#999"}}>
                                    Tombol dengan ikon tesebut dalam menu koneksi berfungsi untuk mengunggah struk
                                </Text>
                            </Col>
                        </Row>

                        <Row style={{ height:65}}  alignItems="center" justifyContent="center">
                            <Col style={{flex:1 }}>
                                <View style={{flex:1 ,paddingRight:20 , paddingTop:10 , paddingBottom:15}}>
                                    <Image source={require(`${TAKE_AWAY}img/icon/waiting.png`)} style={{flex:1 , width:null , height:null}} resizeMode="stretch" />
                                </View>
                            </Col>
                            <Col style={{flex:4}}>
                                <Text  style={{color:"#999"}}>
                                    Menunjukan jumlah nota yang  sedang proses
                                </Text>
                            </Col>
                        </Row>

                        <Row style={{ height:65}} alignItems="center" justifyContent="center" >
                            <Col style={{flex:1 }}>
                                <View style={{flex:1 ,paddingRight:20 , paddingTop:10 , paddingBottom:15}}>
                                    <Image source={require(`${TAKE_AWAY}img/icon/done.png`)} style={{flex:1 , width:null , height:null}} resizeMode="stretch" />
                                </View>
                            </Col>
                            <Col style={{flex:4}}>
                                <Text  style={{color:"#999"}}>
                                    Menunjukan jumlah nota yang  sudah disetujui
                                </Text>
                            </Col>
                        </Row>

                        <Row style={{ height:65}} alignItems="center" justifyContent="center" >
                            <Col style={{flex:1 }}>
                                <View style={{flex:1 ,paddingRight:20 , paddingTop:10 , paddingBottom:15}}>
                                    <Image source={require(`${TAKE_AWAY}img/icon/rejected.png`)} style={{flex:1 , width:null , height:null}} resizeMode="stretch" />
                                </View>
                            </Col>
                            <Col style={{flex:4}}>
                                <Text  style={{color:"#999"}}>
                                    Menunjukan jumlah nota yang tidak disetujui
                                </Text>
                            </Col>
                        </Row>
                    </Grid>  
                </Content>
            </Container>
            

        )
    }
} 