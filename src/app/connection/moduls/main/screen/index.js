import React , {Component} from "react";
import { Text , TouchableWithoutFeedback , Image, RefreshControl} from "react-native";
import {ListItem, List  , Grid, Row , Col , Icon , H2   , Left , Container , Content , Right , Body , Button} from "native-base";
import {Styles , COLOR_PRIMARY , COLOR_SECONDARY } from "../../../styles";
import { SOCIAL_MEDIA ,  } from "../variables";

import ConnectSocialMedia from "../components/connectSocialMedia";
import PermissionPromotion from "../components/permissionPromotion";
import ControlData from "../components/controlData";

import Header from "../components/header";

import {connect} from "react-redux";


const TAKE_AWAY = "../../../../../../";

class ConnectSosmed extends Component {
    constructor(props){
        super(props)
        this.props.navigator.setStyle({
            navBarHidden : true
        })
    }
    handleOpenPopup = (popup)=>{
        this.props.navigator.showLightBox({
            screen: `bagidata.Layout.Connection.${popup}`,            
            passProps: {
                
            },
            style: {
                backgroundBlur: 'dark', 
                backgroundColor: '#44444480', 
                tapBackgroundToDismiss: true 
            }
        })
    }

    handlePressConnectSocialMedia = (data) => {
        this.props.navigator.showModal({
            screen : "bagidata.Layout.Connection.ConnectSosmed",
            passProps: {
                id : data.id,
                name : data.name ,
                walpaperUrl : data.walpaper.url,
                walpaperHeight : data.walpaper.height,
                walpaperWidth : data.walpaper.width
            }
        })
    }

    handleChangePage = (page)=>{
        this.props.navigator.showModal({
            screen : `bagidata.Layout.Connection.${page}`
        })
    }
    render(){
        const {wallet} = this.props;
        return(
            <Container>
                <Header />
                <Content 
                    refreshControl={
                        <RefreshControl
                            style={{backgroundColor: '#E0FFFF'}}
                            refreshing={false}
                            tintColor="#ff0000"
                            title="Loading..."
                            titleColor="#00ff00"
                            colors={[COLOR_SECONDARY]}
                            progressBackgroundColor="white"
                        />}
                >
                    <Grid style={{paddingHorizontal:20 , paddingVertical:10 , backgroundColor:"#f8f8f8" }}>
                        <Row alignItems="center" justifyContent="center">
                            <Col >
                                <H2 style={Styles.fontWeight}>
                                    {wallet.result ? wallet.result.point : "-"}
                                </H2>
                            </Col>
                            <Col alignItems="flex-end">
                                <Image source={require(`${TAKE_AWAY}img/icon/point.png`)} style={{width:53 , height:31}} resizeMode="stretch"/>
                            </Col>
                        </Row>
                    </Grid>
                    <Grid style={{ paddingHorizontal:20 , paddingVertical : 15 , borderBottomColor:"#eee" , borderBottomWidth:1 }}>
                        <Row alignItems="center" justifyContent="center">
                            <Col justifyContent="center" >
                                <Text style={[Styles.fontWeight  ,Styles.fontBlack , {fontSize:17}]}>Unggah Struk</Text>
                            </Col>
                            
                            <Col justifyContent="center" alignItems="flex-end">
                                <TouchableWithoutFeedback onPress={()=>{this.handleOpenPopup("PopupReceipt")}}>
                                    <Icon name="ios-alert-outline" style={{color:COLOR_PRIMARY , fontSize:16 , fontWeight:"bold"}}/>
                                </TouchableWithoutFeedback>
                            </Col>
                        </Row>
                    </Grid>
                    <Grid style={{ padding: 10 , borderBottomColor:"#eee" , borderBottomWidth:5 }}>
                        <Row alignItems="center" justifyContent="center">
                            <Col justifyContent="center" style={{flex:1.2}}>
                                <Row alignItems="center" justifyContent="center">
                                    <Col >
                                        <Row alignItems="center" justifyContent="center">
                                            <Image source={require(`${TAKE_AWAY}img/icon/waiting.png`)} style={{width:30 , height:30}} resizeMode="stretch"/>
                                            <Text style={{ paddingLeft:5 , fontSize:17}}>
                                                15
                                            </Text>
                                        </Row>
                                    </Col>
                                    <Col>
                                        <Row alignItems="center" justifyContent="center">
                                            <Image source={require(`${TAKE_AWAY}img/icon/done.png`)} style={{width:30 , height:30}} resizeMode="stretch"/>
                                            <Text style={{ paddingLeft:5 , fontSize:17}}>
                                                15
                                            </Text>
                                        </Row>
                                    </Col>
                                    <Col>
                                        <Row alignItems="center" justifyContent="center">
                                            <Image source={require(`${TAKE_AWAY}img/icon/rejected.png`)} style={{width:30 , height:30}} resizeMode="stretch"/>
                                            <Text style={{ paddingLeft:5 , fontSize:17}}>
                                                15
                                            </Text>
                                        </Row>
                                    </Col>
                                </Row>
                            </Col>
                            <Col justifyContent="center" alignItems="flex-end" style={{flex:1 }}>
                                <Button full rounded style={[Styles.flat , Styles.bgPrimary , { marginLeft:40 , height:33}]} onPress={()=>{this.handleChangePage("UploadReceipt")}}>
                                    <Image source={require(`${TAKE_AWAY}img/icon/scan.png`)} style={{height:23 , width:23}}/>
                                </Button>
                            </Col>
                        </Row>
                    </Grid>

                    
                    
                    <ConnectSocialMedia dataArray={SOCIAL_MEDIA} onPress={this.handlePressConnectSocialMedia} openPopUp={this.handleOpenPopup} />
                    
                    <PermissionPromotion dataArray={[1]} openPopUp={this.handleOpenPopup}/>

                    <ControlData dataArray={[1]} openPopUp={this.handleOpenPopup} changePage={this.handleChangePage}/>

                </Content>
            </Container>
        )
    }
}


const mapStateToProps = (state) => ({
    wallet : state.WalletReducer
})
export default connect(mapStateToProps)(ConnectSosmed);