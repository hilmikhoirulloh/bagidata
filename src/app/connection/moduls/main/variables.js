export const SOCIAL_MEDIA = [
    {
        id : 1,
        name : "Facebook",
        icon : {
            url : require("../../../../../img/icon/facebook.png"),
            width : 29,
            height: 52,
        }, 
        walpaper :  {
            url :require("../../../../../img/media-sosial-walpaper/facebook.png"),
            width : 100 , 
            height:100
        },
        status : 0,
        type : 1
    }
]