export const WARNING = [
    {
        icon : require("../../../../../img/icon/retail.png"),
        title : "Retail",
        width : 90,
        height : 80
    },
    {
        icon : require("../../../../../img/icon/train_big.png"),
        title : "Kereta",
        width : 70,
        height : 90
    },
    {
        icon : require("../../../../../img/icon/aircraft_big.png"),
        title : "Pesawat",
        width : 90 ,
        height : 90
    },
    {
        icon : require("../../../../../img/icon/bus_big.png"),
        title : "Bus",
        width: 84, 
        height : 90
    },
    {
        icon : require("../../../../../img/icon/restaurant_big.png"),
        title : "Restoran",
        width : 90 ,
        height : 90
    }
]