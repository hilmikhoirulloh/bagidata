import React , {Component} from "react";
import {View, Text , Image} from "react-native";
import {Container , Content , Header , Left , Right , Body , Button , Icon  , Title , Grid , Row} from "native-base";

import {Styles} from "../../../styles";
import {WARNING} from "../variables";
import {connect } from "react-redux";

const TAKE_AWAY=  "../../../../../../";
class PageWarning extends Component{
    constructor(props){
        super(props)
        this.data = WARNING[this.props.int];
        this.props.navigator.setStyle({
            navBarHidden : true
        })
    }
    handleBack(){
        this.props.navigator.pop();
    }
    handleChangePage(page){
        this.props.navigator.push({
            screen : `bagidata.Layout.Connection.${page}`,
            passProps : {
                id : this.props.int
            } 
        });
    }
    render(){
        return(
            <Container style={Styles.bgWhite}>
                <Header style={[Styles.bgWhite,{marginBottom:3 }]}>
                    <Left>
                        <Button transparent onPress={()=>{this.handleBack()}}>
                            <Icon name='arrow-back' style={Styles.fontBlack}/>
                        </Button>
                    </Left>
                    <Body>
                        <Title style={Styles.fontBlack}>
                            {this.data.title}
                        </Title>
                    </Body>
                    <Right />
                </Header>
                <Grid style={{position:"absolute" , top:30 , bottom:0 , left:0 , right:0}}>
                    <Row alignItems="flex-end" justifyContent="center" style={{flex:3}}>
                        <View style={{width:this.data.width , height:this.data.height}}>
                            <Image source={this.data.icon} style={{flex:1 , height:null , width:null}} resizeMode="stretch"/>
                        </View>
                    </Row>
                    <Row style={{flex:3}} justifyContent="center">
                        <View style={{width: "90%" , paddingTop:10}}>
                            <Text style={{textAlign:'center' , color:"#aaa"}}>
                                Unggah struk kamu dan  dapatkan Rp 100 setelah disetujui , struk kamu akan Bagidata gunakan untuk kebiasaan kamu sehingga promo yang masuk bisa kamu banget
                            </Text>
                        </View>
                    </Row>
                    <Row style={{flex:1}} alignItems="center" justifyContent="center">
                        <View style={{width:"30%"}}>
                            <Button full rounded style={[Styles.flat , Styles.bgPrimary ]} onPress={()=>{this.handleChangePage("Camera")}}>
                                <Image source={require(`${TAKE_AWAY}img/icon/scan.png`)} style={{height:30 , width:30}}/>
                            </Button>
                        </View>
                    </Row>
                    
                </Grid>
                
            </Container>
        )
    }
}

const mapStateToProps =(state) => ({

}) 

export default connect(mapStateToProps)(PageWarning);