import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image, 
  ImageEditor
} from 'react-native';
import {Container , Left , Body , Title , Right  , Header ,Button , Icon , Grid , Row, Col, Footer  } from "native-base";
import {Styles, COLOR_PRIMARY} from "../../../styles";
import { RNCamera  } from 'react-native-camera';
import ImagePicker from 'react-native-image-picker';


export default class Camera extends Component {
    state= {
        flash: "off",
        uri : null
    }
    handleBack(){
        this.props.navigator.pop();
    }
    handleFlash = ()=>{
        this.setState({
            flash : this.state.flash == "off" ? "on": "off"
        })
    }
    takePicture() {
        if (this.camera) {
            const options = { quality: 0.1 , base64: true };
            
            this
            .camera
            .takePictureAsync(options)
            .then(({uri})=>{
                this.props.navigator.push({
                    screen : "bagidata.Layout.Connection.AuthCamera",
                    passProps : {
                        uri 
                    }
                })
            })   
        }
    }
    getPhotoOfLibrary(){
        const options = {
            title: 'Upload Gambar',
            takePhotoButtonTitle : null,
            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            if(response.uri){
                this.props.navigator.push({
                    screen : "bagidata.Layout.Connection.AuthCamera",
                    passProps : {
                        uri : response.uri
                    }
                })
            }
            // let source = { uri: response.uri };
            // this.setState({
            //     avatarSource: source
            // });
        });
    } 
    render() {
        return (
            <Container style={{backgroundColor:"transparent"}}>
                <Header  style={[Styles.bgWhite,{marginBottom:3}]}>
                    <Left>
                        <Button transparent onPress={()=>{this.handleBack()}}>
                            <Icon name='arrow-back' style={Styles.fontBlack}/>
                        </Button>
                    </Left>
                    <Body>
                        <Title style={Styles.fontBlack}>
                            Unggah Struk
                        </Title>
                    </Body>
                    <Right />
                </Header>
                <RNCamera
                    ref={ref => {
                    this.camera = ref;
                    }}
                    style = {styles.preview}
                    type={RNCamera.Constants.Type.back}
                    flashMode={RNCamera.Constants.FlashMode[this.state.flash]}
                />
                <Footer style={{backgroundColor:"transparent" , height:200 ,}}>
                    <Grid>
                        <Row>
                            <Col style={{flex:2}}> 
                                <Row justifyContent="center" alignItems="center">
                                    <TouchableOpacity onPress={()=>{this.getPhotoOfLibrary()}}>
                                        <View style={{width:40 , height:40 , overflow:"hidden", borderRadius:8 , borderColor:"white" , borderWidth:2}}>
                                                
                                        </View>
                                    </TouchableOpacity>

                                </Row>
                            </Col>
                            
                            <Col alignItems="center" justifyContent="center" style={{flex:1}} > 
                                <Button full rounded style={[{backgroundColor:COLOR_PRIMARY , height:70 , width:70 } , Styles.flat]} onPress={()=>{this.takePicture()}}/>
                            </Col>
                            <Col style={{flex:2}}> 
                                <Row justifyContent="center" alignItems="center">
                                    <TouchableOpacity onPress={this.handleFlash}>
                                        <Icon name={this.state.flash == "on" ? "ios-flash":"ios-flash-outline"} style={{fontSize:60 , width:40 , color:"white"}}/>
                                    </TouchableOpacity>
                                </Row>
                            </Col>
                        </Row>
                    </Grid>
                </Footer>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginTop: -5,
    marginBottom: -200,
    zIndex:-1000
  }
});
