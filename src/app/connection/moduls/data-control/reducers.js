const searchDataControlState ={
    result : [],
    isLoading : false,
    error : false
}  

export const SearchDataControlReducer = (state = searchDataControlState , action) => {
    switch(action.type){
        case "SEARCH_DATA_CONTROL": 
            return {...state , result : action.payload}
        default :
            return state
    }
}