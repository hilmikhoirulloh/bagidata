import React , {Component} from "react";
import {View , Text , LayoutAnimation } from "react-native";
import  {Container  , Content , Header , Left , Right , Body , Title , Icon , Button  ,List , ListItem , Thumbnail} from "native-base";

import {Styles , HEIGHT_WINDOW } from "../../../styles";
import Form from "../components/form";

import {connect} from "react-redux";

const FAR_TOP = 185
class DataControl extends Component {
    state = {
        active : false
    }
    constructor(props){
        super(props);
        this.props.navigator.setStyle({
            navBarHidden :true
        });
    }
    handleBack(){
        this.props.navigator.dismissModal({
            animationType: 'slide-down'
        });
    }
    handleFocus = () => {
        LayoutAnimation.spring();
        this.setState({
            active : true  
        })
    }
    handleBlur = () => {
        LayoutAnimation.spring();
        this.setState({
            active : false 
        })
    }
    render(){
        return(
            <Container style={Styles.bgWhite}>
                <Header style={[Styles.bgWhite , {marginBottom:3}]}>
                    <Left>
                        <Button transparent onPress={()=>{this.handleBack()}}>
                            <Icon name='arrow-back' style={Styles.fontBlack}/>
                        </Button>
                    </Left>
                    <Body>
                        <Title style={Styles.fontBlack  }>
                            Data Kontrol
                        </Title>
                    </Body>
                    <Right />
                </Header>
                <Content padder>
                    <View style={{paddingHorizontal:15}}>
                        <Text style={[Styles.fontBlack , Styles.fontWeight , {fontSize:17}]}>
                            Pengecualian Data
                        </Text>
                        <Text style={[Styles.colorGrey  , {fontSize:12 }]}>
                            Berikut adalah daftar industri yang tidak kamu inginkan
                        </Text>
                    </View>
                    <View style={{paddingVertical:20}}>
                        <Form onFocus={this.handleFocus} onBlur={this.handleBlur}/>
                    </View>
                    <View style={{flex:1 , flexDirection:"row" , flexWrap:"wrap", marginHorizontal:10 }}>
                        <Button bordered rounded style={[{paddingHorizontal:10 ,marginHorizontal:5 , marginBottom:10 } , Styles.bordered , Styles.borderSecondary]}>
                            <Text style={[Styles.colorSecondary , {fontSize:15 , paddingRight:15}]}>
                                Politik
                            </Text>
                            <Thumbnail source={require("../../../../../../img/icon/cancel.png")}  style={{width:15 , height:15}} small square resizeMode="stretch"/>
                        </Button>
                        
                        
                    </View>
                </Content>
                <View style={[{
                    position:"absolute" , 
                    bottom:this.state.active ? 0 : HEIGHT_WINDOW-FAR_TOP  ,   
                    top:this.state.active ? FAR_TOP : FAR_TOP-10  , 
                    left: 0 , 
                    right:0 , 
                    backgroundColor:this.state.active ? "white" : "transparent"  
                    }]} >
                    <List
                        dataArray ={this.props.search}
                        renderRow = {(item) =>
                            <ListItem>
                                <Text>{item}</Text>
                            </ListItem>                
                        }
                    />
                </View>
                
            </Container>
        )
    }
}

const mapStateToProps = (state) =>({
    search : state.SearchDataControlReducer.result
})
export default connect(mapStateToProps)(DataControl);