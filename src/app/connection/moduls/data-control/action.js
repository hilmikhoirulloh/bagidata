const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

import DATA_CONTROL from "../../variables/data-control"; 

export const asyncValidateSearch  = (values , dispatch)=>{
    return sleep(1).then(() => {
        const filterSearch = DATA_CONTROL.filter((data)=>( data.search(values.search) != -1 ));
        dispatch(setSearch(filterSearch));
    })
}

export const setSearch = (data) => {
    return {
        type : "SEARCH_DATA_CONTROL" , 
        payload : data
    }
}
