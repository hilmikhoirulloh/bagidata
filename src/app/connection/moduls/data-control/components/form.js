import React , {Component} from "react";
import {Form as FForm } from "native-base";

import  {Field , reduxForm} from "redux-form";
import {InputSearch} from "./input";

import {asyncValidateSearch as asyncValidate} from "../action";
class Form extends Component{
    render(){
        return(
            <FForm>
                <Field
                    name="search"   
                    onFocus={this.props.onFocus}
                    onBlur={this.props.onBlur}
                    placeholder="Pilih Industri(contoh: Makanan)"
                    component={InputSearch}
                />
            </FForm>
        )
    }
}

export default reduxForm({
    form : "search",
    asyncChangeFields : ["search"] , 
    asyncValidate   
})(Form)