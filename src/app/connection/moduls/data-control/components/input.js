import React from "react";
import {Item , Input} from "native-base";
import {Text , View} from "react-native";
import {Styles} from "../../../styles";

export const InputSearch = ({
    input ,
    placeholder ,
    onfocus,
    meta : {touch , error}
})=>{
    return(
        <Item rounded style={[Styles.bgGrey , Styles.noBorder, {paddingHorizontal:10} ]}>
            <Input {...input} placeholder={placeholder} style={{fontSize:14 , color:"#444" , paddingLeft:5  }} placeholderTextColor="#aaa" />
        </Item>
    )
}