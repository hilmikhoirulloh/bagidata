import React ,{Component} from "react";
import {Item, Input} from "native-base";
import {Styles} from "../../../styles";

export class InputText extends Component {
    render(){
        const {input , placeholder , meta }= this.props;
        return (
            <Item rounded style={[Styles.bgGrey , Styles.noBorder, {paddingHorizontal:10, marginVertical:5} ]}>
                <Input {...input} placeholder={placeholder} style={{fontSize:14 , fontFamily:"Barlow-Regular" ,  color:"#444" , paddingLeft:5  }} placeholderTextColor="#aaa" />
            </Item>
        )
    }
} 