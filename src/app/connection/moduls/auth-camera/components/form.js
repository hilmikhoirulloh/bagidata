import React, {Component} from "react";
import {Field, reduxForm} from "redux-form";
import {Form} from "native-base";

import {InputText} from "./input";

class FormComponent extends Component{
    render(){
        return(
            <Form>
                <Field
                    name="name_receipt"
                    placeholder="Nama struk"
                    component={InputText} 
                />
                <Field
                    name="owner_receipt"
                    placeholder="Struk ini milik siapa?"
                    component={InputText} 
                />
            </Form>
        )
    }
}

export default reduxForm({
    form : "auth_receipt"
})(FormComponent)