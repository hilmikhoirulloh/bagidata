
import React , {Component} from "react";
import {View , Text, Image, TouchableWithoutFeedback} from "react-native";
import  {Container, Content, Header, Left, Button, Icon, Body, Title, Right} from "native-base";
import {CoordinatorLayout , BottomSheetBehavior , FloatingActionButton } from "react-native-bottom-sheet-behavior";
import {connect} from "react-redux";
import {Styles , HEIGHT_WINDOW, COLOR_PRIMARY} from "../../../styles";
import Form from "../components/form";


class AuthCamera extends Component {
    changePage = (page)=> {
        this.props.navigator.resetTo({
            screen : `bagidata.Layout.Connection.${page}`,
            animationType : "none"
        });
    }
    render(){
        const {uri} = this.props;
        return (
            <View style={{flex:1}}>
                <View style={{position:"absolute" , top :0 , bottom :0,left :0, right :0 }}>
                    <Image source={{uri}} style={{flex:1 , height:HEIGHT_WINDOW}}/>
                </View>
                <CoordinatorLayout style={{flex: 1}}>
                    <Container style={{backgroundColor:"transparent"}}>
                        <Header  style={[Styles.bgWhite,{marginBottom:3}]}>
                            <Left>
                                <Button transparent onPress={()=>{this.changePage("UploadReceipt")}}>
                                    <Icon name='arrow-back' style={Styles.fontBlack}/>
                                </Button>
                            </Left>
                            <Body>
                                <Title style={Styles.fontBlack}>
                                    Unggah Struk
                                </Title>
                            </Body>
                            <Right />
                        </Header>
                    </Container>
                    <BottomSheetBehavior
                        ref='bottomSheet'
                        peekHeight={70}
                        hideable={true}
                        state={BottomSheetBehavior.STATE_EXPANDED}>
                        <View style={{backgroundColor: 'white' ,paddingHorizontal:20}}>
                            <View style={{flex:1, alignItems:"center" , paddingTop:10}}>
                                <View style={{width:30 , height:3 , backgroundColor:"#ddd"}}>
                                </View>
                            </View>
                            <View style={{paddingVertical:19}}>
                                <Text style={[{textAlign:"center" , fontSize: 16 , color:"#ccc"}]}>
                                    Struk kamu kurang jelas?
                                </Text> 
                            </View> 
                            <View style={{height: 300, backgroundColor: '#fff'}} >
                                <View style={{paddingBottom:40}}>
                                    <TouchableWithoutFeedback onPress={()=>{this.changePage("Camera")}}>
                                        <View>
                                            <Text style={[Styles.fontWeight,{textAlign:"center" , fontSize: 16 , color:COLOR_PRIMARY}]}>
                                                Foto Ulang / Ganti Foto?
                                            </Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>

                                <View style={{paddingBottom:40}}>                         
                                    <Form/>
                                </View>

                                <View style={{flex:1, alignItems:"center" }}>
                                    <View style={{width:150}}>
                                        <Button  rounded  style={[{backgroundColor:COLOR_PRIMARY , width:"100%"} , Styles.flat]}>
                                            <Text style={[Styles.fontWeight, {color:"white" , textAlign:"center" , width:"100%"}]}>
                                                Submit
                                            </Text>
                                        </Button>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </BottomSheetBehavior>
                </CoordinatorLayout>
            </View>
        )
    }
}

export default connect()(AuthCamera);