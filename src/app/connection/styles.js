import React from "react";
import {StyleSheet , Dimensions} from "react-native";

export const COLOR_PRIMARY  = "#ec008c";
export const COLOR_SECONDARY = "#222e61";
export const HEIGHT_WINDOW  = Dimensions.get("screen").height;
export const WIDTH_WINDOW  = Dimensions.get("screen").width;
export const Styles= StyleSheet.create({
    bgWhite : {
        backgroundColor:'white'     
    },
    fontBlack : {
        color: "#444"
    },
    fontWeight : {
        fontFamily : "Barlow-SemiBold"
    },
    bgPrimary : {
        backgroundColor : COLOR_PRIMARY
    },
    flat : {
        elevation: 0
    },
    textColorSecondary : {
        color : COLOR_SECONDARY
    },
    boxConnection:{
        width:"100%" , 
        height:110 , 
        borderColor:COLOR_SECONDARY , 
        borderWidth:2 , 
        borderRadius:8,
        paddingVertical:8
    },
    disabledBoxConnection:{
        width:"100%" , 
        height:110 ,  
        borderRadius:8,
        backgroundColor:"#eee"
    },
    ListItem : {
        borderBottomColor:"#eee" , 
        borderBottomWidth:1  , 
        paddingBottom:15 , 
        paddingTop:15,
        paddingLeft : 20,
        paddingRight:20
    },
    boxPopup : {
        width:"95%" ,
        borderRadius:20
    },
    boldSmall : {
        fontSize : 10,
        fontFamily:"Barlow-SemiBold"
    },
    colorGrey : {
        color :"#999"
    },
    bgGrey : {
        backgroundColor :"#eee"
    },
    noBorder : {
        borderBottomWidth: 0 ,
        borderTopWidth : 0 , 
        borderLeftWidth : 0,
        borderRightWidth : 0
    },
    bordered : {
        borderBottomWidth: 2 ,
        borderTopWidth : 2 , 
        borderLeftWidth : 2,
        borderRightWidth : 2
    },
    borderSecondary : {
        borderColor:COLOR_SECONDARY
    },
    colorSecondary : {
        color:COLOR_SECONDARY
    },
    bottomButton : {
        position:"absolute" , 
        bottom:10,
        left:10,
        right :10 ,
        elevation: 0
    },
});