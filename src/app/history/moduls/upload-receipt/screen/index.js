import React , { Component } from "react" ;
import {View , Text  ,Image  , TouchableOpacity} from "react-native" ;

import { Styles , WINDOW_WIDTH } from "../../../styles";
import {Container , Content , Header , Left , Body , Right , Icon , List , Title , Button , ListItem , Item ,  Footer , Grid , Col , Row}  from "native-base";

import {connect} from "react-redux";

const TAKE_AWAY = "../../../../../../";
class HistoryBuy extends Component {
    constructor(props){
        super(props);
        this.props.navigator.setStyle({
            navBarHidden : true
        })
    }
    handleBack(){
        this.props.navigator.dismissModal({
            animationType: 'none'
        });
    }
    items = [
        'Simon Mignolet',
    ];
    render(){
        return(
            <Container>
                <Header style={[Styles.bgWhite,{marginBottom:3 }]}>
                    <Left>
                        <Button transparent onPress={()=>{this.handleBack()}}>
                            <Icon name='arrow-back' style={Styles.fontBlack}/>
                        </Button>
                    </Left>
                    <Body>
                        <Title style={Styles.fontBlack}>
                            Beli
                        </Title>
                    </Body>
                    <Right />
                </Header>
                <Content style={{backgroundColor:"white" }}>
                    <List dataArray={this.items}
                        renderRow={(item) =>
                            <ListItem style={[Styles.bgGrey , {marginVertical:2 , borderBottomWidth:0}]} last>
                                <Grid >
                                    <Row>
                                        <Col style={{flex:1}}  justifyContent="center">
                                            <Image source={require(`${TAKE_AWAY}img/icon/waiting.png`)} style={{width:35 , height:35}} resizeMode="stretch"/>
                                        </Col>
                                        <Col style={{flex:1.5}} alignItems="flex-start">
                                            <Text style={{fontFamily : "Barlow-SemiBold" }}>Provide</Text>
                                            <Text style={[Styles.fontThin , {marginVertical:-4}]}>Type</Text>
                                            <Text style={Styles.fontThin}>hp</Text>
                                        </Col>
                                        <Col style={{flex:1.5}} alignItems="flex-start" justifyContent="center">
                                            <Item style={{borderBottomWidth:0}}>
                                                <Icon name="ios-time-outline" style={{fontSize:10 ,  paddingRight: 2, padding:0}}/>
                                                <Text style={{fontSize:10}}>13.45 WIB</Text>
                                            </Item>
                                        </Col>
                                        <Col style={{flex:1.5}} alignItems="flex-start" justifyContent="center">
                                            <Item style={{borderBottomWidth:0}}>
                                                <Image source={require(`${TAKE_AWAY}img/icon/calender.png`)} style={{width:10 , height:10, marginRight:2}} resizeMode="stretch"/>
                                                <Text style={{fontSize:10}}>13.45 WIB</Text>
                                            </Item>
                                        </Col>
                                        <Col style={{flex:1.5}} alignItems="flex-start" justifyContent="center">
                                            <Text style={[Styles.fontBlack , {fontFamily : "Barlow-SemiBold" }]}>+100</Text>
                    
                                        </Col>
                                    </Row>
                                </Grid>
                            </ListItem>
                        }>
                    </List>
                </Content>
            </Container>
        )
    }
} 

const mapStateToProps = () => ({

})
export default connect(mapStateToProps)(HistoryBuy)