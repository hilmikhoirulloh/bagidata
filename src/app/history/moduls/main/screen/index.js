import React , {Component} from "react" ;
import {Container , Content , Header , ListItem , List , Body , Title  , Right , Left } from "native-base";
import {View , Text ,Image, TouchableOpacity} from "react-native";

import {Styles} from "../../../styles";
import {connect} from "react-redux";

const TAKE_AWAY = "../../../../../../";
class Main extends Component{
    constructor(props){
        super(props)
        this.props.navigator.setStyle({
            navBarHidden : true
        })
    }
    handleChangePage(page){
        this.props.navigator.showModal({
            screen  :`bagidata.Layout.History.${page}`,
            animationType : "none"
        })
    }
    render(){
        return(
            <Container>
                <Header style={[Styles.bgWhite , {marginBottom: 3}]}>
                    <Body style={{marginLeft:10}}>
                        <Title style={Styles.fontBlack}>
                            Riwayat
                        </Title> 
                    </Body> 
                    <Right />
                </Header>
                <Content>
                    <List>
                        <ListItem itemHeader first style={Styles.ListItem } onPress={()=>{this.handleChangePage("Transfer")}}>
                            <Left >
                                <Image source={require(`${TAKE_AWAY}img/icon/history_transfer.png`)} style={{height:51/2 , width:29/2}} resizeMode="stretch"/>
                            </Left>
                            <Body style={{marginLeft:-170}}>
                                <Text>Transfer</Text>
                            </Body>
                            <Right>
                                <TouchableOpacity onPress={()=>{this.handleChangePage("Transfer")}}>
                                    <Image source={require(`${TAKE_AWAY}img/icon/to_history.png`)} style={{height:50/2 , width:51/2}} resizeMode="stretch"/> 
                                </TouchableOpacity>
                            </Right>
                        </ListItem>
                        <ListItem itemHeader first style={Styles.ListItem} onPress={()=>{this.handleChangePage("Buy")}} >
                            <Left>
                                <Image source={require(`${TAKE_AWAY}img/icon/history_buy.png`)} style={{height:50/2 , width:51/2}} resizeMode="stretch"/>
                            </Left>
                            <Body style={{marginLeft:-170}}>
                                <Text>Beli</Text>
                            </Body>
                            <Right>
                                <TouchableOpacity onPress={()=>{this.handleChangePage("Buy")}}>
                                    <Image source={require(`${TAKE_AWAY}img/icon/to_history.png`)} style={{height:50/2 , width:51/2}} resizeMode="stretch"/> 
                                </TouchableOpacity>
                            </Right>
                        </ListItem>
                        <ListItem itemHeader first style={[Styles.ListItem]} onPress={()=>{this.handleChangePage("Bill")}}>
                            <Left>
                                <Image source={require(`${TAKE_AWAY}img/icon/history_bill.png`)} style={{height:50/2 , width:50/2}} resizeMode="stretch"/>
                            </Left>
                            <Body style={{marginLeft:-170}}>
                                <Text>Bayar</Text>
                            </Body>
                            <Right>
                                <TouchableOpacity onPress={()=>{this.handleChangePage("Bill")}}>
                                    <Image source={require(`${TAKE_AWAY}img/icon/to_history.png`)} style={{height:50/2 , width:51/2}} resizeMode="stretch"/> 
                                </TouchableOpacity>
                            </Right>
                        </ListItem>
                        <ListItem itemHeader first style={Styles.ListItem} onPress={()=>{this.handleChangePage("UploadReceipt")}}>
                            <Left>
                                <Image source={require(`${TAKE_AWAY}img/icon/history_receipt.png`)} style={{height:54/2 , width:58/2}} resizeMode="stretch"/>
                            </Left>
                            <Body style={{marginLeft:-170}}>
                                <Text>Unggah Struk</Text>
                            </Body>
                            <Right>
                                <TouchableOpacity onPress={()=>{this.handleChangePage("UploadReceipt")}}>
                                    <Image source={require(`${TAKE_AWAY}img/icon/to_history.png`)} style={{height:50/2 , width:51/2}} resizeMode="stretch"/> 
                                </TouchableOpacity>
                            </Right>
                        </ListItem>
                        <ListItem itemHeader first style={Styles.ListItem}>
                            <Left>
                                <Image source={require(`${TAKE_AWAY}img/icon/history_transaction.png`)} style={{height:42/2 , width:52/2}} resizeMode="stretch"/>
                            </Left>
                            <Body style={{marginLeft:-170}}>
                                <Text>Data Transaksi</Text>
                            </Body>
                            <Right>
                                <TouchableOpacity>
                                    <Image source={require(`${TAKE_AWAY}img/icon/to_history.png`)} style={{height:50/2 , width:51/2}} resizeMode="stretch"/> 
                                </TouchableOpacity>
                            </Right>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        )
    }
}

const mapStateToProps = (state) => ({});
export default connect(mapStateToProps)(Main);