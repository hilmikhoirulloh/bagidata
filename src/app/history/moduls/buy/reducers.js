const HandleIconState = {
    result : {
        allIcon : []
    },
    change: 0
}

export function HandleIconHomeReducer(state = HandleIconState , action) {
    switch(action.type){
        case "SET_DEFAULT_RECOMMEND_ICON_REDUCER": 
            return {
                ...state , 
                result : {
                    allIcon : action.payload
                },
                change : action.change       
            }
        default :
            return state    
    }
}
