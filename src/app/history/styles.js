import React from "react";
import {StyleSheet , Dimensions} from "react-native";

export const COLOR_PRIMARY  = "#ec008c";
export const COLOR_SECONDARY = "#222e61";
export const HEIGHT_WINDOW  = Dimensions.get("screen").height;
export const WIDTH_WINDOW  = Dimensions.get("screen").width;
export const Styles= StyleSheet.create({
    bgWhite : {
        backgroundColor:'white'     
    },
    fontBlack : {
        color: "#444"
    },  
    ListItem : {
        borderBottomColor:"#eee" , 
        borderBottomWidth:1  , 
        paddingBottom:15 , 
        paddingTop:15,
        paddingLeft : 20,
        paddingRight:20
    },
    fontBlack : {
        color :"#444",
        fontFamily : "Barlow-SemiBold"
    },
    bgGrey : {
        backgroundColor: "#f8f8f8"
    },
    fontThin : {
        fontSize:11 ,
        fontFamily : "Barlow-Light"
    },
})