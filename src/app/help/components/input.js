import React  from "react" ;
import {Item , Icon , Input } from "native-base";
import { Text , View} from "react-native";
import {Styles} from "../styles";

export const InputSearch = ({
    input,
    placeholder ,
    meta : { asyncValidating, touched, error }
})=>{
    return(
        <Item rounded  style={[Styles.noBorder , Styles.bgGrey , {paddingHorizontal:5}]}>
            <Icon name="ios-search" style={{color:"#aaa"}} />
            <Input placeholder={placeholder} {...input} style={{fontSize:14  , fontFamily:"Barlow-Regular", color:"#444" , paddingLeft:5  }} placeholderTextColor="#aaa"/>
        </Item>
    )
}