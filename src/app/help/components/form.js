import React , {Component} from "react"
import {View , Text} from "react-native";
import {Form as FormD} from "native-base";

import {Field , reduxForm} from "redux-form";
import {Async} from "../action";
import {InputSearch} from "./input";

class Form extends Component {
    render(){
        return(
            <FormD>
                <Field
                    name="search"
                    placeholder="cari bantuan kamu disini"
                    component={InputSearch} 
                />
            </FormD>
        )
    }
}


export default reduxForm({
    form : "helpSearch",
    asyncValidate : Async,
    asyncChangeFields: ['search']
})(Form);