import React from "react";
import {StyleSheet , Dimensions} from "react-native";

export const COLOR_PRIMARY  = "#ec008c";
export const COLOR_SECONDARY = "#222e61";
export const HEIGHT_WINDOW  = Dimensions.get("screen").height;
export const WIDTH_WINDOW  = Dimensions.get("screen").width;
export const Styles= StyleSheet.create({
    bgWhite : {
        backgroundColor:'white'     
    },
    fontBlack : {
        color: "#444"
    },  
    ListItem : {
        borderBottomColor:"#eee" , 
        borderBottomWidth:1  , 
        paddingBottom:15 , 
        paddingTop:15,
        paddingLeft : 20,
        paddingRight:20
    },
    fontBlack : {
        color :"#444"
    },
    bgGrey : {
        backgroundColor: "#f8f8f8"
    },
    fontThin : {
        fontSize:11 , 
        fontFamily : "Barlow-Light"
    },
    noBorder : {
        borderBottomWidth : 0,
        borderTopWidth : 0,
        borderLeftWidth : 0,
        borderRightWidth : 0,
    },
    fontBold : {
        fontFamily : "Barlow-SemiBold"
    }, 
    helpInformation : {
        fontSize:15 , 
        fontFamily : "Barlow-Regular",
        color:"#bbb"
    },
    bgPrimary : {
        backgroundColor:COLOR_PRIMARY
    },
    btnFlat : {
        elevation :0
    },
    boldBorder: {
        borderBottomWidth:2 , 
        borderTopWidth:2 , 
        borderRightWidth:2 , 
        borderLeftWidth:2
    }
})