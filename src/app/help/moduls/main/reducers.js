const GetHelpState= {
    results : [],
    isLoading : false,
    error : false
}

export function GetHelpReducer(state=GetHelpState , action){
    switch(action.type){
        case "GET_HELP_REDUCER_PENDING" : 
            return {...state, isLoading : true, error:false}
        case "GET_HELP_REDUCER_FULFILLED" :
            return {...state, isLoading : false, results:action.payload.data, error:false }
        case "GET_HELP_REDUCER_REJECTED":
            return {...state, isLoading : false, results:[], error:true}
        default :
            return state
    }
}