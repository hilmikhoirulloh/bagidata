import React , {Component} from "react" ;
import {Container , Content , Header , ListItem , List , Body , Title  , Right , Left } from "native-base";
import {View , Text ,Image, TouchableOpacity} from "react-native";

import {Styles} from "../../../styles";
import {connect} from "react-redux";
import {GetHelp} from "../action"
import {DATA_HELP} from "../variables";
import Form  from '../../../components/form';

const TAKE_AWAY = "../../../../../../";
class Main extends Component{
    constructor(props){
        super(props)
        this.props.navigator.setStyle({
            navBarHidden : true
        })
    }
    handleChangePage = (name) => {
        // console.log(page);
        this.props.navigator.showModal({
            screen  :`bagidata.Layout.Help.EachPage`,
            passProps : {
                name
            },
            animationType : "none"
        })
    }

    handleChangePageFaq = () => {
        this.props.navigator.showModal({
            screen  :`bagidata.Layout.Help.Provider`,
            animationType : "none"
        })
    }

    async componentDidMount(){
        await this.props.dispatch(GetHelp());
    }

    render(){
        return(
            <Container style={Styles.bgWhite}>
                <Header style={[Styles.bgWhite , {marginBottom: 3}]}>
                    <Body style={{marginLeft:10}}>
                        <Title style={Styles.fontBlack}>
                            Bantuan
                        </Title> 
                    </Body> 
                    <Right />
                </Header>
                <Content padder>
                    <Form />
                    <List
                        style={{marginHorizontal:-10}}>
                        {DATA_HELP.map((item , i)=>(
                            <ListItem key={i}  itemHeader first style={Styles.ListItem } onPress={()=>{this.handleChangePage(item.name)}}>
                                <Left >
                                    <Image source={item.uri} style={{height:item.icon.height/3 , width:item.icon.width/3}} resizeMode="stretch"/>
                                </Left>
                                <Body style={{marginLeft:-190}}>
                                    <Text>{item.title}</Text>
                                </Body>
                                <Right>
                                    <TouchableOpacity onPress={()=>{this.handleChangePage(item.name)}}>
                                        <Image source={require(`${TAKE_AWAY}img/icon/to_history.png`)} style={{height:50/2 , width:51/2}} resizeMode="stretch"/> 
                                    </TouchableOpacity>
                                </Right>
                            </ListItem>
                        ))}

                        <ListItem  itemHeader first style={Styles.ListItem } onPress={()=>{this.handleChangePageFaq()}}>
                            <Left >
                                <Image source={require(`${TAKE_AWAY}img/icon/provider.png`)} style={{height:69/3 , width:67/3}} resizeMode="stretch"/>
                            </Left>
                            <Body style={{marginLeft:-190}}>
                                <Text>Call Center & Layanan Pelanggan</Text>
                            </Body>
                            <Right>
                                <TouchableOpacity >
                                    <Image source={require(`${TAKE_AWAY}img/icon/to_history.png`)} style={{height:50/2 , width:51/2}} resizeMode="stretch"/> 
                                </TouchableOpacity>
                            </Right>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        )
    }
}

const mapStateToProps = (state) => ({});
export default connect(mapStateToProps)(Main);