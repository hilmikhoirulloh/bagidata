import axios from "axios";
import {URL_API} from "../../../../../config";
import {store} from "../../../redux/store";
import {header} from "../../../../helper";
export  function GetHelp(data){
    const {LoginAuthReducer} = store.getState();
    const token = LoginAuthReducer.results.token;
    
    return {
        type : "GET_HELP_REDUCER",
        payload : axios.post(`${URL_API}/api/v1/faq` , 
        {} ,
        header())
    }   
} 