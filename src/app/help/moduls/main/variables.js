// const {roleArray, title, image, titleRole} = this.props;

export const  DATA_HELP = [
    {
        uri : require('../../../../../img/icon/facebook.png'),
        icon : {
            width : 29 , 
            height :52 
        },
        name : "Facebook",
        title : "Facebook Data Aktivasi",
        titleRole : "Kami menggunakan informasi data aktivitas Facebook anda untuk",
        role :[
            "Administrasi akun anda",
            "Memberikan layanan promo yang sesuai kepada anda",
            "Memberikan  anda penghasilan tambahan dengan memberikan informasi insight data pengguna internet kepada instansi atau perusahaan-perusahaan",
            "Memberikan informasi kepada anda tentang logbook belanjaan, logbook perjalanan",
            "Penghasilan yang anda dapat, dapat langsung anda gunakan untuk bertransaksi di berbagai merchant"
        ]
    }
]

export const LIST = [
    {
        icon : require("../../../../../img/icon/facebook.png"),
        width : 29 , 
        height :52 ,
        title : "Facebook Data Aktivasi",
        name : "Facebook"
    },
    {
        icon : require("../../../../../img/icon/twitter.png"),
        width : 53 , 
        height :44 ,
        title : "twitter data aktivasi",
        name: "Twitter"
    },
    {
        icon : require("../../../../../img/icon/instagram.png"),
        width : 52 , 
        height :51 ,
        title : "Instagram Data Aktivasi",
        name : "Instagram"
    },
    {
        icon : require("../../../../../img/icon/lock.png"),
        width : 46 , 
        height :68 ,
        title : "Data Istilah & Privasi",
        name : "Lock"
    },
    {
        icon : require("../../../../../img/icon/note.png"),
        width : 58 , 
        height :68 ,
        title : "Data Nota & Tagihan",
        name : "Note"
        
    },
    {
        icon : require("../../../../../img/icon/award.png"),
        width : 58 , 
        height :69 ,
        title : "Poin & Hadiah",
        name : "Award"

    },
    {
        icon : require("../../../../../img/icon/provider.png"),
        width : 67 , 
        height :69 ,
        title : "Call Center & Layanan Pelanggan",
        name : "Provider"
    },
    
]