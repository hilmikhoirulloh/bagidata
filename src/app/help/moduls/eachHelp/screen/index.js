import React , {Component} from "react";
import {Container, Content } from "native-base";
import Header from "../components/header";

import {connect} from "react-redux";
import {HelpDescription} from "../components/typeHelp";
import {DATA_HELP} from "../../main/variables";


class EachHelp extends Component{
    data = null
    constructor(props){
        super(props);
        this.data = DATA_HELP.filter((data)=>(data.name == this.props.name));
    }
    
    handleBack = () => {
        this.props.navigator.dismissModal({
            animationType: 'none'
        });
    }
    
    render(){
        const data = this.data[0];
        return(
            <Container>
                <Header onBack={this.handleBack}/>
                <Content>
                    <HelpDescription 
                        roleArray={data.role} 
                        title={data.title} 
                        image={data.uri} 
                        size={data.icon} 
                        titleRole={data.titleRole} 
                    />
                </Content>
            </Container>
        )
    }
}

export default connect()(EachHelp);