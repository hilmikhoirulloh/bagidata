import React , {Component} from "react"
import {View } from "react-native";
import {Form as FormD , Button , Text} from "native-base";

import {Field , reduxForm} from "redux-form";
import {Async} from "../action";
import {InputMessage} from "./input";
import {Styles} from "../../../styles";

class Form extends Component {
    render(){
        const {placeholder} = this.props;
        return(
            <FormD>
                <View style={{marginHorizontal:-15 , marginVertical:10}}>
                <Field
                    name="input_form"
                    placeholder={placeholder}
                    component={InputMessage} 
                />
                </View>
                <View style={{ marginVertical:10}}>
                    <Button full rounded style={[Styles.bgPrimary , Styles.btnFlat ,]}>
                        <Text>
                            Kirim Tanggapan
                        </Text>
                    </Button>
                </View>
                
            </FormD>
        )
    }
}


export default reduxForm({
    form : "sendMessage",
    // asyncValidate : Async,
    asyncChangeFields: ['input_form']
})(Form);