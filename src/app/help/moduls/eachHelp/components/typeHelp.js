import React ,{Component} from "react";
import {View, Image, Text} from "react-native";
import {Item, Grid, Row, Col, Button} from "native-base";

import {Styles} from "../../../styles";


export class HelpDescription extends Component{
    render(){
        const {roleArray, title, image, titleRole, size} = this.props;
        return(
            <View>
                <View style={{paddingHorizontal:15 }}>
                    <View style={{paddingTop:20}}>
                        <Item style={Styles.noBorder}>
                            <View style={{paddingRight:20}}>
                                <Image source={image} style={{width:size.width/1.6 , height:size.height/1.6}}/>
                            </View>
                            <Text style={[Styles.fontBlack,{fontFamily:"Barlow-SemiBold" , fontSize:15}]}>
                                {title}
                            </Text>
                        </Item>
                    </View>
                    <View style={{paddingVertical:10}}>
                        <Text style={Styles.helpInformation}>
                            {titleRole}
                        </Text>
                    </View>
                    
                    <Grid>
                        {roleArray.map((data,i)=>(
                            <Row key={i}>
                                <Col style={{flex:1}}>
                                    <Text style={Styles.helpInformation}>
                                        {i+1}.
                                    </Text>
                                </Col>
                                <Col style={{flex:17}}>
                                    <Text style={Styles.helpInformation}>
                                        {data}
                                    </Text>
                                </Col>
                            </Row>
                        ))}
                    </Grid>

                </View>
            </View> 
        )
    }
}
