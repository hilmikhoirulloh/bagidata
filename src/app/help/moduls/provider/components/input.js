import React  from "react" ;
import {Item , Icon , Input ,Textarea } from "native-base";
import { Text , View} from "react-native";
import {Styles} from "../../../styles";

export const InputMessage = ({
    input,
    placeholder ,
    meta : { asyncValidating, touched, error }
})=>{
    return(
        <Item rounded  style={[Styles.noBorder , Styles.bgGrey , {paddingHorizontal:5 , borderRadius:10}]} >
            <Textarea rowSpan={5} bordered placeholder={placeholder} style={[{flex:1 , backgroundColor:"tranparent" ,fontSize:14 , color:"#444", fontFamily:"Barlow-Regular"  } , Styles.noBorder]}  placeholderTextColor="#aaa"/>
        </Item>
    )
}