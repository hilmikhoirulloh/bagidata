import React , {Component} from "react" ;
import { View  , Image } from "react-native";
import { Container , Content , Text , Header , Left , Body , Title, Right , Icon , Button , H3 , Item , Grid , Row, Col} from "native-base" ;

import Form  from '../../../components/form';
import FormMessage from "../components/form";

import {Styles} from "../../../styles";

import {connect} from "react-redux";

class Provide extends Component {
    constructor(props){
        super(props);
        this.props.navigator.setStyle({
            navBarHidden : true
        })
    }
    handleBack(){
        this.props.navigator.dismissModal({
            animationType: 'none'
        });
    }
    render(){
        return(
            <Container style={Styles.bgWhite}>
                <Header style={[Styles.bgWhite,{marginBottom:3 }]}>
                    <Left>
                        <Button transparent onPress={()=>{this.handleBack()}}>
                            <Icon name='arrow-back' style={Styles.fontBlack}/>
                        </Button>
                    </Left>
                    <Body>
                        <Title style={Styles.fontBlack}>
                            Bantuan
                        </Title>
                    </Body>
                    <Right />
                </Header>
                <Content padder>
                    <View style={{paddingHorizontal:15 }}>
                        <View style={{paddingTop:20}}>
                            <Item style={Styles.noBorder}>
                                <View style={{paddingRight:20}}>
                                    <Image source={require("../../../../../../img/icon/provider.png")} style={{width:67/2.4 , height:69/2.4}}/>
                                </View>
                                <Text style={[Styles.fontBlack,{fontFamily:"Barlow-SemiBold" , fontSize:15}]}>
                                    Call Center & Layanan Pelanggan
                                </Text>
                            </Item>
                        </View>
                        <View style={{paddingVertical:10}}>
                            <FormMessage/>
                        </View>
                        <View style={{paddingBottom:20}}>
                            <Text style={Styles.helpInformation}>
                                Atau Kamu bisa  menghubungi kami secara langsung melaui
                            </Text>
                        </View>
                        <Grid>
                            <Row style={{paddingHorizontal:10}}>
                                <Col justifyContent="center" alignItems="center">
                                    <Button bordered rounded style={[Styles.boldBorder , {width:"100%"}]}>
                                        <Text style={{ width:"100%" , textAlign:"center"}}>Email</Text>
                                    </Button>
                                </Col>
                                <Col>
                                    <Button bordered rounded style={[Styles.boldBorder , {width:"100%"}]}>
                                        <Text style={{ width:"100%" , textAlign:"center"}}>Telefon</Text>
                                    </Button>
                                </Col>
                            </Row>
                        </Grid>
                    </View>
                </Content>

            </Container>
        )
    }
}

const mapStateToProps = (state) =>({

});
export default connect(mapStateToProps)(Provide);