package com.awesome;

import android.app.Application;

import com.imagepicker.ImagePickerPackage;
import com.bottomsheetbehavior.BottomSheetBehaviorPackage;
import android.support.annotation.Nullable;

import im.shimo.react.cookie.CookieManagerPackage;

import com.facebook.react.ReactPackage;
import com.reactnativenavigation.NavigationApplication;
import org.reactnative.camera.RNCameraPackage;
import com.horcrux.svg.SvgPackage;

import java.util.Arrays;
import java.util.List;
// import java.util.Arrays;
// import java.util.List;


public class MainApplication extends NavigationApplication {

    public boolean isDebug() {
        // Make sure you are using BuildConfig from your own application
        return BuildConfig.DEBUG;
    }

    protected List<ReactPackage> getPackages() {
        // Add additional packages you require here
        // No need to add RnnPackage and MainReactPackage
        return Arrays.<ReactPackage>asList(
          new SvgPackage(),
          new RNCameraPackage(),
          new BottomSheetBehaviorPackage(),
          new ImagePickerPackage(),
          new CookieManagerPackage() // <-- add this line
            // OR if you want to customize dialog style
            // new ImagePickerPackage(R.style.my_dialog_style)
        );
    }

    public List<ReactPackage> createAdditionalReactPackages() {
        return getPackages();
    }
}